
Partial Class photogalleryimage
    Inherits System.Web.UI.Page

    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private SiteNumber As Integer = System.Configuration.ConfigurationManager.AppSettings("Site")
    Public TableWidth As Integer = System.Configuration.ConfigurationManager.AppSettings("SmallTableWidth")
    Public TopImage As String = "BloemCeltic_adTop"
    Public BottomImage As String = "BloemCeltic_adBottom"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        head.Title = "Bloemfontein Celtic Official Website"

        Dim Id As String = ""
        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If

        DatabaseConn.Open()

        SqlQuery = New SqlCommand("Select LargeImage, Headline As ValHeadline, Description As ValDescription From SuperSportZone.dbo.ZonePhotoGalleryImages Where (Id = '" & Id & "')", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlImage.Text = "<img src='http://images.supersport.com/" & RsRec("LargeImage") & "' alt='' border='0' />"
            ltlBlurb.Text = RsRec("ValDescription")
        End While
        RsRec.Close()

    End Sub

    Sub Page_UnLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload

        DatabaseConn.Close()

    End Sub

End Class

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="articles.aspx.vb" Inherits="Default2" MasterPageFile="master_1.master"%>
<%@ MasterType VirtualPath="master_1.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<div class="body_art_content"> 
<div class="art_title"><asp:Label ID="ltlarticleheadline" runat="server" /></div>
<div class="date"><asp:Label ID="ltlarticledate" runat="server" /></div>

<div id="art_story">

<div id="art_story_wrapper">
<div class="image">
<asp:Label ID="ltlarticleimage" runat="server" />
<div class="caption"><asp:Label id="ltlarticlecaption" runat="server" /></div>
</div>
</div>

  <asp:Label Id="ltlarticlebody" runat="server" />
</div>
<div style="clear:both;"></div>
</div> <!-- end body -->



<!-- end body art content -->

</asp:Content>

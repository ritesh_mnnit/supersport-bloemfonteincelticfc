
Partial Class siwelele
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Archive()
    End Sub
    Sub Archive()
        SqlQuery = New SqlCommand("Select Top 20 a.Id, a.Headline, a.Blurb, a.SmallImage As Image, b.ArticleDate As Created From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (c.Site = 79) And (c.Name = 'bloemceltic/siwelele') And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc", DatabaseConn)
        Dim tmpCategory As Integer = SqlQuery.ExecuteScalar
        dl_stories.DataSource = SqlQuery.ExecuteReader
        dl_stories.DataBind()
        dl_stories.DataSource.Close()
    End Sub
    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class

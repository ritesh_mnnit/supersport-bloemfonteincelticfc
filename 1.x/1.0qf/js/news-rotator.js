/************************************************************************************************************
Ajax dynamic list
Copyright (C) August 2008  DTHMLGoodies.com, Alf Magne Kalleland

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

Dhtmlgoodies.com., hereby disclaims all copyright interest in this script
written by Alf Magne Kalleland.

Alf Magne Kalleland, 2008
Owner of DHTMLgoodies.com
	
************************************************************************************************************/	
if(!window.DHTMLGoodies)window.DHTMLGoodies = {};
DHTMLGoodies.NewsFlasher = new Class( {
	config : {
		html :  {
			main : null,
			stories : [],
			hint : null,
			navigator : null
		},
		currentStoryIndex : false,
		currentZIndex : 100,
		userProperties : {
			slide : 'random',
			duration: 200
		},
		inSlideProperties : {
			currentIndex:0,
			coordinates : []
			
		}	
	},		
	initialize : function(props) {
		this._setProperties(props);
		this._parseHtml();			
		
	},
	_setProperties : function(properties) {			
		this.config.html.main = $(properties.html);
		for(var prop in properties) {
			this.config.userProperties[prop] = properties[prop];
		}
	}
	,
	_parseHtml : function() {			
		this.config.html.main.addClass('newsRotator');
		this._parseNews();
		this._createNavigatorPane();
		this._createHintObject();
	},
	_createHintObject : function() {
		var el = this.config.html.hint = new Element('div');
		el.addClass('newsRotatorTooltip');
		el.setStyles( {
			position:'absolute',
			visibility : 'hidden',
			'z-index':100000			
		});
		this.config.html.main.adopt(el);
	},
	_parseNews : function() {
		this.config.html.stories = this.config.html.main.getElements('.newsRotatorStory');
		for(var i=0;i<this.config.html.stories.length;i++) {
			this.config.html.stories[i].setStyles( {
				position:'absolute',
				'z-index' : this.config.html.stories.length-i,
				left:0,
				top:0
			}			
			);
		}			
	},
	_displayTooltip : function(e) {
		var storyObj = this.config.html.stories[e.target.getProperty('storyIndex')];
		var txt = '';
		
		var heading = storyObj.getElements('.newsRotatorHeading')[0];
		var ingress = storyObj.getElements('.newsRotatorIngress')[0];
		if(heading) {
			txt = txt + '<b>' + heading.get('text') + '</b><br>';
		}
		if(ingress) {
			txt = txt + ingress.get('text');
		}
		if(!txt)return;
		
		this.config.html.hint.set('html',txt);
		this.config.html.hint.setStyle('visibility','visible');
		
	},
	_hideTooltip : function() {
		
		this.config.html.hint.setStyle('visibility','hidden');
	},
	_createNavigatorPane : function() {
		var el = this.config.html.navigator = new Element('div');
		el.addClass('newsRotatorNavigator');	
		this.config.html.main.adopt(el);
		el.setStyle('z-index',100000);
		
		for(var i=this.config.html.stories.length-1;i>=0;i--) {
			var linkDiv = new Element('div');
			linkDiv.addClass('newsRotatorNavigatorLinks');
			el.adopt(linkDiv);
			
			var link = new Element('a');
			link.set('html',(i+1));
			link.setProperty('href','#');
			linkDiv.adopt(link);
			link.setProperty('id',this.config.html.main.getProperty('id') + '_link' + i);
			link.addEvent('click',this._clickOnNavigationLink.bind(this));
			linkDiv.addEvent('mouseover',this._displayTooltip.bind(this));
			linkDiv.addEvent('mouseout',this._hideTooltip.bind(this));
			link.setProperty('storyIndex',i);
			linkDiv.setProperty('storyIndex',i);
			if(i==0) {
				this._setActiveStory(i);	
			}
		}
		
	},
	_getSlideMethod : function() {
		if(this.config.userProperties.slide!='random')return this.config.userProperties.slide.getRandom();
		var num = Math.floor(Math.random()*8);
		switch(num) {
			case 0:
				return 'fromLeft';
			case 1:
				return 'fromTop';
			case 2:
				return 'fromRight';
			case 3:
				return 'fromBottom';
			case 4:
				return 'fromTopLeft';
			case 5:
				return 'fromTopRight'
			case 6:
				return 'fromBottomLeft'
			case 7:
				return 'fromBottomRight'
		}				
		
	}
	,
	_slideInStory : function() {
		var slideMethod = this._getSlideMethod();
		var storyObj = this.config.html.stories[this.config.currentStoryIndex];
		var position =  { 
			'z-index' : this.config.currentZIndex++ 
		};
		slideMethod = slideMethod.toLowerCase();
		switch(slideMethod) {
			case 'fromleft':
				position.left = 0-this.config.html.main.offsetWidth;
				position.top = '0';
				break;
			case 'fromtop':
				position.left = 0;
				position.top = 0-this.config.html.main.offsetHeight;
				break;
			case 'fromright':
				position.left = this.config.html.main.offsetWidth;
				position.top = 0;
				break;
			case 'frombottom':
				position.top = this.config.html.main.offsetHeight;
				position.left =0;
				break;
			case 'frombottomleft':
				position.top = this.config.html.main.offsetHeight;
				position.left = 0-this.config.html.main.offsetWidth;
				break;
			case 'frombottomright':
				position.top = this.config.html.main.offsetHeight;
				position.left = this.config.html.main.offsetWidth;
				break;
			case 'fromtopleft':
				position.top = 0-this.config.html.main.offsetHeight;
				position.left = 0-this.config.html.main.offsetWidth;
				break;
			case 'fromtopright':
				position.top = 0-this.config.html.main.offsetHeight;
				position.left = this.config.html.main.offsetWidth;
				break;
		}
		
		storyObj.setStyles(position);
		this._slide();		
	},
	_slide : function() {
		this._setSlideCoordinates();
		this._performSlide();		
	},
	_performSlide : function() {
		if(this.config.inSlideProperties.currentIndex<this.config.inSlideProperties.numberOfCoordinates) {

			var storyObj = this.config.html.stories[this.config.currentStoryIndex];
			storyObj.setStyles( {
				left : this.config.inSlideProperties.coordinates[this.config.inSlideProperties.currentIndex].left,
				top : this.config.inSlideProperties.coordinates[this.config.inSlideProperties.currentIndex].top					
			});	
			window.refToNewsFlasher = this;
			setTimeout('window.refToNewsFlasher._performSlide()',20);	
		}
		this.config.inSlideProperties.currentIndex++;
	},
	_setSlideCoordinates : function() {
		var numberOfCoordinates = Math.ceil(this.config.userProperties.duration / 20);
		var storyObj = this.config.html.stories[this.config.currentStoryIndex];
		var changes = {
			left : (storyObj.getStyle('left').toInt() * -1) / numberOfCoordinates,
			top : (storyObj.getStyle('top').toInt() * -1) / numberOfCoordinates
		}
		
		
		var initCoordinates = {
			left: storyObj.getStyle('left').toInt(),
			top : storyObj.getStyle('top').toInt()
		} 
		this.config.inSlideProperties.currentIndex = 0;
		this.config.inSlideProperties.coordinates = [];
		this.config.inSlideProperties.numberOfCoordinates = numberOfCoordinates;
		var coord = this.config.inSlideProperties.coordinates;
		for(var i=0;i<numberOfCoordinates;i++) {
			initCoordinates.left += changes.left;
			initCoordinates.top += changes.top;
			if(i==numberOfCoordinates.length-1) {
				initCoordinates.left = initCoordinates.top = 0;
			}
			coord[coord.length] =  {
				left : initCoordinates.left,
				top : initCoordinates.top	
			}			
		}		
	}
	,
	_clickOnNavigationLink : function(e) {
		this._hideTooltip();
		var storyIndex = $(e.target).getProperty('storyIndex');
		if(storyIndex==this.config.currentStoryIndex)return;
		this._setActiveStory(storyIndex);
		this._slideInStory();
		return false;	
	}
	,
	_setActiveStory : function(indexOfStory) {
		if(this.config.currentStoryIndex!==false) {
			$(this.config.html.main.getProperty('id') + '_link' + this.config.currentStoryIndex).removeClass('newsRotatorNavigatorActiveLink');	
		}			
		this.config.currentStoryIndex = indexOfStory;
		$(this.config.html.main.getProperty('id') + '_link' + this.config.currentStoryIndex).addClass('newsRotatorNavigatorActiveLink');	
	}
});

Partial Class videos
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer
    Public FileName As String = ""
    Public Background As String = ""
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Videos()
    End Sub
    Sub Videos()
        Dim CountryCode As String = ""
        Dim Show As String = ""

        Dim tmpId As String = ""
        Dim Id As String = ""
        Dim PageId As Integer = 1

        If Not Request.QueryString("id") Is Nothing Then
            tmpId = Request.QueryString("id")
            Id = Request.QueryString("id")
        End If

        If Not Request.QueryString("page") Is Nothing Then
            PageId = Request.QueryString("page")
        End If

        If Not Request.QueryString("show") Is Nothing Then
            Show = Request.QueryString("show").ToLower
        End If

        If Id = "" Then
            SqlQuery = New SqlCommand("Select Top 1 a.Id, (Case When a.ShortName <> '' Then a.ShortName Else a.Name End) As Name, a.FeatureImage As Image, a.description From SSZGeneral.dbo.broadBandVideo a INNER JOIN SSZGeneral.dbo.broadBandVideoCategories b ON a.category = b.Id Where (b.Sport = 4) AND (a.Name Like '%Bloem%') And (a.Active = 1) Order By Id Desc", DatabaseConn)
            'SqlQuery = New SqlCommand("Select Top 1 a.Id From SSZGeneral.dbo.BroadBandVideo a INNER JOIN SSZGeneral.dbo.BroadBandVideoCategories b ON b.Id = a.Category Where (a.SuperSportUnited = 1) And (a.Active = 1) Order By a.Created Desc, a.Id Desc", DatabaseConn)
            Id = SqlQuery.ExecuteScalar
        End If
        ' SqlQuery = New SqlCommand("Select Top 1 Id, (Case When ShortName <> '' Then ShortName Else Name End) As Name, FeatureImage As Image From SSZGeneral.dbo.broadBandVideo Where (Name Like '%Bloem%') And (Active = 1) Order By Id Desc", DatabaseConn)
        SqlQuery = New SqlCommand("Select * From SSZGeneral.dbo.BroadBandVideo Where (Id = " & Id & ")", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            Dim Display As Boolean = True
            If Display = True Then
                ltl_headline.Text = RsRec("Name")
                Dim ClipLength As String = ""
                If RsRec("Length") <> "" And IsNumeric(RsRec("Length")) Then
                    Dim tmpLength As Integer = RsRec("Length")
                    If tmpLength <= 60 Then
                        ClipLength = "0:" & tmpLength.ToString
                    Else
                        If tmpLength Mod 60 > 0 Then
                            If tmpLength Mod 60 < 10 Then
                                ClipLength = (tmpLength - (tmpLength Mod 60)) / 60.ToString & ":0" & tmpLength Mod 60
                            Else
                                ClipLength = (tmpLength - (tmpLength Mod 60)) / 60.ToString & ":" & tmpLength Mod 60
                            End If
                        Else
                            ClipLength = (tmpLength - (tmpLength Mod 60)) / 60.ToString & ":00"
                        End If
                    End If
                End If
                ltl_length.Text = ClipLength
                ltl_headline.Text = RsRec("Name")
                ltl_detail.Text = RsRec("Description")
                FileName = RsRec("Video").Replace(".wmv", "")
                Background = RsRec("Preview")
            End If
        End While
        RsRec.Close()
        SqlQuery = New SqlCommand("Select Top 24 a.Id, (Case When a.ShortName <> '' Then a.ShortName Else a.Name End) As Name, a.FeatureImage As Image, a.description From SSZGeneral.dbo.broadBandVideo a INNER JOIN SSZGeneral.dbo.broadBandVideoCategories b ON a.category = b.Id Where (b.Sport = 4) AND (a.Name Like '%Bloem%') And (a.Active = 1) Order By Id Desc", DatabaseConn)
        'SqlQuery = New SqlCommand("Select Top 24 a.Id, (Case When a.ShortName <> '' Then ShortName Else a.Name End) As Name, a.Created, (Case When a.ThumbNailImage = '' Then 'ss_logo_thumbnail.jpg' Else a.ThumbNailImage End) As Image, Description From SSZGeneral.dbo.BroadBandVideo a INNER JOIN SSZGeneral.dbo.BroadBandVideoCategories b ON b.Id = a.Category Where (a.Active = 1) And (a.SuperSportUnited = 1) Order By a.Created Desc, a.Id Desc", DatabaseConn)

        Dim da As SqlDataAdapter = New SqlDataAdapter
        Dim ds As DataSet = New DataSet
        Dim tmpSource As PagedDataSource = New PagedDataSource
        da.SelectCommand = SqlQuery
        da.Fill(ds, "videos")
        Dim Total As Integer = ds.Tables("videos").Rows.Count
        Dim dv As DataView = ds.Tables("videos").DefaultView
        tmpSource.DataSource = dv
        tmpSource.AllowPaging = True
        tmpSource.PageSize = 6
        tmpSource.CurrentPageIndex = PageId - 1
        dlVideos.DataSource = tmpSource
        dlVideos.DataBind()
        da.Dispose()
        ds.Dispose()

        If tmpSource.IsFirstPage = False Then
            ltl_prev.Text = "<a href='videos.aspx?page=" & PageId - 1 & "'>Previous</a>"
        End If

        If tmpSource.PageCount > 1 Then
            Dim I As Integer
            For I = 1 To tmpSource.PageCount
                ltl_pages.Text &= "<a style='padding-right: 15px;' href='videos.aspx?page=" & I & "'>" & I & "</a>"
            Next
        End If

        If tmpSource.IsLastPage = False Then
            ltl_next.Text = "<a href='videos.aspx?page=" & PageId + 1 & "'>Next</a>"
        End If

    End Sub

    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="photogallery.aspx.vb" Inherits="pics_photogallery" MasterPageFile="master_1.master" %>
<%@ MasterType VirtualPath="master_1.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">




<div class="body_art_content"> 
<div class="column_title">Photogallery</div>




  <script LANGUAGE="JavaScript">
    function gallery(galleryid){
        window.open("photogalleryimage.aspx?id="+galleryid+"", galleryid, "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,copyhistory=0,width=600,height=600,top=50,left=200")
    }
    </script>
    <table cellspacing="0" cellpadding="0" border="0" align="center" width="590px">

    <tr>
        <td class="content"><div class="content">
            <asp:Panel id="pnl1" runat="server" EnableViewState="False">
                <asp:Repeater Id="rptGalleries" Runat="Server" Visible="False" EnableViewState="False">
	                <HeaderTemplate>
		                <table cellspacing="0" cellpadding="5" border="0" align="center" width="590px">
	                </HeaderTemplate>
	                <ItemTemplate>
		                <tr>
			                <td bgcolor="#FFFFFF" ><a href="photogallery.aspx?id=<%# Container.DataItem("Id") %>"><img src="http://images.supersport.com/<%# Container.DataItem("SmallImage") %>" alt="" align="left"   width="80px" height="60px" border="0" style="padding-right:10px"/></a><a href="photogallery.aspx?id=<%# Container.DataItem("Id") %>"><b><%# Container.DataItem("ValHeadline") %></b></a><br /><%# Container.DataItem("ValDescription") %></td>
		                </tr>
	                </ItemTemplate>
	                <AlternatingItemTemplate>
	                    <tr>
			                <td class='fix'><a href="photogallery.aspx?id=<%# Container.DataItem("Id") %>"><img src="http://images.supersport.com/<%# Container.DataItem("SmallImage") %>" alt="" align="left" width="80px" height="60px" border="0" style="padding-right:10px"/></a><a href="photogallery.aspx?id=<%# Container.DataItem("Id") %>"><b><%# Container.DataItem("ValHeadline") %></b></a><br /><b><%# Container.DataItem("ValDescription") %></td>
		                </tr>
	                </AlternatingItemTemplate>
	                <FooterTemplate>
		                </table>
	                </FooterTemplate>
                </asp:Repeater>	
            </asp:Panel>
            
            <asp:Panel id="pnl2" runat="server" EnableViewState="False">
                <asp:Literal id="ltlPhotos" runat="server" EnableViewState="False" />
            </asp:Panel>
        </div></td>
    </tr>

    </table>
</div>
<div style="clear:both;"></div>
 <!-- end body -->

<!-- end body art content -->

</asp:Content>

﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System
Imports System.Collections

Partial Class VotePollHandler
    Inherits System.Web.UI.Page

    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Private Print As Boolean = False
    Public Shadows Id As String = ""
    Dim Counter As Integer
    Dim Count As Integer
    Dim FullResultsPageOutput As Boolean = True
    Dim CurrentPollId As String = "-1"
    Dim VoteId As String = "-1"
    Dim ProcessVote As Boolean = False

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call ValidateQueryString()
        If ProcessVote = True Then
            Call SubmitVote()
        End If
        Call BuildResults()
    End Sub

    Sub ValidateQueryString()
        If Request.QueryString Is Nothing Then
            Return
        Else
            'Process the querystring'
            If Request.QueryString("getresults") Is Nothing Then
            Else
                If Request.QueryString("getresults").ToString() = "1" Then
                    FullResultsPageOutput = False
                End If
            End If

            'Process the vote flag'
            If Request.QueryString("dovote") Is Nothing Then
            Else
                If Request.QueryString("dovote").ToString() = "1" Then
                    ProcessVote = True
                End If
            End If

            'Process the poll id'
            If Request.QueryString("pollid") Is Nothing Then
            Else
                CurrentPollId = Request.QueryString("pollid").ToString()
            End If

            'Process the poll id'
            If Request.QueryString("voteid") Is Nothing Then
            Else
                VoteId = Request.QueryString("voteid").ToString()
            End If
        End If
    End Sub

    Sub SubmitVote()
        'Submit the vote to the database for this poll'
        Dim RowsAffected As Integer = -1
        SqlQuery = New SqlCommand("UPDATE SuperSportZone.dbo.ZoneVotePollOptions SET Votes = Votes + 1 WHERE VotePollId = " & CurrentPollId & " AND Id = " & VoteId & "", DatabaseConn)
        RowsAffected = SqlQuery.ExecuteNonQuery()
    End Sub

    Sub BuildResults()
        Dim RsRec As SqlDataReader
        Dim PollQuestion As String = ""
        Dim Votes As Integer = 0

        SqlQuery = New SqlCommand("Select Sum(Votes) As Votes From SuperSportZone.dbo.ZoneVotePollOptions Where (VotePollId = " & CurrentPollId & ")", DatabaseConn)
        Votes = SqlQuery.ExecuteScalar

        SqlQuery = New SqlCommand("Select Text, Votes From SuperSportZone.dbo.ZoneVotePollOptions Where (VotePollId = " & CurrentPollId & ") Order By Number", DatabaseConn)
        RsRec = SqlQuery.ExecuteReader

        ltl_polls.Text &= "<tr>"
        While RsRec.Read
            Dim Percentage As Double = 0
            Dim tmpVotes As Integer = 0
            tmpVotes = RsRec("Votes")
            If tmpVotes > 0 Then
                Percentage = (tmpVotes / Votes) * 100
                Percentage = Math.Round(Percentage, 0)
            End If
            ltl_polls.Text &= "<tr>"
            ltl_polls.Text &= "<td width='60%' style='white-space:nowrap;'>" & RsRec("Text") & "</td>"
            ltl_polls.Text &= "<td width='40%'><div style='float: right; background-color: #010066; width: " & Percentage * 5 & "px;'></div><div style='float: right; padding-right: 75px;'>" & Percentage & "%</div></td>"
            ltl_polls.Text &= "</tr>"
        End While
        RsRec.Close()

        ltl_polls.Text &= "<tr>"
        ltl_polls.Text &= "<td colspan='2'><u>Total votes:</u> <b>" & Votes & "</b></td>"
        ltl_polls.Text &= "</tr>"
    End Sub

    Sub Page_UnLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="videos.aspx.vb" Inherits="videos" MasterPageFile="master_1.master" %>
<%@ MasterType VirtualPath="master_1.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
<div class="body_art_content"> 
<div class="column_title">Videos<br /></div>
 <table cellspacing="0" cellpadding="0" border="0" align="center" width="590px">
    <tr>
        <td colspan="3"  style="padding-left: 2px;">
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="550">
            <tr>
                <td width="350" valign="top" style="padding-top: 5px;">
                
                
                
                
                 <script language="JavaScript">
					<!--
					    if ( navigator.appName == "Netscape" )
					    {
					        //-- This next line ensures that any plug-ins just installed are updated in the browser
					        //-- without quitting the browser.
					        navigator.plug-ins.refresh();
					        // We don't need the APPLET within IE
					        // ***Please note that if you do not need to script events, you can safely remove the next two lines
					        document.write("\x3C" + "applet MAYSCRIPT Code=NPDS.npDSEvtObsProxy.class")
					        document.writeln(" width=5 height=5 name=appObs\x3E \x3C/applet\x3E")
					    }
					//-->
					</script> 
		            <object id="MediaPlayer" width="320" height="265" classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112" standby="Loading Microsoft Windows Media Player components..." type="application/x-oleobject">
		              <param name="FileName" value="asxgen.asp?file=<% =FileName %>">
		              <param name="ShowControls" value="1">
		              <param name="ShowDisplay" value="0">
		              <param name="ShowStatusBar" value="0">
		              <param name="TransparentAtStart" value="1">
		              <param name="AutoSize" value="1"><embed type="application/x-mplayer2" pluginspage="http://www.microsoft.com/windows/windowsmedia/download/" filename="asxgen.asp?file=<% =FileName %>" src="asxgen.asp?file=<% =FileName %>" Name="MediaPlayer" ShowControls="1" ShowDisplay="0" ShowStatusBar="0" width="320" height="240" TransParentAtStart="1">
		            </object>

                </td>
                <td width="10" valign="top"></td>
                <td width="200" valign="top" style="padding-top: 8px;">
                                   <div id="newstext">
                <font color="#0E6227"><strong><asp:Literal id="ltl_headline" runat="server" EnableViewState="False" /></strong></font><br />
                <br />
                <asp:Panel Id="pnl_length" runat="server" EnableViewState="False" Visible="True">
                <b>Clip Length: </b><asp:Literal id="ltl_length" runat="server" EnableViewState="False" /><br />
                </asp:Panel>
                <b>Descritpion:</b> <asp:Literal id="ltl_detail" runat="server" EnableViewState="False" />
                </div>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" height="4"></td>
    </tr>
    <tr>
        <td colspan="3"  style="padding-left: 2px;"  >
        <table cellspacing="0" cellpadding="1" border="0"  width="100%">
            <tr>
                <td align="center" style="padding-top: 7px;">
                    <asp:DataList id="dlVideos" RepeatColumns="2" RepeatDirection="Horizontal" runat="Server" Visible="True" Width="97%" CellSpacing="5" Height="100%" CellPadding="7" ItemStyle-VerticalAlign="top" ItemStyle-Width="50%" BackColor="#ffffff" ItemStyle-BackColor="#ffffff" HorizontalAlign="Center" ItemStyle-BorderColor="#C2DEC0" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px">
                        <ItemTemplate>
                            <img src='http://cdn.dstv.com/supersport.img/videoimages/<%#Container.DataItem("image")%>' width="80" height="60" align="left" style="border-right: 5px solid #ffffff; " /><a href="videos.aspx?id=<%#Container.DataItem("id")%>"><b><%#Container.DataItem("name")%></b></a><br /><%#Container.DataItem("description")%>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
            <tr>
                <td class='fix'>
                    <table width="70px" border="0" cellpadding="0" cellspacing="1"  align="left">
                        <tr>
                            <td >
                                <table width="80%" cellspacing="1" cellpadding="0" border="0" align="left">
                                    <tr>
                                        <td width="33%" align="left" style="font-weight:bold"><asp:Literal id="ltl_prev" runat="server" EnableViewState="False" /></td>
                                        <td width="33%" align="center" style="font-weight:bold"><asp:Literal id="ltl_pages" runat="server" EnableViewState="False" /></td>
                                        <td width="33%" align="right" style="font-weight:bold"><asp:Literal id="ltl_next" runat="server" EnableViewState="False" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>  
    </table>
    </div>
<div style="clear:both;"></div>
 <!-- end body -->
</asp:Content>
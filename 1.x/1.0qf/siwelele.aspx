<%@ Page Language="VB" AutoEventWireup="false" CodeFile="siwelele.aspx.vb" Inherits="siwelele" MasterPageFile="master_1.master" %>
<%@ MasterType VirtualPath="master_1.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<div class="body_cat_content">

<div class="cat_subhead"><img src="pics/bloem_celtic_news.jpg" border="0" /></div>

<asp:DataList id="dl_stories" RepeatColumns="2" RepeatDirection="Horizontal" runat="Server" Visible="True" BackColor="#FFFFFF" Width="100%" CellSpacing="1" CellPadding="3" ItemStyle-Width="50%">
  
    <ItemTemplate >
        <div style="padding-top: 5px;">
            <div id='left'><a href="articles.aspx?id=<%#Container.DataItem("Id")%>"><span style="color:#00743C; font-size:13px; font-weight:bold; margin:0; padding:0;text-decoration:none;"><%#Container.DataItem("Headline")%></span></a><br /><span style='color:#9C9C9C; margin:0; padding:0 0 3px 0'><%#Container.DataItem("Created")%></span><br /><span style='line-height:16px; color:#4D4D4D;'><%#Container.DataItem("Blurb")%></span> <a href="articles.aspx?id=<%#Container.DataItem("Id")%>">read more..</a></div>
       </div>
     
    </ItemTemplate>
    <AlternatingItemTemplate>
     <div style="padding-top: 5px;">
            <div id='right'><a href="articles.aspx?id=<%#Container.DataItem("Id")%>"><span style="color:#00743C; font-size:13px; font-weight:bold; margin:0; padding:0;text-decoration:none;"><%#Container.DataItem("Headline")%></span></a><br /><span style='color:#9C9C9C; margin:0; padding:0 0 3px 0'><%#Container.DataItem("Created")%></span><br /><span style='line-height:16px; color:#4D4D4D;'><%#Container.DataItem("Blurb")%></span> <a href="articles.aspx?id=<%#Container.DataItem("Id")%>">read more..</a></div>
        </div>
 
    </AlternatingItemTemplate>
  
</asp:DataList>

<!-- end body cat content -->
<div class></div>
<div style="clear:both;"></div>
</div> <!-- end body -->



<!-- end body art content -->

</asp:Content>


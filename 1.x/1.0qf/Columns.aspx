<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Columns.aspx.vb" Inherits="Columns" MasterPageFile="master_1.master" %>
<%@ MasterType VirtualPath="master_1.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<div class="body_art_content"> 
<div class="column_title"><asp:Label ID="ltlcolumnheader" runat="server" /></div>
<div class="date"><asp:Label ID="ltlcolumndate" runat="server" /></div>

<div id="column_story">

<div id="column_story_wrapper">
<div class="image">
<asp:Label ID="ltlcolumnimage" runat="server" />
</div>
</div>

  <asp:Label Id="ltlcolumnbody" runat="server" />
</div>
<div style="clear:both;"></div>
</div> <!-- end body -->

<div class="body_art_content"> 
<div class="column_title"><asp:Label ID="ltlheadline2" runat="server" /></div>
<div class="more"><asp:Label ID="ltlheadline1" runat="server" /></div>
</div>

<!-- end body art content -->

</asp:Content>


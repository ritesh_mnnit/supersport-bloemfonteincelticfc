
Partial Class fixtures
    Inherits System.Web.UI.Page
    Private SiteNumber As Integer = System.Configuration.ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Sub Page_Load(ByVal sedner As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Dim Id As String = ""
        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If
        fixtures()
    End Sub
    Private Sub Fixtures()
        ltlFixtures.Text &= "<table width='590px' cellspacing='1' cellpadding='2' border='0'>"

        Dim LeagueName As String = ""
        Dim CurrentMonth As String = ""
        Dim Id As String = ""
        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If

        SqlQuery = New SqlCommand("SELECT Distinct  '' As Id,(Case When  c.LeagueId = 155 OR  e.LeagueId = 155 Then 5 When  c.LeagueId = 189 OR  e.LeagueId = 189 Then 2  Else 1 End) As Ordering, a.MatchDateTime, b.Name As HomeTeam,c.Name As AwayTeam,d.Name As League, e.Name As Venue,Year(a.MatchDateTime),Month(a.MatchDateTime),Day(a.MatchDateTime) As MatchDay, a.Postponed ,a.Completed FROM Soccer.dbo.Matches a INNER JOIN Soccer.dbo.TeamsByLeague b ON b.Id = a.TeamAId INNER JOIN Soccer.dbo.TeamsByLeague c ON c.Id = a.TeamBId INNER JOIN Soccer.dbo.Leagues d ON d.Id = a.LeagueId INNER JOIN Soccer.dbo.VenuesByLeague e ON e.Id = a.VenueId WHERE (b.ParentId = 121 OR c.ParentId = 121) AND (c.LeagueId <> 123 OR e.LeagueId <> 123) AND (c.LeagueId <> 334 OR e.LeagueId <> 334) AND (a.Completed = 0) AND (b.Name LIKE '%Bloem%' OR c.Name LIKE '%Bloem%') UNION Select Distinct a.Id,'' As Odering ,a.MatchDateTime, a.HomeTeamName As HomeTeam,a.AwayTeamname As AwayTeam,b.Competition_Name As League,a.Venue,Year(a.MatchDateTime), Month(a.MatchDateTime),Day(a.MatchDateTime) As MatchDay,(Case When StatusId = 14 Then 1 Else 0 End) As Postponed,a.Result From Soccer.dbo.pa_Matches a INNER JOIN Soccer.dbo.pa_Competitions b ON b.Competition_Id = a.Competition Where ((a.Competition = 794) or (a.Competition = 880)) And (a.Result = 0) AND (a.HomeTeamId =49944 OR a.AwayTeamId =49944) AND (b.Competition_Season = a.Season) AND (a.StatusId < 14) ORDER BY a.MatchDateTime", DatabaseConn)

        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        If RsRec.HasRows Then
            While RsRec.Read
                Dim MatchDateTime As DateTime = RsRec("MatchDateTime")

                If MatchDateTime.ToString("MMMM") <> CurrentMonth Then
                    ltlfixtures.Text &= "<tr>"
                    ltlFixtures.Text &= "<td colspan='6' class='fix' style='font-weight:bold;'>" & MatchDateTime.ToString("MMMM") & "</td>"
                    ltlfixtures.Text &= "</tr>"
                End If

                ltlfixtures.Text &= "<tr>"
                ltlFixtures.Text &= "<td width='15' align='center' class='fix'>" & Day(RsRec("MatchDateTime")) & "</td>"
                If MatchDateTime.Hour = 0 And MatchDateTime.Minute = 0 Then
                    ltlFixtures.Text &= "<td class='fix' align='center'></td>"
                Else
                    ltlFixtures.Text &= "<td class='fix' align='center'>" & MatchDateTime.ToString("HH':'mm") & "</td>"
                End If
                ltlFixtures.Text &= "<td class='fix'>" & RsRec("HomeTeam") & "</td>"
                If RsRec("PostPoned") = True Then
                    ltlFixtures.Text &= "<td width='20' align='center' class='fix'><b>P-P</b></td>"
                Else
                    ltlFixtures.Text &= "<td width='20' align='center' class='fix'><b>v</b></td>"
                End If
                ltlFixtures.Text &= "<td class='fix'>" & RsRec("AwayTeam") & "</td>"
                ltlFixtures.Text &= "<td class='fix'>" & RsRec("Venue") & "</td>"
                ltlfixtures.Text &= "</tr>"
                CurrentMonth = MatchDateTime.ToString("MMMM")

            End While
        Else
            ltlFixtures.Text &= "<tr><td class='fix' style='font-weight:bold;text-align:center'>There are no upcoming fixtures</td></tr>"
        End If
        RsRec.Close()
        ltlFixtures.Text &= "<tr><td><a href=""javascript:history.go(-1)"" style=""font-weight:bold;"">Back</a></td></tr>"
        ltlfixtures.Text &= "</table>"
    End Sub
    Private Sub Page_UnLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class


Partial Class Default2
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Articles(ID)
    End Sub
    Sub Articles(ByVal Id As String)
        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If
        'SqlQuery = New SqlCommand("Select Top 1 *  from articlebreakdown where (Id = " & Request.QueryString("id") & ")", DatabaseConn)
        SqlQuery = New SqlCommand("Select Top 1 *  from articlebreakdown where (Id = @Id)", DatabaseConn)
        SqlQuery.Parameters.Add("@Id", Request.QueryString("id"))

        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlarticleimage.Text = "<img src='http://images.supersport.com/" & RsRec("LargeImage") & "' border=""0"" />"
            ltlarticleheadline.Text = "" & RsRec("Headline") & ""
            Const pattern As String = "(\[embed:)(.+)(\])"
            Dim body As String = RsRec("body").ToString()
            body = Regex.Replace(body, pattern, String.Empty, RegexOptions.IgnoreCase)
            ltlarticlebody.Text = "" & body & ""
            ltlarticlecaption.Text = "" & RsRec("SmallImageAlt") & ""
            ltlarticledate.Text = "" & RsRec("Articledate") & ""

        End While
        RsRec.Close()
    End Sub

    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub


End Class

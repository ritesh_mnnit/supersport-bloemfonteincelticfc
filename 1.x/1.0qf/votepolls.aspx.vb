
Partial Class votepolls
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Private Print As Boolean = False
    Public Shadows Id As String = ""
    Dim Counter As Integer
    Dim Count As Integer
    Dim FullResultsPageOutput As Boolean = True

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call VotePoll()
    End Sub

    Sub VotePoll()

        ltl_category.Text = "<div class=""body_art_content"">"
        ltl_category.Text = "<div class=""art_title""><asp:Label ID=""ltlcontentheadline"" runat=""server"" /></div>"

        ltl_category.Text = "<div id=""art_story"">"
        ltl_category.Text = "Recent Polls"
        ltl_category.Text = "</div>"
        ltl_category.Text = "<div style=""clear:both;""></div>"
        ltl_category.Text = "</div> "

        SqlQuery = New SqlCommand("Select VotePoll From SuperSportZone.dbo.ZoneSiteConfiguration Where (Site = " & SiteId & ")", DatabaseConn)
        Dim VoteCategory As Integer = SqlQuery.ExecuteScalar

        Dim Polls As ArrayList = New ArrayList
        SqlQuery = New SqlCommand("Select Top 7 a.Id From SuperSportZone.dbo.ZoneVotePolls a INNER JOIN SuperSportZone.dbo.ZoneCategories b ON b.Id = a.Category INNER JOIN SuperSportZone.dbo.ZoneSites c ON c.Id = b.Site Where (a.Category = 692) AND (a.Active = 1) Order By a.UserDate Desc, a.Id Desc", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            Polls.Add(RsRec("Id"))
        End While
        RsRec.Close()
        Polls.TrimToSize()

        Dim IEnum As IEnumerator = Polls.GetEnumerator
        Dim Count As Integer = 1
        While IEnum.MoveNext
            Dim PollId As Integer = IEnum.Current
            Dim PollQuestion As String = ""
            Dim Votes As Integer = 0
            SqlQuery = New SqlCommand("Select Question From SuperSportZone.dbo.ZoneVotePolls Where (Id = " & PollId & ")", DatabaseConn)
            PollQuestion = SqlQuery.ExecuteScalar

            SqlQuery = New SqlCommand("Select Sum(Votes) As Votes From SuperSportZone.dbo.ZoneVotePollOptions Where (VotePollId = " & PollId & ")", DatabaseConn)
            Votes = SqlQuery.ExecuteScalar

            ltl_polls.Text &= "<tr>"
            ltl_polls.Text &= "<td style='color: #C0C0C0; font-weight: bold;padding-left:10px;'>Question:<br />"
            ltl_polls.Text &= "</tr>"
            ltl_polls.Text &= "<tr>"
            ltl_polls.Text &= "<td style='padding-left:12px;'>" & PollQuestion & " - <a href=""javascript:void(0);"" onclick=""results(" & Count & ");"" style=""color: #c21717;"">view results</a><br />"

            ltl_polls.Text &= "<div style='display: none; padding: 10px 0 0 50px;' id='poll" & Count & "'>"
            ltl_polls.Text &= "<table cellspacing='3' cellpadding='0' border='0'>"
            ltl_polls.Text &= "<tr>"
            ltl_polls.Text &= "<td style='color:#c21717;font-weight: bold;'>Results:</td>"
            ltl_polls.Text &= "</tr>"
            SqlQuery = New SqlCommand("Select Text, Votes From SuperSportZone.dbo.ZoneVotePollOptions Where (VotePollId = " & PollId & ") Order By Number", DatabaseConn)
            RsRec = SqlQuery.ExecuteReader
            While RsRec.Read
                Dim Percentage As Double = 0
                Dim tmpVotes As Integer = 0
                tmpVotes = RsRec("Votes")
                If tmpVotes > 0 Then
                    Percentage = (tmpVotes / Votes) * 100
                    Percentage = Math.Round(Percentage, 0)
                End If
                ltl_polls.Text &= "<tr>"
                ltl_polls.Text &= "<td>" & RsRec("Text") & "</td>"
                ltl_polls.Text &= "</tr>"
                ltl_polls.Text &= "<tr>"
                ltl_polls.Text &= "<td><div style='float: left; background-color: #010066; width: " & Percentage * 5 & "px;'></div><div style='float: left; padding-left: 8px;'>" & Percentage & "%</div></td>"
                ltl_polls.Text &= "</tr>"
            End While
            RsRec.Close()
            ltl_polls.Text &= "<tr>"
            ltl_polls.Text &= "<td height='7'></td>"
            ltl_polls.Text &= "</tr>"
            ltl_polls.Text &= "<tr>"
            ltl_polls.Text &= "<td>Total votes: <b>" & Votes & "</b></td>"
            ltl_polls.Text &= "</tr>"
            ltl_polls.Text &= "</table>"
            ltl_polls.Text &= "</div>"

            ltl_polls.Text &= "</td>"
            ltl_polls.Text &= "</tr>"
            ltl_polls.Text &= "<tr>"
            ltl_polls.Text &= "<td height='15'></td>"
            ltl_polls.Text &= "</tr>"

            Count = Count + 1
        End While

        ltl_script.Text = "var count = " & Count - 1 & ";"

    End Sub

    Sub Page_UnLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload

        DatabaseConn.Close()

    End Sub

End Class
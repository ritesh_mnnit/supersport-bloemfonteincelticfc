
Partial Class pics_photogallery
    Inherits System.Web.UI.Page
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private SiteNumber As Integer = System.Configuration.ConfigurationManager.AppSettings("Site")
    Private Category As String = "693"
    Public TableWidth As Integer = System.Configuration.ConfigurationManager.AppSettings("SmallTableWidth")
    Public TopImage As String = "BloemCeltic_adTop"
    Public BottomImage As String = "BloemCeltic_adBottom"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim Id As String = ""

        If Not Request.QueryString("cat") Is Nothing Then
            Category = Request.QueryString("cat")
        End If

        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If

        DatabaseConn.Open()

        If Id <> "" Then
            Photos(Id)
            pnl1.Visible = False
            pnl2.Visible = True
        Else
            FrontPage()
            pnl1.Visible = True
            pnl2.Visible = False
        End If

    End Sub

    Sub Page_UnLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload

        DatabaseConn.Close()

    End Sub

    Private Sub FrontPage()

        SqlQuery = New SqlCommand("Select Distinct Top 10 a.Id, a.Teaser As SmallImage, a.Headline As ValHeadline, a.Description As ValDescription, a.Userdate From SuperSportZone.dbo.ZonePhotoGalleries a INNER JOIN SuperSportZone.dbo.ZonePhotoGalleryCategories b ON b.PhotoGallery = a.Id Where (b.Category = " & Category & ") And (a.Active = 1) Order By a.UserDate Desc, a.Id Desc", DatabaseConn)
        rptGalleries.DataSource = SqlQuery.ExecuteReader()
        rptGalleries.DataBind()
        rptGalleries.Visible = True
        rptGalleries.DataSource.Close()

    End Sub

    Private Sub Photos(ByVal Id As String)

        Dim Description = ""
        Dim Heading = ""

        SqlQuery = New SqlCommand("Select Headline As ValHeadline, Description As ValDescription From SuperSportZone.dbo.ZonePhotoGalleries Where (Id = " & Id & ")", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            Description = RsRec("ValDescription")
            Heading = RsRec("ValHeadline")
        End While
        RsRec.Close()

        SqlQuery = New SqlCommand("Select Id, SmallImage,LargeImage From SuperSportZone.dbo.ZonePhotoGalleryImages Where (PhotoGallery = " & Id & ") And (Active = 1) Order By Rank", DatabaseConn)
        RsRec = SqlQuery.ExecuteReader
        Dim Count As Integer = 1
        While RsRec.Read
            If Count = 1 Then
                ltlPhotos.Text &= "<div class='galleryheader' style='padding-bottom: 5px;'><b>" & Heading & "</b></div>"
                ltlPhotos.Text &= "<table width='100%' cellspacing='1' cellpadding='2' norder='0'>"
                ltlPhotos.Text &= "<tr>"
            ElseIf Count = 6 Then
                ltlPhotos.Text &= "</tr><tr>"
                Count = 1
            End If
            ltlPhotos.Text &= "<td align='center'><a id=""thumb1"" href=""http://images.supersport.co.za/" & RsRec("LargeImage") & """ class=""highslide"" onclick=""return hs.expand(this)""><img src=""http://images.supersport.co.za/" & RsRec("SmallImage") & """  title="""" class=""noborder"" border=""0""/></a></td>"
            Count = Count + 1
        End While
        RsRec.Close()
        ltlPhotos.Text &= "</tr></table>"

    End Sub
End Class

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" MasterPageFile="master.master"  %>
<%@ MasterType VirtualPath="master.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div id="body_container">

<div id="body">

<div id="top_story_main">

<div class="top_story_box">

<div class="image">
<div id="imager"><asp:Label ID="ltlarticleImage" runat="server" /></div>
<div id="heads"><asp:Label Id="ltlartileheader" runat="server" /></div>
<div id="blurb"><asp:Label Id="ltlarticleblurb" runat="server" /></div>
</div>

<div class="txt">
<span style="width:293px; height:30px; margin:1px 0 0 0; padding:0px 3px 0px 0px;"><asp:Label Id="ltlHeadlines" runat="server" /></span>

<asp:Label ID="ltl_stories" runat="server" />

<div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
</div>

<div id="controls">
<div class="more"> <a href="archive.aspx">>> more news</a></div>
</div> <!--end controls -->
</div>

<!-- 468 banner -->

<div id="banner_box">

<div id="banner">

<div class="banner"><asp:Label Id = "ltlBanner468x60" runat="server" /></div>

</div>

</div> <!-- end banner box -->

<div id="stats_box">

<div id="stats_table">

<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab1" tabindex="0"><img src="pics/tab_table_log.jpg" width="80" height="26" border="0" /></li>
    <li class="TabbedPanelsTab2" tabindex="0"><img src="pics/tab_fix.jpg" width="80" height="26" border="0" /></li>
    <li class="TabbedPanelsTab3" tabindex="0"><img src="pics/tab_res.jpg" width="80" height="26" border="0" /> </li>
  </ul>
  <div class="TabbedPanelsContentGroup">

<div class="TabbedPanelsContent">

<table width="596" border="1" cellspacing="0" cellpadding="0" bordercolor="#ffffff">
 
<tr>
<td class="header_rows" align="center" height="23">POS.</td>
<td class="header_rows" align="center" height="23">TEAM</td>
<td class="header_rows" align="center" height="23">P</td>
<td class="header_rows" align="center" height="23">W</td>
<td class="header_rows" align="center" height="23">D</td>
<td class="header_rows" align="center" height="23">L</td>
<td class="header_rows" align="center" height="23">GF</td>
<td class="header_rows" align="center" height="23">GA</td>
<td class="header_rows" align="center" height="23">GD</td>
<td class="header_rows" align="center" height="23">Pts</td>
</tr>

<asp:Label ID ="ltlLogs" runat="server" />
<tr><td colspan="10">
<div id="more">
<a href="logs.aspx">>> more </a>
</div>
</td></tr>
</table>
</div>

<div class="TabbedPanelsContent">

<table width="596" border="1" cellspacing="0" cellpadding="0" bordercolor="#ffffff">
 
 <asp:Label ID="ltlFixtures" runat="server" />
 <tr><td colspan="3">
 <div id="more">
<a href="fixtures.aspx">>> more </a>
</div>
</td></tr>
</table>

</div>

<div class="TabbedPanelsContent">

<table width="596" border="1" cellspacing="0" cellpadding="0" bordercolor="#ffffff">
 
 <tr>
    <td class="header_rows" align="center" height="23">score</td>
    <td class="header_rows" align="center" height="23">Team</td>
 </tr>
 <asp:Label ID="ltlpsl" runat="server" />
 <tr><td colspan="2">
<div id="more">
<a href="results.aspx">>> more </a>
</div>
</td></tr>
</table>
</div>
</div>

</div>
<div style="clear:both;"></div>
</div>

<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
//-->
</script>
</div> <!-- end stats box -->

<div id="gallery">

<div id="header"></div>

<div id="images">

<asp:Label ID="ltlphotogallery" runat="server" />
<div class="divider"></div>

</div>

<div id="more"><asp:Label ID="ltlphotogallerymore" runat="server" /></div>

</div> <!-- end gallery -->

<div style="clear:both;"></div>
</div> <!-- end body -->

<!-- START BODY 2 -->
<div id="body2">

<div id="celtic_tv">

<div id="header"></div>
<asp:Label Id="videos" runat="server" />

<div id="main_vid">


<asp:Label Id="ltlvideopic" runat="server" />
<div class="tv_main">
<div class="tv_sub"><img src="pics/play_button_main.jpg" width="66" height="53" /></div>
<div class="tv_txt"><asp:Label Id="ltlvideotxt" runat="server" /></div>
</div>
</div>

<asp:Label Id="ltlvideoheadlines" runat="server" />



</div> <!-- end Celtic Tv Box -->

<!-- Columns start -->

<div id="column">
<div id="header"></div>

<div id="stories">

<asp:Label ID="ltlcolumnpic" runat="server" />

<div class="story">
<div class="title"><asp:Label ID="ltlcolumnhead" runat="server" /></div>
<div class="summary"><asp:Label ID="ltlcolumnblurb" runat="server" /></div>
<div class="more"><asp:Label ID="ltlcolumnmore" runat="server"/>
</div>
</div>

</div>

<div style="clear:both;"></div>
</div> <!-- columns end -->

<!-- start poll -->
<div id="poll">

<div id="header"></div>

<div id="questions">

<table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <form name="vote">
                            
                            <tr>
                                <td valign="top">
                                    <table>
                                        <tr>
                                            <td class="poll_question"><span style="font-weight:bold;"><asp:Literal ID="ltl_question" runat="server" EnableViewState="false" /></span><br /></td>
                                        </tr>
                                         <tr>
                                            <td class="poll_options">
                                                <div id="polloptions" style="padding-top:3px;">
                                                    <asp:Literal ID="ltl_options" runat="server" EnableViewState="false" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td height="2px"></td></tr>
                            <tr>
                                <td>
                                
                                <asp:Panel ID="pnlbuttons" runat="server" Visible="True">
                                  <div id="polly">
                                    <a href="javascript:void(0);" onclick="votes();"><img src="pics/vote.gif"  border="0" class="poll_btn"></a>&nbsp;<a href="javascript:void(0);" onclick="voteresults(<% =VotePollId %>);"><img src="pics/view.gif" border="0"></a>
                                     </div>
                                </asp:Panel>
                                <asp:Panel id="pnlpolls" runat="server" Visible="False">
                                    <div style="text-align: center;"><a href="votepolls.aspx" style="color: #FFFFFF; font-weight: bold; font-size: 14px; font-style: italic;">previous polls</a></div>
                                </asp:Panel>
                                </td>
                            </tr>
                            </form>
                        </table>
                        </div>


</div><!-- end poll -->

<div id="reebok_banner"><asp:Label ID="ltlBanner318x298" runat="server" /></div>

<!-- newsletter start -->
<!--
<div id="newsletter">

<div id="header"></div>

<div id="info">

<div class="header">Sign up to receive Bloemfontein Celtic FC Monthly Newsletter </div>

<form>

<div class="label">
<table cellpadding="0" cellspacing="0" border="0">
<tr><td width="48">
 Name </td><td> :</td><td>  <input type="text" style="margin-left:10px;"></td></tr>
</table>
</div>

<div class="label">
<table cellpadding="0" cellspacing="0" border="0">
<tr><td>
Surname </td><td align="center" width="10"> : </td><td> <input type="text" style="margin-left:8px;"></td></tr>
</table>
</div>

</form>

</div>

<div id="buttons">
<a href="#"><img src="pics/submit.gif" border="0"></a> <a href="#"><img src="pics/reset.gif" border="0" style="margin-left:15px;"></a></div>

</div>
-->

<div style="clear:both;"></div>
</div> <!-- end BODY 2 -->


<div style="clear:both;"></div>
</div> <!-- end body container -->


<!-- START BODY 3 -->

<div id="body3">
<div id="banner">
<asp:Label ID="ltlBanner728x90" runat="server"/>
</div>


</div><!-- end Body 3-->

<!-- START BOTTOM -->
<div id="bottom">

<div id="football_news">

<div id="header"></div>

<div id="links">

<div class="link">
<ul>
<asp:Label ID="ltlarticlearchive" runat="server" />
</ul>
</div>

</div>

<div id="more">
<a href="archive.aspx"> >> more </a>
</div>

</div> <!-- end football news -->

<div id="fan_comments">

<div id="header"></div>

<div id="info">

<asp:Label ID="ltlFancomments" runat="server" />

</div>

<!--<div id="more">
<a href="photogallery.aspx?Id="> >> more </a>
</div>-->

</div> <!-- end fan comments -->


<div id="bottom_adbox">

<div id="banner"><asp:Label ID="ltlContentbox300x250" runat="server" /></div>

</div>
</asp:content>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="profiles.aspx.vb" Inherits="profiles" MasterPageFile="master_1.master" %>
<%@ MasterType VirtualPath="master_1.master" %>

<asp:Content runat="server" ContentPlaceHolderId="ContentPlaceHolder2" Id="content1">

    <asp:Panel id="pnl1" runat="server" EnableViewState="false">
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="596px">
            
            <tr>
                <td class="content"><div class="content">
                    <div class="art_title" style="padding-bottom: 5px;">Profiles</div>
                    <table width="596" cellspacing="2" cellpadding="0" border="0">
                        <tr>
                            <td width="100%" valign="top">
                                <table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#01743d">
                                    <tr>
                                        <td style="padding: 1px;color:#ffffff; padding-left: 3px; font-weight: bold;">Goalkeepers</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" style="padding: 2px; padding-top: 5px; padding-bottom: 5px;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <asp:Literal id="ltlgoalkeepers" runat="server" EnableViewState="false" />
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="7"></td>
                        </tr>
                        <tr>
                            <td width="100%" valign="top">
                                <table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#01743d">
                                    <tr>
                                        <td style="padding: 1px; padding-left: 3px; font-weight: bold;color:#ffffff;">Defenders</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" style="padding: 2px; padding-top: 5px; padding-bottom: 5px;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <asp:Literal id="ltldefenders" runat="server" EnableViewState="false" />
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="7"></td>
                        </tr>
                        <tr>
                            <td width="100%" valign="top">
                                <table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#01743d">
                                    <tr>
                                        <td style="padding: 1px; padding-left: 3px; font-weight: bold;color:#ffffff;">Midfielders</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" style="padding: 2px; padding-top: 5px; padding-bottom: 5px;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <asp:Literal id="ltlmidfielders" runat="server" EnableViewState="false" />
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="7"></td>
                        </tr>
                        <tr>
                            <td width="100%" valign="top">
                                <table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#01743d">
                                    <tr>
                                        <td style="padding: 1px; padding-left: 3px; font-weight: bold;color:#ffffff;">Forwards</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" style="padding: 2px; padding-top: 5px; padding-bottom: 5px;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <asp:Literal id="ltlforwards" runat="server" EnableViewState="false" />
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div></td>
            </tr>
           
        </table>
    </asp:Panel>
    
    <asp:Panel id="pnl2" runat="server" EnableViewState="false">
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="596px">
         
            <tr>
                <td class="content"><div class="content">
                    <div class="headline" style="padding-bottom: 7px;"><asp:Literal id="ltlname" runat="server" EnableViewState="false" /></div>
                    <asp:Literal id="ltlprofile" runat="server" EnableViewState="false" />
                </div></td>
            </tr>
            
        </table>
    </asp:Panel>
    
</asp:Content>
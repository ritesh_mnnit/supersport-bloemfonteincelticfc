Partial Class _Default
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer
    Public VotePollId As Integer = 0
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Video()
        Call Banner468x60()
        Call Banner318x298()
        Call Banner728x90()
        Call ContentBox300x250()
        Call Articles()
        Call HPArchives()
        Call Logs()
        Call Fancomments()
        Call VotePoll()
        Call Personality()
        Call Fixtures()
        Call PSL()
        Call Gallery()
    End Sub
    Sub Personality()
        SqlQuery = New SqlCommand("Select Top 1 a.*, b.Id As Author, b.Name As PersonalityName, b.Image As personalitypic From SuperSportZone.dbo.ZonePersonalityArticles a INNER JOIN SuperSportZone.dbo.ZonePersonalities b ON b.Id = a.Personality Where (a.Site = 79) And (Active = 1) Order by Created desc", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            If Count > 0 Then
                Dim tmpText As String = RsRec("Blurb")
                If tmpText.Length > 95 Then
                    tmpText = tmpText.Substring(0, 95)
                    tmpText = tmpText.Substring(0, tmpText.LastIndexOf(" ")).Trim
                    tmpText = tmpText & ""
                End If
                ltlcolumnpic.Text = "<img src='http://images.supersport.co.za/" & RsRec("smallImage") & "'>"
                ltlcolumnhead.Text = "<a href='columns.aspx?pId=" & RsRec("personality") & "'>" & RsRec("Headline") & "</a>"
                ltlcolumnblurb.Text = "" & tmpText & ""
                ltlcolumnmore.Text = "<a href='columns.aspx?pId=" & RsRec("personality") & "'>>> more</a>"
            End If
            Count = Count + 1
        End While
        RsRec.Close()
    End Sub
    Sub VotePoll()
        SqlQuery = New SqlCommand("Select VotePoll From SuperSportZone.dbo.ZoneSiteConfiguration Where (Site = " & SiteId & ")", DatabaseConn)
        Dim VoteCategory As Integer = SqlQuery.ExecuteScalar
        Dim VotePollQuestion As String = ""

        SqlQuery = New SqlCommand("Select Top 1 a.Id, a.Question, b.Name As Site From SuperSportZone.dbo.ZoneVotePolls a INNER JOIN SuperSportZone.dbo.ZoneCategories b ON b.Id = a.Category INNER JOIN SuperSportZone.dbo.ZoneSites c ON c.Id = b.Site Where (a.Category = 692) AND (a.Active = 1) Order By a.UserDate Desc, a.Id Desc", DatabaseConn)

        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            VotePollId = RsRec("Id")
            VotePollQuestion = RsRec("Question")
        End While
        RsRec.Close()

        ltl_question.Text = VotePollQuestion

        Dim aCookie As HttpCookie
        Dim CookieValue As String
        Dim Voted As Boolean = False
        If Not Request.Cookies("vote" & VotePollId) Is Nothing Then
            aCookie = Request.Cookies("vote" & VotePollId)
            CookieValue = Server.HtmlEncode(aCookie.Value)
            If CookieValue <> "" Then
                Voted = True

                SqlQuery = New SqlCommand("Select Sum(Votes) As Votes From SuperSportZone.dbo.ZoneVotePollOptions Where (VotePollId = " & VotePollId & ")", DatabaseConn)
                Dim TotalVotes As Integer = SqlQuery.ExecuteScalar

                ltl_options.Text &= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>"
                SqlQuery = New SqlCommand("Select Text, Votes From SuperSportZone.dbo.ZoneVotePollOptions Where (VotePollId = " & VotePollId & ") Order By Number", DatabaseConn)
                RsRec = SqlQuery.ExecuteReader
                Dim Count As Integer = 1
                While RsRec.Read
                    Dim Text As String = RsRec("Text")
                    Dim Votes As Integer = RsRec("Votes")
                    ltl_options.Text &= "<tr>"
                    ltl_options.Text &= "<td align='left' style='height: 22px; padding-left: 15px;'>" & RsRec("Text") & "</td>"
                    If TotalVotes = 0 Or Votes = 0 Then
                        ltl_options.Text &= "<td align='right'>0%</td>"
                    Else
                        Dim VotePercentage As Double = (Votes / TotalVotes) * 100
                        VotePercentage = Math.Round(VotePercentage, 0)
                        ltl_options.Text &= "<td align='right'>" & VotePercentage & "%</td>"
                    End If
                    ltl_options.Text &= "</tr>"
                End While
                ltl_options.Text &= "<tr>"
                ltl_options.Text &= "<td colspan='2' align='right'><b>" & TotalVotes & " votes</b></td>"
                ltl_options.Text &= "</tr>"
                ltl_options.Text &= "</table>"
                RsRec.Close()
                pnlbuttons.Visible = False
                pnlpolls.Visible = True
            End If
        End If

        If Voted = False Then
            ltl_options.Text &= "<table cellspacing='0' cellpadding='0' border='0'>"
            SqlQuery = New SqlCommand("Select Id, Text From SuperSportZone.dbo.ZoneVotePollOptions Where (VotePollId = " & VotePollId & ") Order By Number", DatabaseConn)
            RsRec = SqlQuery.ExecuteReader
            While RsRec.Read
                ltl_options.Text &= "<tr><td style='height: 22px; padding-left: 10px; padding-right: 3px;'><input name=""options"" type=""radio"" value=""" & RsRec("Id") & """ class=""poll_radio_btn"" /></td><td>" & RsRec("Text") & "</td></tr>"
            End While
            ltl_options.Text &= "<tr><td colspan='2' height='5px'></td>"
            ltl_options.Text &= "</table>"
            RsRec.Close()
        End If

    End Sub
    Sub Fancomments()
        SqlQuery = New SqlCommand("SELECT DISTINCT TOP (9) a.Id, a.UserDate, c.SmallImage ,c.LargeImage FROM zonephotogalleries AS a INNER JOIN zonephotogallerycategories AS b ON b.PhotoGallery = a.Id INNER JOIN zonephotogalleryimages AS c ON a.Id = c.PhotoGallery WHERE (b.Category = 710) AND (a.Active = 1)  ORDER BY a.UserDate DESC, a.Id DESC", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        Dim Count As Integer = 1
        While RsRec.Read
            If Count = 1 Then
                ltlFancomments.Text &= "<table width='286px' cellspacing='1' cellpadding='2' norder='0'>"
                ltlFancomments.Text &= "<tr>"
            ElseIf Count = 4 Then
                ltlFancomments.Text &= "</tr><tr>"
                Count = 1
            End If
            ltlFancomments.Text &= "<td align='left' style='padding-Top:1px'><a id=""thumb1"" href=""http://images.supersport.co.za/" & RsRec("LargeImage") & """ class=""highslide"" onclick=""return hs.expand(this)""><img src=""http://images.supersport.co.za/" & RsRec("SmallImage") & """  title="""" height=""60"" width=""80"" class=""noborder"" style=""border:1px solid #c0c0c0;""/></a></td>"
            Count = Count + 1
        End While
        RsRec.Close()
        ltlFancomments.Text &= "</tr>"
        ltlFancomments.Text &= "<tr>"
        ltlFancomments.Text &= "<td colspan='3' style='width:260px;height:15px;margin:0px 0 0 0; padding-right:8px;padding-top:3px;padding-bottom:3px;color:#ffffff;background-color:#00743C; text-align:right;'>"
        ltlFancomments.Text &= "<a href='photogallery.aspx?Id=' style='color:#ffffff;font-weight:bold;'>>> more </a>"
        ltlFancomments.Text &= "</td>"
        ltlFancomments.Text &= "</tr>"
        ltlFancomments.Text &= "</tr></table>"
        ltlFancomments.Text &= "<div id=""controlbar"" class=""highslide-overlay controlbar"">"
        ltlFancomments.Text &= "<a href=""#"" class=""previous"" onclick=""return hs.previous(this)"" title=""Previous (left arrow key)""></a>"
        ltlFancomments.Text &= "<a href=""#"" class=""next"" onclick=""return hs.next(this)"" title=""Next (right arrow key)""></a>"
        ltlFancomments.Text &= "<a href=""#"" class=""highslide-move"" onclick=""return false"" title=""Click and drag to move""></a>"
        ltlFancomments.Text &= "<a href=""#"" class=""close"" onclick=""return hs.close(this)"" title=""Close""></a>"
        ltlFancomments.Text &= "</div>"
    End Sub
    Sub Video()
        ltlvideoheadlines.Text &= "<div id='more_tv_stories_1'>BEHIND THE SCENES"
        ltlvideoheadlines.Text &= "</div>"
        SqlQuery = New SqlCommand("Select Top 1 Id, (Case When ShortName <> '' Then ShortName Else Name End) As Name, FeatureImage As Image From SSZGeneral.dbo.broadBandVideo Where (Name Like '%Bloem%') And (Active = 1) Order By Id Desc", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        Count = 1
        While RsRec.Read
            If Count = 1 Then
                ltlvideopic.Text = "<img src = 'pics/siwelele.jpg' alt ='' /> "
                ltlvideotxt.Text = "<a href='videos.aspx?Id=" & RsRec("Id") & "'>" & RsRec("Name") & "</a>"
            End If
        End While
        RsRec.Close()
        SqlQuery = New SqlCommand("Select Top 3 Id, (Case When ShortName <> '' Then ShortName Else Name End) As Name, FeatureImage As Image From SSZGeneral.dbo.broadBandVideo Where (Name Like '%Bloem%') And (Active = 1) Order By Id Desc", DatabaseConn)
        Dim RsRec_1 As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec_1.Read
            If Count > 1 Then
                Dim tmpText As String = RsRec_1("Name")
                If tmpText.Length > 43 Then
                    tmpText = tmpText.Substring(0, 43)
                    tmpText = tmpText.Substring(0, tmpText.LastIndexOf(" ")).Trim
                    tmpText = tmpText & ""
                End If
                ltlvideoheadlines.Text &= "<div id='more_tv_stories'>"
                ltlvideoheadlines.Text &= "<a href='videos.aspx?Id=" & RsRec_1("Id") & "'>" & tmpText & "</a><br/>"
                ltlvideoheadlines.Text &= "</div>"
            End If
            Count = Count + 1
        End While
        RsRec_1.Close()
        ltlvideoheadlines.Text &= "<div id='more_tv_stories_1'>PSL MATCHES"
        ltlvideoheadlines.Text &= "</div>"
        ltlvideoheadlines.Text &= "<div id='more_tv_stories'>"
        ltlvideoheadlines.Text &= "<a href=""javascript:void(0);"" onClick=""window.open('http://bb.supersport.co.za/bloemceltic_video.asp','bloemceltic_video','width=450,height=550,top=5,left=5,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizeable=yes');"">psl matches</a>"
        ltlvideoheadlines.Text &= "</div>"
        ltlvideoheadlines.Text &= "<div id='more_tv_vids'>"
        ltlvideoheadlines.Text &= "<a href='videos.aspx?'>>>more</a>"
        ltlvideoheadlines.Text &= "</div>"
    End Sub
    Sub Banner468x60()
        SqlQuery = New SqlCommand("Select Content from ZoneSiteContent where (Site = " & SiteId & " ) AND (Type = 'Banner_468x60')", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlBanner468x60.Text = "" & RsRec("Content") & ""
        End While
        RsRec.Close()
    End Sub
    Sub Banner318x298()
        SqlQuery = New SqlCommand("Select Content from ZoneSiteContent where (Site = " & SiteId & " ) AND (Type = 'Banner_318x298')", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlBanner318x298.Text = "" & RsRec("Content") & ""
        End While
        RsRec.Close()
    End Sub
    Sub Banner728x90()
        SqlQuery = New SqlCommand("Select Content from ZoneSiteContent where (Site = " & SiteId & " ) AND (Type = 'Banner_728x90')", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlBanner728x90.Text = "" & RsRec("Content") & ""
        End While
        RsRec.Close()
    End Sub
    Sub ContentBox300x250()
        SqlQuery = New SqlCommand("Select Content from ZoneSiteContent where (Site = " & SiteId & " ) AND (Type = 'ContentBox_300x250')", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlContentbox300x250.Text = "" & RsRec("Content") & ""
        End While
        RsRec.Close()
    End Sub
    Sub Articles()
        'ltlArticles.Text = "<table border='0' cellpadding='0' cellspacing='0'>"
        ' ltlArticles.Text = "<tr>"
        ' SqlQuery = New SqlCommand("Select Top 1  a.Id As Id,a.Headline As Headline,a.ArticleDate,a.body As Body ,a.Blurb As Blurb ,d.Image3 As Image, b.*  from articlebreakdown a  Inner Join ZoneArticleCategories b ON b.ArticleId=a.Id  INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category INNER JOIN ZoneArticles d ON d.Id=a.Id where c.Site = 79 Order by a.ArticleDate desc", DatabaseConn)
        'Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        'While RsRec.Read
        'If Count = 1 Then
        'Dim tmpBody As String = RsRec("body")
        'If tmpBody.Length > 550 Then
        'tmpBody = tmpBody.Substring(0, 550)
        'tmpBody = tmpBody.Substring(0, tmpBody.LastIndexOf(" ")).Trim
        'tmpBody = tmpBody & ""
        'End If
        'If RsRec("Image") = "" Then
        'ltlArticles.Text = "<td><img src='pics/NoImage.jpg' border=""0"" /></td>"
        ' Else
        ' ltlArticles.Text = "<td><img src='http://images.supersport.co.za/" & RsRec("Image") & "' border=""0"" /></td>"
        'End If

        'ltlArticles.Text = "<td><a href=""articles.aspx?Id=" & RsRec("Id") & """>" & RsRec("Headline") & "</a></td>"
        'ltlarticleblurb.Text = "" & tmpBody & "&nbsp;... <a href=""articles.aspx?Id=" & RsRec("Id") & """ style='color:#ffffff;font-weight:bold;'>read more</a>"
        ' Count = Count + 1
        ' End If
        ' End While
        ' ltlArticles.Text = "</tr>"
        ' ltlArticles.Text = "</table>"
        ' RsRec.Close()
        Dim ds As DataSet

        Dim da As SqlDataAdapter = New SqlDataAdapter
        ds = New DataSet
        SqlQuery = New SqlCommand("Select Top 4 a.Active,a.Id As Id,a.Headline As Headline,a.ArticleDate,a.body As Body ,a.SmallImage,a.Blurb As Blurb ,d.Image3 As Image, b.*  from articlebreakdown a  Inner Join ZoneArticleCategories b ON b.ArticleId=a.Id  INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category INNER JOIN ZoneArticles d ON d.Id=a.Id where (c.Site = 79 AND a.Active=1) Order by a.ArticleDate desc", DatabaseConn)
        da = New SqlDataAdapter
        da.SelectCommand = SqlQuery
        da.Fill(ds, "news")

        Dim Count As Integer = 1
        Dim Images As ArrayList = New ArrayList
        Dim Row As DataRow
        For Each Row In ds.Tables("news").Rows()
            Dim tdClass As String = "headlines"
            Dim Headline As String = Row.Item("Headline")

            Dim FullHeadline As String = Row.Item("Headline")
            FullHeadline = FullHeadline.Replace("'", "`")
            FullHeadline = FullHeadline.Replace("""", "`")

            Dim Blurb As String = Row.Item("Blurb")
            If Blurb.Length > 150 Then
                Blurb = Blurb.Substring(0, 150)
                Blurb = Blurb.Substring(0, Blurb.LastIndexOf(" ")).Trim
                Blurb = Blurb & ""
            End If

            '  Dim Blurb As String = Row.Item("Blurb")


            If Count = 1 Then


                If Row.Item("Image") <> "" Then
                    ltlarticleImage.Text = "<img src ='http://images.supersport.co.za/" & Row.Item("Image") & "' width='310px' height='290px'><br>"
                Else
                    ltlarticleImage.Text = ""
                End If
                ltlarticleImage.Text &= "<span style='font-size:12px;width:250px;font-weight:bold;color:#fff;'>" & FullHeadline & "..&nbsp; <a href='articles.aspx?Id=" & Row.Item("Id") & "' style='color:#fff'>read more</a></span><br>"
                ' ltlarticleImage.Text &= "<span style='width:250px'>" & Blurb & "</span><br>"

            Else

                If Count = 1 Then
                    ltlHeadlines.Text &= "<tr>"
                    ltlHeadlines.Text &= "<td></td>"
                    ltlHeadlines.Text &= "</tr>"
                    ltlHeadlines.Text &= "<tr>"
                    ltlHeadlines.Text &= "<td></td>"
                    ltlHeadlines.Text &= "</tr>"
                ElseIf Count = 2 Then
                    ltlHeadlines.Text &= "<tr>"
                    ltlHeadlines.Text &= "<td style='padding-bottom:1px;bgcolor:#ffffff;width:280px;font-weight:bold;color: #FF0000; text-decoration: none;font-size: 12px;' id=""arts" & Count & """ onMouseOver=""changestory(" & Count & ");"" onClick=""location.href='articles.aspx?id=" & Row.Item("Id") & "';"" class='headlines'><a href=""articles.aspx?id=" & Row.Item("Id") & """ style='color:#fff; font-weight:bold; text-decoration:none; padding:0 0 0 0px;' >" & Headline & "</a><br></td>"
                    ltlHeadlines.Text &= "</tr>"
                    ltlHeadlines.Text &= "<tr>"
                    ltlHeadlines.Text &= "<td style='padding-bottom:4px;bgcolor:#ffffff;width:280px;font-weight:bold;color: #FF0000; text-decoration: none;font-size: 12px;' id=""arts" & Count & """ onMouseOver=""changestory(" & Count & ");"" onClick=""location.href='articles.aspx?id=" & Row.Item("Id") & "';"" class='headlines'><a href=""articles.aspx?id=" & Row.Item("Id") & """ style='color:#fff; font-weight:normal; text-decoration:none; padding:0 0 0 0px;' ><img src='http://images.supersport.co.za/" & Row.Item("SmallImage") & "' border='0px' width='60px' height='60px' align='left'><span style='padding-left:2px;'>" & Blurb & "..</span></span></a><a href='articles.aspx?Id=" & Row.Item("Id") & "' style='color:#fff;font-weight:bold;'>&nbsp;read more</a><span style='background-position:0% 20%; margin:0px 0 0 0; padding:0 0 5px 5px; border-top:1px solid #fff; display:block;'></span></td>"
                    ltlHeadlines.Text &= "</tr>"
                ElseIf Count = 4 Then
                    ltlHeadlines.Text &= "<tr style=""bgcolor:#00743C;"">"
                    ltlHeadlines.Text &= "<td style='padding-bottom:1px;width:280px;bgcolor:#00743C;' id=""arts" & Count & """ onMouseOver=""changestory(" & Count & ");"" onClick=""location.href='articles.aspx?id=" & Row.Item("Id") & "';""  ><a href=""articles.aspx?id=" & Row.Item("Id") & """ style='color:#fff; font-weight:bold; text-decoration:none; padding:0 0 0 0px;'>" & Headline & "</a><br></td>"
                    ltlHeadlines.Text &= "</tr>"
                    ltlHeadlines.Text &= "<tr style=""bgcolor:#00743C;"">"
                    ltlHeadlines.Text &= "<td style='padding-bottom:4px;width:280px;bgcolor:#00743C;' id=""arts" & Count & """ onMouseOver=""changestory(" & Count & ");"" onClick=""location.href='articles.aspx?id=" & Row.Item("Id") & "';""  class='headlines_a'><a href=""articles.aspx?id=" & Row.Item("Id") & """ style='color:#fff; font-weight:normal; text-decoration:none; padding:0 0 0 0px;'><img src='http://images.supersport.co.za/" & Row.Item("SmallImage") & "' border='0px' width='60px' height='60px' align='left' ><span style='padding-left:2px;'>" & Blurb & "..</span></a><a href='articles.aspx?Id=" & Row.Item("Id") & "' style='color:#fff;font-weight:bold;'> &nbsp;read more</a><span style='background-position:0% 20%; margin:0px 0 0 0; padding:0 0 5px 5px; border-top:0px solid #fff; display:block;'></span></td>"
                    ltlHeadlines.Text &= "</tr>"
                Else
                    ltlHeadlines.Text &= "<tr>"
                    ltlHeadlines.Text &= "<td style='padding-bottom:1px;width:280px;bgcolor:#ffffff;' id=""arts" & Count & """ onMouseOver=""changestory(" & Count & ");"" onClick=""location.href='articles.aspx?id=" & Row.Item("Id") & "';""  class='headlines_a'><a href=""articles.aspx?id=" & Row.Item("Id") & """ style='color:#fff; font-weight:bold; text-decoration:none; padding:0 0 0 0px;'>" & Headline & "</a><br></td>"
                    ltlHeadlines.Text &= "</tr>"
                    ltlHeadlines.Text &= "<tr>"
                    ltlHeadlines.Text &= "<td style='padding-bottom:4px;width:280px;bgcolor:#ffffff;' id=""arts" & Count & """ onMouseOver=""changestory(" & Count & ");"" onClick=""location.href='articles.aspx?id=" & Row.Item("Id") & "';""  class='headlines_a'><a href=""articles.aspx?id=" & Row.Item("Id") & """ style='color:#fff; font-weight:normal; text-decoration:none; padding:0 0 0 0px;'><img src='http://images.supersport.co.za/" & Row.Item("SmallImage") & "' border='0px' width='60px' height='60px' align='left' ><span style='padding-left:2px;'>" & Blurb & "..</span></a><a href='articles.aspx?Id=" & Row.Item("Id") & "' style='color:#fff;font-weight:bold;'>&nbsp;read more</a><span style='background-position:0% 20%; margin:0px 0 0 0; padding:0 0 5px 5px; border-top:1px solid #fff; display:block;'></span></td>"
                    ltlHeadlines.Text &= "</tr>"
                End If
            End If

            ltl_stories.Text &= "<div id=""heads" & Count & """ class=""hidden""><a href=""articles.aspx?id=" & Row.Item("Id") & """ style='color:#000000;font-weight:bold;'>" & Row.Item("Headline") & "</a></div><br><br>"
            Count = Count + 1
        Next

        
    End Sub
    Sub Gallery()
        SqlQuery = New SqlCommand("SELECT DISTINCT TOP (15) a.Id, a.UserDate, c.SmallImage ,c.LargeImage FROM zonephotogalleries AS a INNER JOIN zonephotogallerycategories AS b ON b.PhotoGallery = a.Id INNER JOIN zonephotogalleryimages AS c ON a.Id = c.PhotoGallery WHERE (b.Category = 693) AND (a.Active = 1)  ORDER BY a.UserDate DESC, a.Id DESC", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        Dim Count As Integer = 1
        While RsRec.Read
            If Count = 1 Then
                ltlphotogallery.Text &= "<table width='100%' cellspacing='1' cellpadding='2' norder='0'>"
                ltlphotogallery.Text &= "<tr>"
            ElseIf Count = 6 Then
                ltlphotogallery.Text &= "</tr><tr>"
                Count = 1
            End If
            ltlphotogallery.Text &= "<td align='center' style='padding-Top:8px'><a id=""thumb1"" href=""http://images.supersport.co.za/" & RsRec("LargeImage") & """ class=""highslide"" onclick=""return hs.expand(this)""><img src=""http://images.supersport.co.za/" & RsRec("SmallImage") & """  title="""" height=""80"" width=""80"" class=""noborder"" /></a></td>"
            Count = Count + 1
        End While
        RsRec.Close()
        ltlphotogallery.Text &= "</tr></table>"
        ltlphotogallery.Text &= "<div id=""controlbar"" class=""highslide-overlay controlbar"">"
        ltlphotogallery.Text &= "<a href=""#"" class=""previous"" onclick=""return hs.previous(this)"" title=""Previous (left arrow key)""></a>"
        ltlphotogallery.Text &= "<a href=""#"" class=""next"" onclick=""return hs.next(this)"" title=""Next (right arrow key)""></a>"
        ltlphotogallery.Text &= "<a href=""#"" class=""highslide-move"" onclick=""return false"" title=""Click and drag to move""></a>"
        ltlphotogallery.Text &= "<a href=""#"" class=""close"" onclick=""return hs.close(this)"" title=""Close""></a>"
        ltlphotogallery.Text &= "</div>"
        ltlphotogallerymore.Text &= "<div id=""more"">"
        ltlphotogallerymore.Text &= "<a href=""photogallery.aspx"">>> more </a>&nbsp;"
        ltlphotogallerymore.Text &= "</div>"
    End Sub
    Sub HPArchives()
        Dim Id As String = ""
        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If
        Count = 1
        SqlQuery = New SqlCommand("Select Top 8 a.Active,a.Id As Id,a.Headline As Headline,a.ArticleDate,a.body As Body,a.Blurb As Blurb ,d.Image3 As Image, b.*  from articlebreakdown a  Inner Join ZoneArticleCategories b ON b.ArticleId=a.Id  INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category INNER JOIN ZoneArticles d ON d.Id=a.Id where (c.Site = 4 AND a.Active = 1) Order by a.ArticleDate desc", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read

            If Count = 1 Then
                ltlarticlearchive.Text &= "<span style='background-position:0% 20%; margin:4px 0 0 0; padding:0 0 5px 10px; border-top:0px dotted #0E6227; display:block;'><a href='articles.aspx?Id=" & RsRec("Id") & "' style='color:#0E6227; text-decoration:none; font-weight:bold; padding:0;'>" & RsRec("Headline") & "</span>"
            Else
                ltlarticlearchive.Text &= "<span style='background-position:0% 20%; margin:4px 0 0 0; padding:0 0 5px 10px; border-top:1px dotted #0E6227; display:block;'><a href='articles.aspx?Id=" & RsRec("Id") & "' style='color:#0E6227; text-decoration:none; font-weight:bold; padding:0;'>" & RsRec("Headline") & "</span>"
            End If

            Count = Count + 1
        End While

        RsRec.Close()
    End Sub
    Sub PSL()
        SqlQuery = New SqlCommand("Select Top 6 HomeTeam, AwayTeam, HomeTeamScore, AwayTeamScore, MatchDateTime From Soccer.dbo.bloemceltic_result Order By MatchDateTime Desc", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        Count = 1
        While RsRec.Read
            If Count > 0 Then
                ltlpsl.Text = "<tr>"
                ltlpsl.Text = "<td class='fix' align='center' height='23'>" & RsRec("HomeTeamScore") & " - " & RsRec("AwayTeamScore") & "</td>"
                ltlpsl.Text = "<td class='fix' style='padding-left:3px;' height='23'>" & RsRec("HomeTeam") & "&nbsp;&nbsp;vs" & RsRec("AwayTeam") & "</td>"
                ltlpsl.Text = "</tr>"
            Else
                Exit While
            End If
            Count = Count + 1
            'ltlpslmore.Text = "<a href='results.aspx'>>> more </a>"
        End While
        RsRec.Close()
    End Sub
    Sub Logs()
        SqlQuery = New SqlCommand("Execute Soccer.dbo.pa_returnLogs 794,0", DatabaseConn)
        Dim TeamName As String = "Bloem Celtic"
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        Dim GroupName As String = ""
        If DatabaseId = 1 Then
            While RsRec.Read
                If RsRec("TeamId") = TeamId Then
                    GroupName = RsRec("GroupName")
                    Exit While
                End If
            End While
        End If
        RsRec.Close()
        RsRec = SqlQuery.ExecuteReader
        If RsRec.HasRows Then
            Dim Count As Integer = 1
            Dim tmpLast As String = ""
            Dim Present As Boolean = False

            While RsRec.Read
                Dim Proceed As Boolean = True
                If GroupName <> "" Then
                    If RsRec("GroupName") <> GroupName Then
                        Proceed = False
                    End If
                End If
                If Proceed = True Then
                    Dim Display As Boolean = True
                    If Count > 6 And Present = True Then
                        Exit While
                    End If
                    If Count > 5 And Present = False Then
                        If RsRec("Team").ToLower = TeamName.ToLower Then
                            Display = True
                        Else
                            Display = False
                        End If
                    End If
                    If Display = True Then
                        If RsRec("Team") = "Bloem Celtic" Then
                            ltlLogs.Text &= "<tr style='font-weight:bold;'>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & Count & "</td>"
                            ltlLogs.Text &= "<td class='row1' style='padding-left:3px;' height='23'>" & RsRec("Team") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("Played") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("Won") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("Drew") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("Lost") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("GoalsFor") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("GoalsAgainst") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("GoalsFor") - RsRec("GoalsAgainst") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("LogPoints") & "</td>"
                            ltlLogs.Text &= "</tr>"
                        Else
                            ltlLogs.Text &= "<tr>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & Count & "</td>"
                            ltlLogs.Text &= "<td class='row1' style='padding-left:3px;' height='23'>" & RsRec("Team") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("Played") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("Won") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("Drew") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("Lost") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("GoalsFor") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("GoalsAgainst") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("GoalsFor") - RsRec("GoalsAgainst") & "</td>"
                            ltlLogs.Text &= "<td class='row1' align='center' height='23'>" & RsRec("LogPoints") & "</td>"
                            ltlLogs.Text &= "</tr>"
                        End If
                    End If
                    If RsRec("Team").ToLower = TeamName.ToLower Then
                        Present = True
                    End If
                    Count = Count + 1
                    '  ltlpslmore.Text = "<a href='logs.aspx'>>> more </a>"
                End If
            End While
        End If
        RsRec.Close()

    End Sub
    Sub Fixtures()
        SqlQuery = New SqlCommand("SELECT Distinct  '' As Id,(Case When  c.LeagueId = 155 OR  e.LeagueId = 155 Then 5 When  c.LeagueId = 189 OR  e.LeagueId = 189 Then 2  Else 1 End) As Ordering, a.MatchDateTime, b.Name As HomeTeam,c.Name As AwayTeam,d.Name As League, e.Name As Venue,Year(a.MatchDateTime),Month(a.MatchDateTime),Day(a.MatchDateTime) As MatchDay, a.Postponed ,a.Completed FROM Soccer.dbo.Matches a INNER JOIN Soccer.dbo.TeamsByLeague b ON b.Id = a.TeamAId INNER JOIN Soccer.dbo.TeamsByLeague c ON c.Id = a.TeamBId INNER JOIN Soccer.dbo.Leagues d ON d.Id = a.LeagueId INNER JOIN Soccer.dbo.VenuesByLeague e ON e.Id = a.VenueId WHERE (b.ParentId = 121 OR c.ParentId = 121 OR c.LeagueId = 155 OR e.LeagueId = 155 OR c.LeagueId = 189 OR e.LeagueId = 189 OR e.LeagueId = 316) AND (c.LeagueId <> 123 OR e.LeagueId <> 123) AND (a.Completed = 0) AND (b.Name LIKE '%Bloem%' OR c.Name LIKE '%Bloem%') UNION Select Distinct a.Id,'' As Odering ,a.MatchDateTime, a.HomeTeamName As HomeTeam,a.AwayTeamname As AwayTeam,b.Competition_Name As League,a.Venue,Year(a.MatchDateTime), Month(a.MatchDateTime),Day(a.MatchDateTime) As MatchDay,(Case When StatusId = 14 Then 1 Else 0 End) As Postponed,a.Result From Soccer.dbo.pa_Matches a INNER JOIN Soccer.dbo.pa_Competitions b ON b.Competition_Id = a.Competition Where (a.Competition = 794) And (a.Result = 0) AND (a.HomeTeamId =49944 OR a.AwayTeamId =49944) AND (b.Competition_Season = a.Season) AND (a.StatusId < 14) ORDER BY a.MatchDateTime", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        Count = 1
        ltlFixtures.Text &= "<tr>"
        ltlFixtures.Text &= "<td class='header_rows' align='center' height='23' width='20%'>Date</td>"
        ltlFixtures.Text &= "<td class='header_rows' align='center' height='23' width='40%'>Fixture</td>"
        ltlFixtures.Text &= "<td class='header_rows' align='center' height='23' width='20%'>Venue</td>"
        ltlFixtures.Text &= "</tr>"
        While RsRec.Read
            Dim HomeTeam As String = RsRec("HomeTeam")
            Dim AwayTeam As String = RsRec("AwayTeam")
            Dim MatchDateTime As DateTime = RsRec("MatchDateTime")
            If Count < 7 Then
                    ltlFixtures.Text &= "<tr>"
                    ltlFixtures.Text &= "<td class='row1' align='center' height='23'>" & MatchDateTime.ToString("dd'&nbsp;'MMMM") & "</td>"
                    ltlFixtures.Text &= "<td class='row1' style='padding-left:3px;' height='23'>" & HomeTeam & " v " & AwayTeam & "</td>"
                    ltlFixtures.Text &= "<td class='row1' style='padding-left:3px;' height='23'>" & RsRec("Venue") & "</td>"
                    ltlFixtures.Text &= "</tr>"
            Else
                Exit While

            End If
            Count = Count + 1
            'ltlpslmore.Text = "<a href='fixtures.aspx'>>> more </a>"
        End While
        RsRec.Close()

    End Sub
    Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class

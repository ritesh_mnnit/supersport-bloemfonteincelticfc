<%@ Page Language="VB" AutoEventWireup="false" CodeFile="content.aspx.vb" Inherits="content" MasterPageFile="master_1.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<div class="body_art_content"> 
<div class="art_title"><asp:Label ID="ltlcontentheadline" runat="server" /></div>

<div id="art_story">
  <asp:Label Id="ltlcontentbody" runat="server" />
</div>
<div style="clear:both;"></div>
</div> <!-- end body -->
<!-- end body art content -->

</asp:Content>

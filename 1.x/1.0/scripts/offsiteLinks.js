var thispagedomain = ExtractDomainName(document.URL);

for(var i = 0; i <= document.links.length - 1; i++) {
  var url = document.links[i].href.toLowerCase();
  if(url.indexOf('http://') != 0) { continue; }
  var hrefdomain = ExtractDomainName(url);
  if(thispagedomain != hrefdomain) {
    document.links[i].target = '_blank';
  }
}

function ExtractDomainName(s) {
  var i = s.indexOf('//');
  if(i > -1) { s = s.substr(i+2); }
  i = s.indexOf('/');
  if(i > -1) { s = s.substr(0,i); }
  i = s.indexOf(':');
  if(i > -1) { s = s.substr(0,i); }
  var re = /[a-z]/i;
  if(! re.test(s)) { return s; }
  var a = s.split('.');
  if(a.length < 2) { return s; }
  var domain = a[a.length-2] + '.' + a[a.length-1];
  if(a.length > 2) {
    if(a[a.length-2].length==2 && a[a.length-1].length==2) {
      domain = a[a.length-3] + '.' + domain;
      }
    }
  return domain.toLowerCase();
}
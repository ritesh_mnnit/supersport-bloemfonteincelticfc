﻿function HTTPObject() {
	var JXHR;
	if (window.ActiveXObject) {
		if (_XML_ActiveX) {
			JXHR = new ActiveXObject(_XML_ActiveX);
		} else {
			var versions = ["MSXML2.XMLHTTP", "Microsoft.XMLHTTP", "Msxml2.XMLHTTP.7.0", "Msxml2.XMLHTTP.6.0", "Msxml2.XMLHTTP.5.0", "Msxml2.XMLHTTP.4.0", "MSXML2.XMLHTTP.3.0"];
			for (var i = 0; i < versions.length ; i++) {
				try {
				JXHR = new ActiveXObject(versions[i]);
					if (JXHR) {
						var _XML_ActiveX = versions[i];
						break;
					}
				}
				catch (e) {
				}
			}
		}
	}
	if (!JXHR && typeof XMLHttpRequest != 'undefined') {
		try {
			JXHR = new XMLHttpRequest();
		} catch (e) {
			JXHR = false;
		}
	}
	return JXHR;
}

var httpvote = HTTPObject();
var voted = false;
var busy = false;

function votes() {
    if (!busy) {
        busy = true;
	    var selection = getCheckedValue(document.forms['vote'].elements['options']);
	    if (!voted && selection != "") {
		    voted = true;
		    var urlz = "votepoll.aspx?id="+encodeURIComponent(selection)
		    httpvote.open("GET", urlz, true);
		    httpvote.onreadystatechange = voteresponse;
		    busy = true;
		    httpvote.send(null);
	    } else {
	        busy = false;
	    }
	}
}

function voteresults(votepoll) {
    if (!busy) {
        busy = true;
	    var urlz = "votepoll.aspx?poll="+encodeURIComponent(votepoll)
	    httpvote.open("GET", urlz, true);
	    httpvote.onreadystatechange = voteresponse;
	    busy = true;
	    httpvote.send(null);
	}
}

function voteresponse() {
	if (httpvote.readyState == 4) {
		if(httpvote.status == 200){
			var tmptext = httpvote.responseText;
			document.getElementById("polloptions").innerHTML = tmptext;
			document.getElementById("buttons").innerHTML = "<div style='text-align: center;'><a href='votepolls.aspx' style='color: #FFFFFF; font-weight: bold; font-size: 14px; font-style: italic;'>previous polls</a></div>";
		} else {

		}
		busy = false;
	}	
}

function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

var stopchanging = false;
var loaded = false;

function preload() {
    if (!loaded) {
        for (var i = 0; i < pics.length; i++) {
            if (pics[i] != '') {
                pic[i] = new Image();
                pic[i].src = pics[i];
            }
        }
        loaded = true;
    }
}

function changestory(id) {
    document.getElementById("heads").innerHTML = document.getElementById("heads"+id).innerHTML;
    document.getElementById("blurb").innerHTML = document.getElementById("blurb"+id).innerHTML;
    document.getElementById("imager").innerHTML = document.getElementById("imager"+id).innerHTML;
    for (var i = 1; i<= 4; i++) {
        if (document.getElementById("arts"+i)) {
            if (i == 4) {
                document.getElementById("arts"+i).className = "headlines_last";
            } else {
                document.getElementById("arts"+i).className = "headlines_a";
            }
            document.getElementById("arts"+id).className = "headlines_featured";
        }
    }
}


var partnertimeout;

function partners(partner) {
    clearTimeout(partnertimeout);
    document.getElementById("partners").style.display = "none";
    document.getElementById("bodies").style.display = "none";
    
    var obj1;
    var obj2;
    var xDif;
    
    if (partner == 1) {
        obj1 = document.getElementById("partnerslink");
        obj2 = document.getElementById("partners");
        xDif = 135;
    } else {
        obj1 = document.getElementById("bodieslink");
        obj2 = document.getElementById("bodies");
        xDif = 50;
    }
    var coors = findPos(obj1);
    obj2.style.top = (coors[1] + 25) + 'px';
	obj2.style.left = (coors[0] - xDif) + 'px';
	obj2.style.display = "block";
}

function hidepartners() {
    document.getElementById("partners").style.display = "none";
    document.getElementById("bodies").style.display = "none";
}

function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft;
		curtop = obj.offsetTop;
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		}
	}
	return [curleft,curtop];
}





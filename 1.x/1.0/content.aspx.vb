Partial Class content
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Content(ID)
    End Sub
    Sub Content(ByVal Id As String)
        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If
        SqlQuery = New SqlCommand("SELECT Id As Content_Id, Headline As Content_Headline, Content As Content_Body FROM ZoneContent WHERE (Id = " & Id & ")", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlcontentheadline.Text = "" & RsRec("Content_Headline") & ""
            ltlcontentbody.Text = "" & RsRec("Content_Body") & ""
        End While
        RsRec.Close()
    End Sub
    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class

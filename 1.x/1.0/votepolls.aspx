<%@ Page Language="VB" AutoEventWireup="false" CodeFile="votepolls.aspx.vb" Inherits="votepolls" MasterPageFile="master_1.master" %>
<%@ MasterType VirtualPath="master_1.master" %>

<asp:Content runat="server" ContentPlaceHolderId="ContentPlaceHolder2" Id="content2">

<script type="text/javascript">
    <asp:Literal id="ltl_script" runat="server" EnableViewState="False" />
    function results(divid) {
        for (var i = 1; i <= count; i++) {
            if (document.getElementById("poll"+i).style.display == "block") {
                document.getElementById("poll"+i).style.display = "none";
            }
        }
        document.getElementById("poll"+divid).style.display = "block";
    }
</script>




<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>
     <div class="body_art_content">
       <div class="art_title">Recent Polls</div>

        <div id="art_story">
    

              </div>

        </div> 
        
        </td>
    </tr>
    <tr>
        <td><asp:Label CssClass="channel_cat" id="ltl_sport" runat="server" /><asp:Label class="channel_title" ID="ltl_category" runat="server" /></td>
    </tr>
    <tr>
        <td height='11'></td>
    </tr>
</table>

<table width="100%" cellspacing="2" cellpadding="0" border="0">
    <asp:Literal id="ltl_polls" runat="server" EnableViewState="False" />
</table>


</asp:Content>
<% @ Language=VBScript %>
<% Option Explicit %>
<!--#include file="common.asp" -->
<!--#include file="functions/functions_date_time_format.asp" -->
<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Forums(TM)
'**  http://www.webwizforums.com
'**                            
'**  Copyright (C)2001-2008 Web Wiz(TM). All Rights Reserved.
'**  
'**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS UNDER LICENSE FROM 'WEB WIZ'.
'**  
'**  IF YOU DO NOT AGREE TO THE LICENSE AGREEMENT THEN 'WEB WIZ' IS UNWILLING TO LICENSE 
'**  THE SOFTWARE TO YOU, AND YOU SHOULD DESTROY ALL COPIES YOU HOLD OF 'WEB WIZ' SOFTWARE
'**  AND DERIVATIVE WORKS IMMEDIATELY.
'**  
'**  If you have not received a copy of the license with this work then a copy of the latest
'**  license contract can be found at:-
'**
'**  http://www.webwizguide.com/license
'**
'**  For more information about this software and for licensing information please contact
'**  'Web Wiz' at the address and website below:-
'**
'**  Web Wiz, Unit 10E, Dawkins Road Industrial Estate, Poole, Dorset, BH15 4JD, England
'**  http://www.webwizguide.com
'**
'**  Removal or modification of this copyright notice will violate the license contract.
'**
'****************************************************************************************



'*************************** SOFTWARE AND CODE MODIFICATIONS **************************** 
'**
'** MODIFICATION OF THE FREE EDITIONS OF THIS SOFTWARE IS A VIOLATION OF THE LICENSE  
'** AGREEMENT AND IS STRICTLY PROHIBITED
'**
'** If you wish to modify any part of this software a license must be purchased
'**
'****************************************************************************************



'Set the response buffer to true as we maybe redirecting
Response.Buffer = True

'Make sure this page is not cached
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 2
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "No-Store"


'Dimension variables
Dim strForumName		'Holds the forum name
Dim intCatID			'Holds the cat ID
Dim strCatName			'Holds the cat name
Dim lngNumberOfReplies		'Holds the number of replies for a topic
Dim lngTopicID			'Holds the topic ID
Dim strSubject			'Holds the topic subject
Dim strTopicIcon		'Holds the topic icon
Dim strTopicStartUsername 	'Holds the username of the user who started the topic
Dim lngTopicStartUserID		'Holds the users Id number for the user who started the topic
Dim lngNumberOfViews		'Holds the number of views a topic has had
Dim lngLastEntryMessageID	'Holds the message ID of the last entry
Dim strLastEntryUsername	'Holds the username of the last person to post a message in a topic
Dim lngLastEntryUserID		'Holds the user's ID number of the last person to post a meassge in a topic
Dim dtmLastEntryDate		'Holds the date the last person made a post in the topic
Dim intRecordPositionPageNum	'Holds the recorset page number to show the topics for
Dim intRecordLoopCounter	'Holds the loop counter numeber
Dim intTopicPageLoopCounter	'Holds the number of pages there are in the forum
Dim intShowTopicsFrom		'Holds when to show the topics from
Dim strShowTopicsFrom		'Holds the display text from when the topics are shown from
Dim blnForumLocked		'Set to true if the forum is locked
Dim blnTopicLocked		'set to true if the topic is locked
Dim intPriority			'Holds the priority level of the topic
Dim intNumberOfTopicPages	'Holds the number of topic pages
Dim intTopicPagesLoopCounter	'Holds the number of loops
Dim lngPollID			'Holds the Poll ID
Dim intShowTopicsWithin		'Holds the amount of time to show topics within
Dim intMovedForumID		'If the post is moved this holds the moved ID
Dim intNonPriorityTopicNum	'Holds the record count for non priority topics
Dim strFirstPostMsg		'Holds the first message in the topic
Dim dtmFirstEntryDate		'Holds the date of the first message
Dim lngLastEntryTopicID		'Holds the topic ID of the last entry
Dim strLastEntryUser		'Holds the the username of the user who made the last entry
Dim strForumDiscription		'Holds the forum description
Dim strForumPassword		'Holds the forum password if there is one
Dim lngNumberOfTopics		'Holds the number of topics in a forum
Dim lngNumberOfPosts		'Holds the number of Posts in the forum
Dim blnHideForum		'Set to true if this is a hidden forum
Dim intForumColourNumber	'Holds the number to calculate the table row colour
Dim lngLastEntryMeassgeID	'Holds the message ID of the last entry
Dim intMasterForumID		'Holds the main forum ID
Dim strMasterForumName		'Holds the main forum name
Dim blnHideTopic		'Set to true if the topic is hidden
Dim strTableRowColour		'Holds the row colour for the table
Dim sarryTopics			'Holds the topics to display
Dim sarrySubForums		'Holds the sub forums to display
Dim intSubForumID		'Holds the sub forum ID
Dim intCurrentRecord		'Holds the current record position
Dim intTotalRecords		'Holds the number of records in the topics array
Dim intTotalRecordsPages	'Holds the total number of pages
Dim intStartPosition		'Holds the start poition for records to be shown
Dim intEndPosition		'Holds the end poition for records to be shown
Dim intSubCurrentRecord		'Holds the current records for the sub forums
Dim strSortDirection		'Holds the sort order
Dim strSortBy			'Holds the way the records are sorted
Dim strShowTopicsDate		'Holds the show topics date
Dim dtmEventDate		'Holds the date if this is a calendar event
Dim dtmEventDateEnd		'Holds the date if this is a calendar event
Dim intPageLinkLoopCounter
Dim intUnReadPostCount		'Holds the count for the number of unread posts in the forum
Dim intUnReadForumPostsLoop	'Loop to count the number of unread posts in a forum
Dim intMaxResults		'Max results retrurned
Dim strSubForums		'Holds the subforums names

'Initlaise variables
intNonPriorityTopicNum = 0
blnHideTopic = false
intSubCurrentRecord = 0
intCurrentRecord = 0
strShowTopicsFrom = " '" & strTxtAnyDate & "'"



'Max results returned, to high and your forum will run slow
If strDatabaseType = "Access" Then
	intMaxResults = 500
Else
	intMaxResults = 5000
End If



'Read in the Forum ID to display the Topics for
If isNumeric(Request.QueryString("FID")) Then intForumID = CInt(Request.QueryString("FID")) Else intForumID = 0



'If there is no Forum ID to display the Topics for then redirect the user to the main forum page
If intForumID = 0 Then

	'Clean up
	Call closeDatabase()

	'Redirect
	Response.Redirect("default.asp" & strQsSID1)
End If



'If this is the first time the page is displayed then the Forum Topic record position is set to page 1
If isNumeric(Request.QueryString("PN")) = false Then
	intRecordPositionPageNum = 1
	
ElseIf Request.QueryString("PN") < 1 Then
	intRecordPositionPageNum = 1
	
'Else the page has been displayed before so the Forum Topic record postion is set to the Record Position number
Else
	intRecordPositionPageNum = CInt(Request.QueryString("PN"))
End If






'If we have not yet checked for unread posts since last visit run it now
If Session("dtmUnReadPostCheck") = "" Then 
	Call UnreadPosts()
	
'Read in array if at application level
ElseIf isArray(Application("sarryUnReadPosts" & strSessionID)) Then  
	sarryUnReadPosts = Application("sarryUnReadPosts" & strSessionID)
	
'Read in the unread posts array	
ElseIf isArray(Session("sarryUnReadPosts")) Then 
	sarryUnReadPosts = Session("sarryUnReadPosts")
	
End If




'Read in the forum details inc. cat name, forum details, and permissions (also reads in the main forum name if in a sub forum, saves on db call later)

'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "" & _
"SELECT" & strDBTop1 & " " & strDbTable & "Category.Cat_ID, " & strDbTable & "Category.Cat_name, " & strDbTable & "Forum.Forum_ID, " & strDbTable & "Forum.Sub_ID, " & strDbTable & "Forum.Forum_name, " & strDbTable & "Forum2.Forum_name AS Main_forum, " & strDbTable & "Forum.Password, " & strDbTable & "Forum.Forum_code, " & strDbTable & "Forum.Locked, " & strDbTable & "Forum.Show_topics, " & strDbTable & "Permissions.* " & _
"FROM " & strDbTable & "Category" & strDBNoLock & ", " & strDbTable & "Forum" & strDBNoLock & ", " & strDbTable & "Forum AS " & strDbTable & "Forum2" & strDBNoLock & ", " & strDbTable & "Permissions" & strDBNoLock & " " & _
"WHERE " & strDbTable & "Category.Cat_ID = " & strDbTable & "Forum.Cat_ID " & _
	"AND " & strDbTable & "Forum.Forum_ID = " & strDbTable & "Permissions.Forum_ID " & _
	"AND " & strDbTable & "Forum.Forum_ID = " & intForumID & " " & _
	"AND (" & strDbTable & "Forum.Sub_ID = " & strDbTable & "Forum2.Forum_ID OR (" & strDbTable & "Forum.Sub_ID = 0 AND " & strDbTable & "Forum.Forum_ID = " & strDbTable & "Forum2.Forum_ID)) " & _
	"AND (" & strDbTable & "Permissions.Author_ID = " & lngLoggedInUserID & " OR " & strDbTable & "Permissions.Group_ID = " & intGroupID & ") " & _
"ORDER BY " & strDbTable & "Forum.Sub_ID, " & strDbTable & "Forum.Forum_Order, " & strDbTable & "Permissions.Author_ID DESC" & strDBLimit1 & ";"


'Set error trapping
On Error Resume Next

'Query the database
rsCommon.Open strSQL, adoCon

'If an error has occurred write an error to the page
If Err.Number <> 0 Then	Call errorMsg("An error has occurred while executing SQL query on database.", "get_forum_data", "forum_topics.asp")

'Disable error trapping
On Error goto 0

'If there is a record returned by the recordset then check to see if you need a password to enter it
If NOT rsCommon.EOF Then

	'Read in forum details from the database
	intCatID = CInt(rsCommon("Cat_ID"))
	strCatName = rsCommon("Cat_name")
	strForumName = rsCommon("Forum_name")
	strMasterForumName = rsCommon("Main_forum")
	intMasterForumID = CLng(rsCommon("Sub_ID"))
	blnForumLocked = CBool(rsCommon("Locked"))
	intShowTopicsWithin = CInt(rsCommon("Show_topics"))

	'Read in the forum permissions
	blnRead = CBool(rsCommon("View_Forum"))
	blnPost = CBool(rsCommon("Post"))
	blnReply = CBool(rsCommon("Reply_posts"))
	blnEdit = CBool(rsCommon("Edit_posts"))
	blnDelete = CBool(rsCommon("Delete_posts"))
	blnPriority = CBool(rsCommon("Priority_posts"))
	blnPollCreate = CBool(rsCommon("Poll_create"))
	blnVote = CBool(rsCommon("Vote"))
	blnModerator = CBool(rsCommon("Moderate"))
	blnCheckFirst = CBool(rsCommon("Display_post"))

	'If the user has no read writes then kick them
	If blnRead = False Then

		'Reset Server Objects
		rsCommon.Close
		Call closeDatabase()


		'Redirect to a page asking for the user to enter the forum password
		Response.Redirect("insufficient_permission.asp" & strQsSID1)
	End If


	'If the forum requires a password and a logged in forum code is not found on the users machine then send them to a login page
	If rsCommon("Password") <> "" AND (getCookie("fID", "Forum" & intForumID) <> rsCommon("Forum_code") AND getSessionItem("Forum" & intForumID) <> rsCommon("Forum_code")) Then

		'Reset Server Objects
		rsCommon.Close
		Call closeDatabase()


		'Redirect to a page asking for the user to enter the forum password
		Response.Redirect("forum_password_form.asp?FID=" & intForumID & strQsSID3)
	End If
End If

'Clean up
rsCommon.Close



'Get what date to show topics till from querystring
If isNumeric(Request.QueryString("TS")) AND Request.QueryString("TS") <> "" Then

	Call saveSessionItem("TS", Request.QueryString("TS"))
	intShowTopicsFrom = CInt(Request.QueryString("TS"))

'Get what date to show topics
ElseIf getSessionItem("TS") <> "" Then

	intShowTopicsFrom = CInt(getSessionItem("TS"))

'Else if there is no cookie use the default set by the forum
Else
	intShowTopicsFrom = intShowTopicsWithin
End If



'Get the sort critiria
Select Case Request.QueryString("SO")
	Case "T"
		strSortBy = strDbTable & "Topic.Subject "
	Case "A"
		strSortBy = strDbTable & "Author.Username "
	Case "R"
		strSortBy = strDbTable & "Topic.No_of_replies "
	Case "V"
		strSortBy = strDbTable & "Topic.No_of_views "
	Case Else
		strSortBy = strDbTable & "Topic.Last_Thread_ID "
End Select

'Sort the direction of db results
If Request.QueryString("OB") = "desc" Then
	strSortDirection = "asc"
	strSortBy = strSortBy & "DESC"
Else
	strSortDirection = "desc"
	strSortBy = strSortBy & "ASC"
End If

'If this is the first time it is run the we want dates DESC
If Request.QueryString("OB") = "" AND Request.QueryString("SO") = "" Then
	strSortDirection = "asc"
	strSortBy = strDbTable & "Topic.Last_Thread_ID DESC"
End If



'Read in all the topics for this forum and place them in an array
strSQL = "" & _
"SELECT "
If strDatabaseType = "SQLServer" OR strDatabaseType = "Access" Then
	strSQL = strSQL & " TOP " & intMaxResults & " "
End If
strSQL = strSQL & _
" " & strDbTable & "Topic.Topic_ID, " & strDbTable & "Topic.Poll_ID, " & strDbTable & "Topic.Moved_ID, " & strDbTable & "Topic.Subject, " & strDbTable & "Topic.Icon, " & strDbTable & "Topic.Start_Thread_ID, " & strDbTable & "Topic.Last_Thread_ID, " & strDbTable & "Topic.No_of_replies, " & strDbTable & "Topic.No_of_views, " & strDbTable & "Topic.Locked, " & strDbTable & "Topic.Priority, " & strDbTable & "Topic.Hide, " & strDbTable & "Thread.Message_date, " & strDbTable & "Thread.Message, " & strDbTable & "Thread.Author_ID, " & strDbTable & "Author.Username, LastThread.Message_date, LastThread.Author_ID, LastAuthor.Username, " & strDbTable & "Topic.Event_date, " & strDbTable & "Topic.Event_date_end " & _
"FROM " & strDbTable & "Topic" & strDBNoLock & ", " & strDbTable & "Thread" & strDBNoLock & ", " & strDbTable & "Thread AS LastThread" & strDBNoLock & ", " & strDbTable & "Author" & strDBNoLock & ", " & strDbTable & "Author AS LastAuthor" & strDBNoLock & " " & _
"WHERE ("

'Do the table joins
strSQL = strSQL & " " & strDbTable & "Thread.Author_ID = " & strDbTable & "Author.Author_ID " & _
	"AND LastThread.Author_ID = LastAuthor.Author_ID " & _
	"AND " & strDbTable & "Topic.Start_Thread_ID = " & strDbTable & "Thread.Thread_ID " & _
	"AND " & strDbTable & "Topic.Last_Thread_ID = LastThread.Thread_ID "

'If there is a date to show topics with then apply it to the SQL query
If intShowTopicsFrom <> 0 Then

	strSQL = strSQL & "AND ((LastThread.Message_date > "

	'If Access use # around dates, other DB's use ' around dates
	If strDatabaseType = "Access" Then
		strSQL = strSQL & "#"
	Else
		strSQL = strSQL & "'"
	End If

	'Initialse the string to display when active topics are shown since
	Select Case intShowTopicsFrom
		Case 1
			strShowTopicsFrom = strTxtLastVisitOn & " " & DateFormat(dtmLastVisitDate) & " " & strTxtAt & " " & TimeFormat(dtmLastVisitDate)
			strShowTopicsDate = internationalDateTime(dtmLastVisitDate)
		Case 2
			strShowTopicsFrom = strTxtYesterday
			strShowTopicsDate = internationalDateTime(DateAdd("d", -1, now()))
		Case 3
			strShowTopicsFrom = strTxtLastTwoDays
			strShowTopicsDate = internationalDateTime(DateAdd("d", -2, now()))
		Case 4
			strShowTopicsFrom = strTxtLastWeek
			strShowTopicsDate = internationalDateTime(DateAdd("ww", -1, now()))
		Case 5
			strShowTopicsFrom = strTxtLastMonth
			strShowTopicsDate = internationalDateTime(DateAdd("m", -1, now()))
		Case 6
			strShowTopicsFrom = strTxtLastTwoMonths
			strShowTopicsDate = internationalDateTime(DateAdd("m", -2, now()))
		Case 7
			strShowTopicsFrom = strTxtLastSixMonths
			strShowTopicsDate = internationalDateTime(DateAdd("m", -6, now()))
		Case 8
			strShowTopicsFrom = strTxtLastYear
			strShowTopicsDate = internationalDateTime(DateAdd("yyyy", -1, now()))
	End Select


	'If SQL server remove dash (-) from the ISO international date to make SQL Server safe
	If strDatabaseType = "SQLServer" Then strShowTopicsDate = Replace(strShowTopicsDate, "-", "", 1, -1, 1)

	'Place into SQL query
	strSQL = strSQL & strShowTopicsDate

	'If Access use # around dates, other DB's use ' around dates
	If strDatabaseType = "Access" Then
		strSQL = strSQL & "#"
	Else
		strSQL = strSQL & "'"
	End If

	strSQL = strSQL & ") OR (" & strDbTable & "Topic.Priority > 0)) "
	
	
	
End If

'Select which topics to get
strSQL = strSQL & "AND (" & strDbTable & "Topic.Priority = 3 " & _
		"OR " & strDbTable & "Topic.Moved_ID = " & intForumID & " " & _
		"OR " & strDbTable & "Topic.Forum_ID = " & intForumID & ") " & _
	") "

'If this isn't a moderator only display hidden posts if the user posted them
If blnModerator = false AND blnAdmin = false Then
	strSQL = strSQL & "AND (" & strDbTable & "Topic.Hide = " & strDBFalse & " "
	'Don't display hidden posts if guest
	If intGroupID <> 2 Then strSQL = strSQL & "OR " & strDbTable & "Thread.Author_ID = " & lngLoggedInUserID
	strSQL = strSQL & ") "
End If

'Order by
strSQL = strSQL & "ORDER BY " & strDbTable & "Topic.Priority DESC, " & strSortBy & " "

'mySQL limit operator
If strDatabaseType = "mySQL" Then
	strSQL = strSQL & " LIMIT " & intMaxResults
End If
strSQL = strSQL & ";"



'Set error trapping
On Error Resume Next

'Query the database
rsCommon.Open strSQL, adoCon

'If an error has occurred write an error to the page
If Err.Number <> 0 Then	Call errorMsg("An error has occurred while executing SQL query on database.", "get_topics_data", "forum_topics.asp")

'Disable error trapping
On Error goto 0




'SQL Query Array Look Up table
'0 = tblTopic.Topic_ID
'1 = tblTopic.Poll_ID
'2 = tblTopic.Moved_ID
'3 = tblTopic.Subject
'4 = tblTopic.Icon
'5 = tblTopic.Start_Thread_ID
'6 = tblTopic.Last_Thread_ID
'7 = tblTopic.No_of_replies
'8 = tblTopic.No_of_views
'9 = tblTopic.Locked
'10 = tblTopic.Priority
'11 = tblTopic.Hide
'12 = tblThread.Message_date
'13 = tblThread.Message,
'14 = tblThread.Author_ID,
'15 = tblAuthor.Username,
'16 = LastThread.Message_date,
'17 = LastThread.Author_ID,
'18 = LastAuthor.Username
'19 = tblTopic.Event_date
'20 = tblTopic.Event_date_end



'Read in some details of the topics
If NOT rsCommon.EOF Then

	'Read in the Topic recordset into an array
	sarryTopics = rsCommon.GetRows()

	'Count the number of records
	intTotalRecords = Ubound(sarryTopics,2) + 1

	'Count the number of pages for the topics using FIX so that we get the whole number and  not any fractions
	intTotalRecordsPages = FIX(intTotalRecords / intTopicPerPage)

	'If there is a remainder or the result is 0 then add 1 to the total num of pages
	If intTotalRecords Mod intTopicPerPage > 0 OR intTotalRecordsPages = 0 Then intTotalRecordsPages = intTotalRecordsPages + 1

	'Start position
	intStartPosition = ((intRecordPositionPageNum - 1) * intTopicPerPage)

	'End Position
	intEndPosition = intStartPosition + intTopicPerPage

	'Get the start position
	intCurrentRecord = intStartPosition
End If

'Close the recordset
rsCommon.Close



'Read the various forums from the database
'Initalise the strSQL variable with an SQL statement to query the database
strSQL = "" & _
"SELECT " & strDbTable & "Forum.Forum_ID, " & strDbTable & "Forum.Forum_name, " & strDbTable & "Forum.Forum_description, " & strDbTable & "Forum.No_of_topics, " & strDbTable & "Forum.No_of_posts, " & strDbTable & "Author.Username, " & strDbTable & "Forum.Last_post_author_ID, " & strDbTable & "Forum.Last_post_date, " & strDbTable & "Forum.Password, " & strDbTable & "Forum.Locked, " & strDbTable & "Forum.Hide, " & strDbTable & "Permissions.View_Forum, " & strDbTable & "Forum.Last_topic_ID, " & strDbTable & "Topic.Subject " & _
"FROM (((" & strDbTable & "Category INNER JOIN " & strDbTable & "Forum ON " & strDbTable & "Category.Cat_ID = " & strDbTable & "Forum.Cat_ID) LEFT JOIN " & strDbTable & "Topic ON " & strDbTable & "Forum.Last_topic_ID = " & strDbTable & "Topic.Topic_ID) INNER JOIN " & strDbTable & "Author ON " & strDbTable & "Forum.Last_post_author_ID = " & strDbTable & "Author.Author_ID) INNER JOIN " & strDbTable & "Permissions ON " & strDbTable & "Forum.Forum_ID = " & strDbTable & "Permissions.Forum_ID " & _
"WHERE " & strDbTable & "Forum.Sub_ID = " & intForumID & " " & _
	"AND (" & strDbTable & "Permissions.Author_ID = " & lngLoggedInUserID & " OR " & strDbTable & "Permissions.Group_ID = " & intGroupID & ") " & _
"ORDER BY " & strDbTable & "Forum.Forum_Order, " & strDbTable & "Permissions.Forum_ID;"


'Set error trapping
On Error Resume Next

'Query the database
rsCommon.Open strSQL, adoCon

'If an error has occurred write an error to the page
If Err.Number <> 0 Then	Call errorMsg("An error has occurred while executing SQL query on database.", "get_sub_forum_data", "forum_topics.asp")

'Disable error trapping
On Error goto 0

'If there are sub forums to dsiplay, then display them
If NOT rsCommon.EOF Then

	'Read in the sub forum details into an array
	sarrySubForums = rsCommon.GetRows()
End If

'Close the recordset
rsCommon.Close





'Use the application session to pass around what forum this user is within
Call saveSessionItem("FID", intForumID)



'Page to link to for mutiple page (with querystrings if required)
strLinkPage = "forum_topics.asp?FID=" & intForumID & "&amp;"






'If active users is enabled update the active users application array
If blnActiveUsers Then
	'Call active users function
	saryActiveUsers = activeUsers(strTxtViewingIndex, strForumName, "forum_topics.asp?FID=" & intForumID, 0)
End If






'Set bread crumb trail
'Display the category name
strBreadCrumbTrail = strBreadCrumbTrail & strNavSpacer & "<a href=""default.asp?C=" & intCatID & strQsSID2 & """>" & strCatName & "</a>" & strNavSpacer

'Display if there is a main forum to the sub forums name
If intMasterForumID <> 0 Then strBreadCrumbTrail = strBreadCrumbTrail & "<a href=""forum_topics.asp?FID=" & intMasterForumID & strQsSID2 & """>" & strMasterForumName & "</a>" & strNavSpacer

'Display forum name
If strForumName = "" Then strBreadCrumbTrail = strBreadCrumbTrail &  strTxtNoForums Else strBreadCrumbTrail = strBreadCrumbTrail & "<a href=""forum_topics.asp?FID=" & intForumID & strQsSID2 & """>" & strForumName & "</a>"






'Set the status bar tools

'Modertor Tools
If blnAdmin OR blnModerator Then
	strStatusBarTools = strStatusBarTools & "&nbsp;&nbsp;<span id=""modTools"" onclick=""showDropDown('modTools', 'modToolsMenu', 120, 0);"" class=""dropDownPointer""><img src=""" & strImagePath & "moderator_tools." & strForumImageType & """ alt=""" & strTxtModeratorTools & """ title=""" & strTxtModeratorTools & """ style=""vertical-align: text-bottom"" /> " & strTxtModeratorTools & "</span>" & _
	"<div id=""modToolsMenu"" class=""dropDownMenu"">" & _
	"<a href=""pre_approved_topics.asp" & strQsSID1 & """><div>" & strTxtHiddenTopics & "</div></a>" & _
	"<a href=""resync_post_count.asp?FID=" & intForumID & strQsSID2 & """><div>" & strTxtResyncTopicPostCount & "</div></a>"
	
	'Lock or un-lock forum if admin
	If blnAdmin AND blnForumLocked Then
		strStatusBarTools = strStatusBarTools & "<a href=""lock_forum.asp?mode=UnLock&amp;FID=" & intForumID & strQsSID2 & """><div>" & strTxtUnForumLocked & "</div></a>"
	Else
		strStatusBarTools = strStatusBarTools & "<a href=""lock_forum.asp?mode=Lock&amp;FID=" & intForumID & strQsSID2 & """><div>" & strTxtLockForum & "</div></a>"
	End If
	
	strStatusBarTools = strStatusBarTools & "</div>"
End If


'Topic Options drop down
strStatusBarTools = strStatusBarTools & "&nbsp;&nbsp;<span id=""forumOptions"" onclick="""
'If we need a subscription link then include a call to the ajax function
If blnEmail AND blnLoggedInUserEmail AND intGroupID <> 2 AND blnActiveMember Then strStatusBarTools = strStatusBarTools & "getAjaxData('ajax_email_notify.asp?FID=" & intForumID & "&amp;PN=" & intRecordPositionPageNum & strQsSID2 & "', 'ajaxEmailSub');"
	
strStatusBarTools = strStatusBarTools & "showDropDown('forumOptions', 'optionsMenu', 132, 0);"" class=""dropDownPointer""><img src=""" & strImagePath & "forum_options." & strForumImageType & """ alt=""" & strTxtForumOptions & """ title=""" & strTxtForumOptions & """ style=""vertical-align: text-bottom"" /> <a href=""#"">" & strTxtForumOptions & "</a></span>" & _
"<div id=""optionsMenu"" class=""dropDownStatusBar"">" & _
"<a href=""new_topic_form.asp?FID=" & intForumID & strQsSID2 & """ rel=""nofollow""><div>" & strTxtCreateNewTopic & "</div></a>"

'If the user can create a poll disply a create poll link
If blnPollCreate Then strStatusBarTools = strStatusBarTools & "<a href=""new_poll_form.asp?FID=" & intForumID & strQsSID2 & """ rel=""nofollow""><div>" & strTxtCreateNewPoll & "</div></a>"

'Display option to subscribe or un-subscribe to forum
If blnEmail AND blnLoggedInUserEmail AND intGroupID <> 2 AND blnActiveMember Then strStatusBarTools = strStatusBarTools & "<span id=""ajaxEmailSub""></span>"

strStatusBarTools = strStatusBarTools & "</div>"


'Active Topics Links
strStatusBarTools = strStatusBarTools & "&nbsp;&nbsp;<img src=""" & strImagePath & "active_topics." & strForumImageType & """ alt=""" & strTxtActiveTopics & """ title=""" & strTxtActiveTopics & """ style=""vertical-align: text-bottom"" /> <a href=""active_topics.asp" & strQsSID1 & """>" & strTxtActiveTopics & "</a>"

'If RSS XML enabled then display an RSS button to link to XML file
If blnRSS Then strStatusBarTools = strStatusBarTools & " <a href=""RSS_topic_feed.asp?FID=" & intForumID & strQsSID2 & """ target=""_blank""><img src=""" & strImagePath & "rss." & strForumImageType & """ alt=""" & strTxtRSS & ": " & strForumName & """ title=""" & strTxtRSS & ": " & strForumName & """ /></a>"


%>
<!-- #include file="includes/browser_page_encoding_inc.asp" -->
<title>Bloemfontein Celtic : <% = strForumName %></title>
<meta name="generator" content="Web Wiz Forums" />

<%
'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
Response.Write("<!--//" & _
vbCrLf & "/* *******************************************************" & _
vbCrLf & "Software: Web Wiz Forums(TM) ver. " & strVersion & "" & _
vbCrLf & "Info: http://www.webwizforums.com" & _
vbCrLf & "Copyright: (C)2001-2008 Web Wiz(TM). All rights reserved" & _
vbCrLf & "******************************************************* */" & _
vbCrLf & "//-->")
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******


'If RSS feed is enabled then have an alt link to it for browers that support RSS Feeds
If blnRSS Then Response.Write(vbCrLf & "<link rel=""alternate"" type=""application/rss+xml"" title=""RSS 2.0"" href=""RSS_topic_feed.asp?FID=" & intForumID & strQsSID2 & """ />")
%>

<script language="JavaScript" type="text/javascript">
function ShowTopics(Show){
   	strShow = escape(Show.options[Show.selectedIndex].value);
   	if (Show != '') self.location.href = 'forum_topics.asp?FID=<% = intForumID %>&TS=' + strShow + '<% = strQsSID2 %>';
	return true;
}
</script>
<link href="<% = strCSSfile %>default_style.css" rel="stylesheet" type="text/css" />
<!-- #include file="includes/header.asp" -->
<!-- #include file="includes/status_bar_header_inc.asp" -->
<table class="basicTable" cellspacing="0" cellpadding="3" align="center">
 <tr>
  <td align="left"><h1><%

'If the forum is locked show a locked pad lock icon
If blnForumLocked = True Then Response.Write ("<img src=""" & strImagePath & "forum_lock." & strForumImageType & """ alt=""" & strTxtForumLocked & """ title=""" & strTxtForumLocked & """ style=""vertical-align: text-bottom"" />")

'Display the forum name
Response.Write(strForumName)

%></h1></td>
</tr>
</table><%


'******************************************
'***	      Display Sub Forums	***
'******************************************

'If there are sub forums to dsiplay, then display them
If isArray(sarrySubForums) Then


	'SQL Query Array Look Up table
	'0 = Forum_ID
	'1 = Forum_name
	'2 = Forum_description
	'3 = No_of_topics
	'4 = No_of_posts
	'5 = Username
	'6 = Last_post_author_ID
	'7 = Last_post_date
	'8 = Password
	'9 = Locked
	'10 = Hide
	'11 = Read
	'12 = Last_topic_ID
	'13 = Topic.Subject


	'First we need to loop through the array to see if the user can view any of the sub forums if not don't display them
	'If we find a sub forum that the user can view we exit this loop to display the sub forum
	Do While intSubCurrentRecord <= Ubound(sarrySubForums,2)

		'Read in details
		blnHideForum = CBool(sarrySubForums(10,intSubCurrentRecord))
		blnRead = CBool(sarrySubForums(11,intSubCurrentRecord))

		'If this forum is to be shown then leave the loop and display the sub forums
		If blnHideForum = False OR blnRead Then Exit Do

		'Move to next record
		intSubCurrentRecord = intSubCurrentRecord + 1
	Loop


	'If there are still records left in the array then these are the forums that the user can view so display them
	If intSubCurrentRecord <= Ubound(sarrySubForums,2) Then


%>
<br />
<table class="tableBorder" align="center" cellspacing="1" cellpadding="3">
 <tr class="tableLedger">
  <td width="5%">&nbsp;</td>
  <td width="50%"><% = strTxtSub & " " & strTxtForums %></td>
  <td width="10%" align="center"><% = strTxtTopics %></td>
  <td width="10%" align="center"><% = strTxtPosts %></td>
  <td width="30%"><% = strTxtLastPost %></td>
 </tr><%

		'Loop through the array and display the forums
		Do While intSubCurrentRecord <= Ubound(sarrySubForums,2)

			'Initialise variables
			lngLastEntryTopicID = 0

			'Read in the details for this forum
			intSubForumID = CInt(sarrySubForums(0,intSubCurrentRecord))
			strForumName = sarrySubForums(1,intSubCurrentRecord)
			strForumDiscription = sarrySubForums(2,intSubCurrentRecord)
			lngNumberOfTopics = CLng(sarrySubForums(3,intSubCurrentRecord))
			lngNumberOfPosts = CLng(sarrySubForums(4,intSubCurrentRecord))
			strLastEntryUser = sarrySubForums(5,intSubCurrentRecord)
			If isNumeric(sarrySubForums(6,intSubCurrentRecord)) Then lngLastEntryUserID = CLng(sarrySubForums(6,intSubCurrentRecord)) Else lngLastEntryUserID = 0
			If isDate(sarrySubForums(7,intSubCurrentRecord)) Then dtmLastEntryDate = CDate(sarrySubForums(7,intSubCurrentRecord)) Else dtmLastEntryDate = CDate("2001-01-01 00:00:00")
			If isNull(sarrySubForums(8, intSubCurrentRecord)) Then strForumPassword = "" Else strForumPassword = sarrySubForums(8, intSubCurrentRecord)
			blnForumLocked = CBool(sarrySubForums(9,intSubCurrentRecord))
			blnHideForum = CBool(sarrySubForums(10,intSubCurrentRecord))
			blnRead = CBool(sarrySubForums(11,intSubCurrentRecord))
			If isNumeric(sarrySubForums(12,intSubCurrentRecord)) Then lngTopicID = CLng(sarrySubForums(12,intSubCurrentRecord)) Else lngTopicID = 0
			strSubject = sarrySubForums(13,intSubCurrentRecord)	

			'If this forum is to be hidden but the user is allowed access to it set the hidden boolen back to false
			If blnHideForum AND blnRead Then blnHideForum = False
				
			
			'Unread Posts *********
			intUnReadPostCount = 0
					
			'If there is a newer post than the last time the unread posts array was initilised run it again
			If dtmLastEntryDate > CDate(Session("dtmUnReadPostCheck")) Then Call UnreadPosts()
						
			'Count the number of unread posts in this forum
			If isArray(sarryUnReadPosts) AND dtmLastEntryDate > dtmLastVisitDate AND blnRead Then
				For intUnReadForumPostsLoop = 0 to UBound(sarryUnReadPosts,2)
					'Increament unread post count
					If CInt(sarryUnReadPosts(2,intUnReadForumPostsLoop)) = intSubForumID AND sarryUnReadPosts(3,intUnReadForumPostsLoop) = "1" Then intUnReadPostCount = intUnReadPostCount + 1
				Next	
			End If


			'If the forum is to be hidden then don't show it
			If blnHideForum = False Then

				'Get the row number
				intForumColourNumber = intForumColourNumber + 1

				'Write the HTML of the forum descriptions and hyperlinks to the forums

				'Calculate row colour
				Response.Write(vbCrLf & " <tr ")
				If (intForumColourNumber MOD 2 = 0 ) Then Response.Write("class=""evenTableRow"">") Else Response.Write("class=""oddTableRow"">")


				'Display the status forum icons
				Response.Write(vbCrLf & "   <td align=""center"">")
				%><!-- #include file="includes/forum_status_icons_inc.asp" --><%
     				Response.Write("</td>" & _
				vbCrLf & "  <td>")


				'Display forum
				Response.Write("<a href=""forum_topics.asp?FID=" & intSubForumID & strQsSID2 & """>" & strForumName & "</a>")
				
				'Display the number of people viewing in that forum
				If blnForumViewing AND blnActiveUsers Then 
					If viewingForum(intSubForumID) > 0 Then Response.Write(" <span class=""smText"">(" & viewingForum(intSubForumID) & " " & strTxtViewing & ")</span>")
				End If
				
				'Display forum details
				Response.Write("<br />" & strForumDiscription & "</td>" & _
				vbCrLf & "  <td align=""center"">" & lngNumberOfTopics & "</td>" & _
				vbCrLf & "  <td align=""center"">" & lngNumberOfPosts & "</td>" & _
				vbCrLf & "  <td class=""smText""  nowrap=""nowrap"">")
				If lngNumberOfPosts <> 0 Then 'Don't disply last post details if there are none
					
					'Don't dispaly details if the user has no read access on the forum
					If blnRead AND strForumPassword = "" Then
						
						'Display last post subject
						Response.Write("<a href=""forum_posts.asp?TID=" & lngTopicID & strQsSID2 & """ title=""" & strSubject & """>")
						If blnBoldNewTopics AND intUnReadPostCount > 0 Then 'Unread topic subjects in bold
							Response.Write("<strong>" & TrimString(strSubject, 25) & "</strong>")
						Else
							Response.Write(TrimString(strSubject, 25))
						End If
						Response.Write("</a><br />")
							
						'Who last post is by
						Response.Write(strTxtBy & "&nbsp;<a href=""member_profile.asp?PF=" & lngLastEntryUserID & strQsSID2 & """ class=""smLink"" rel=""nofollow"">" & strLastEntryUser & "</a> ")
						'If there are unread posts in the forum display differnt icon
						If intUnReadPostCount > 0 Then
							Response.Write("<a href=""get_last_post.asp?TID=" & lngTopicID & strQsSID2 & """><img src=""" & strImagePath & "view_unread_post." & strForumImageType & """ alt=""" & strTxtViewUnreadPost & " [" & intUnReadPostCount & " " & strTxtNewPosts & "]"" title=""" & strTxtViewUnreadPost & " [" & intUnReadPostCount & " " & strTxtNewPosts & "]"" /></a>")
						
						'Else there are no unread posts so display a normal last post link
						Else
							Response.Write("<a href=""get_last_post.asp?TID=" & lngTopicID & strQsSID2 & """><img src=""" & strImagePath & "view_last_post." & strForumImageType & """ alt=""" & strTxtViewLastPost & """ title=""" & strTxtViewLastPost & """ /></a>")
						End If
						
					End If
					'Last post date
					Response.Write("<br />" & DateFormat(dtmLastEntryDate) & "&nbsp;" &  strTxtAt & "&nbsp;" & TimeFormat(dtmLastEntryDate))
				End If
				Response.Write("</td>"  & _
				vbCrLf & " </tr>")


			End If

			'Move to the next database record
			intSubCurrentRecord = intSubCurrentRecord + 1


			'If there are more records in the array to display then run some test to see what record to display next and where
			If intSubCurrentRecord <= Ubound(sarrySubForums,2) Then

				'Becuase the member may have an individual permission entry in the permissions table for this forum,
				'it maybe listed twice in the array, so we need to make sure we don't display the same forum twice
				If intSubForumID = CInt(sarrySubForums(0,intSubCurrentRecord)) Then intSubCurrentRecord = intSubCurrentRecord + 1

				'If there are no records left exit loop
				If intSubCurrentRecord > Ubound(sarrySubForums,2) Then Exit Do
			End If

		'Loop back round for next forum
		Loop

%>
</table>
<br /><%
	End If
End If





'******************************************
'***	      Display Topics		***
'******************************************

%>
<table class="basicTable" cellspacing="1" cellpadding="3" align="center">
 <tr>
  <td width="<% If blnPollCreate Then Response.Write("205") Else Response.Write("100") %>">
   <a href="new_topic_form.asp?FID=<% = intForumID & strQsSID2 %>" title="<% = strTxtCreateNewTopic %>" class="largeButton" rel="nofollow">&nbsp;<% = strTxtNewTopic %> <img src="<% = strImagePath %>new_topic.<% = strForumImageType %>" alt="<% = strTxtCreateNewTopic %>" /></a><%

'If the user can create a poll disply a create poll link
If blnPollCreate Then Response.Write ("<a href=""new_poll_form.asp?FID=" & intForumID & strQsSID2 & """ title=""" & strTxtCreateNewPoll & """ class=""largeButton"" rel=""nofollow"">&nbsp;&nbsp;" & strTxtNewPoll & "&nbsp;&nbsp;<img src=""" & strImagePath & "new_poll." & strForumImageType & """ alt=""" & strTxtCreateNewPoll & """></a>")

%>
  </td>
  <td><% = strTxtShowTopics %>
   <select name="show" id="show" onchange="ShowTopics(this)">
    <option value="0"<% If intShowTopicsFrom = 0 Then Response.Write " selected=""selected""" %>><% = strTxtAnyDate %></option>
    <option value="1"<% If intShowTopicsFrom = 1 Then Response.Write " selected=""selected""" %>><% = DateFormat(dtmLastVisitDate) & " " & strTxtAt & " " & TimeFormat(dtmLastVisitDate) %></option>
    <option value="2"<% If intShowTopicsFrom = 2 Then Response.Write " selected=""selected""" %>><% = strTxtYesterday %></option>
    <option value="3"<% If intShowTopicsFrom = 3 Then Response.Write " selected=""selected""" %>><% = strTxtLastTwoDays %></option>
    <option value="4"<% If intShowTopicsFrom = 4 Then Response.Write " selected=""selected""" %>><% = strTxtLastWeek %></option>
    <option value="5"<% If intShowTopicsFrom = 5 Then Response.Write " selected=""selected""" %>><% = strTxtLastMonth %></option>
    <option value="6"<% If intShowTopicsFrom = 6 Then Response.Write " selected=""selected""" %>><% = strTxtLastTwoMonths %></option>
    <option value="7"<% If intShowTopicsFrom = 7 Then Response.Write " selected=""selected""" %>><% = strTxtLastSixMonths %></option>
    <option value="8"<% If intShowTopicsFrom = 8 Then Response.Write " selected=""selected""" %>><% = strTxtLastYear %></option>
   </select>
  </td>
  <td align="right"><%

'If there is more than one page display links to the others
If intTotalRecordsPages > 1 Then
	%><!-- #include file="includes/page_link_inc.asp" --><%

End If

%>
  </td>
 </tr>
</table>
<table cellspacing="1" cellpadding="3" class="tableBorder" align="center">
 <tr class="tableLedger">
   <td width="5%">&nbsp;</td>
   <td width="50%"><a href="forum_topics.asp?FID=<% = intForumID %>&amp;SO=T<% = strQsSID2 %>" title="<% = strTxtReverseSortOrder %>"><% = strTxtTopics %></a><% If Request.QueryString("SO") = "T" Then Response.Write(" <a href=""forum_topics.asp?FID=" & intForumID & "&amp;SO=T&amp;OB=" & strSortDirection & strQsSID2 & """><img src=""" & strImagePath & strSortDirection & "." & strForumImageType & """ title=""" & strTxtReverseSortOrder & """ alt=""" & strTxtReverseSortOrder & """ /></a>") %> / <a href="forum_topics.asp?FID=<% = intForumID %>&amp;SO=A<% = strQsSID2 %>" title="<% = strTxtReverseSortOrder %>"><% = strTxtThreadStarter %></a><% If Request.QueryString("SO") = "A" Then Response.Write(" <a href=""forum_topics.asp?FID=" & intForumID & "&amp;SO=A&amp;OB=" & strSortDirection & strQsSID2 & """><img src=""" & strImagePath & strSortDirection & "." & strForumImageType & """ alt=""" & strTxtReverseSortOrder & """ title=""" & strTxtReverseSortOrder & """ /></a>") %></td>
   <td width="10%" align="center" nowrap="nowrap"><a href="forum_topics.asp?FID=<% = intForumID %>&amp;SO=R<% = strQsSID2 %>" title="<% = strTxtReverseSortOrder %>"><% = strTxtReplies %></a><% If Request.QueryString("SO") = "R" Then Response.Write(" <a href=""forum_topics.asp?FID=" & intForumID & "&amp;SO=R&amp;OB=" & strSortDirection & strQsSID2 & """><img src=""" & strImagePath & strSortDirection & "." & strForumImageType & """ title=""" & strTxtReverseSortOrder & """ alt=""" & strTxtReverseSortOrder & """ /></a>") %></td>
   <td width="10%" align="center" nowrap="nowrap"><a href="forum_topics.asp?FID=<% = intForumID %>&amp;SO=V<% = strQsSID2 %>" title="<% = strTxtReverseSortOrder %>"><% = strTxtViews %></a><% If Request.QueryString("SO") = "V" Then Response.Write(" <a href=""forum_topics.asp?FID=" & intForumID & "&amp;SO=V&amp;OB=" & strSortDirection & strQsSID2 & """><img src=""" & strImagePath & strSortDirection & "." & strForumImageType & """ title=""" & strTxtReverseSortOrder & """ alt=""" & strTxtReverseSortOrder & """ /></a>") %></td>
   <td width="30%"><a href="forum_topics.asp?FID=<% = intForumID %><% = strQsSID2 %>" title="<% = strTxtReverseSortOrder %>"><% = strTxtLastPost %></a><% If Request.QueryString("SO") = "" Then Response.Write(" <a href=""forum_topics.asp?FID=" & intForumID & "&amp;OB=" & strSortDirection & strQsSID2 & """><img src=""" & strImagePath & strSortDirection & "." & strForumImageType & """ title=""" & strTxtReverseSortOrder & """ alt=""" & strTxtReverseSortOrder & """ /></a>") %></td>
  </tr><%


'If there are no topics to display, show a message saying so
If intTotalRecords <= 0 Then

	'If there are no Topic's to display then display the appropriate error message
	Response.Write vbCrLf & "  <tr class=""tableRow""><td colspan=""6"" align=""center""><br />" & strTxtNoTopicsToDisplay & "&nbsp;" & strShowTopicsFrom & "<br /><br /></td></tr>"

'Else there the are topic's so write the HTML to display the topic names and a discription
Else

	'Do....While Loop to loop through the recorset to display the forum topics
	Do While intCurrentRecord < intEndPosition

		'If there are no topic records left to display then exit loop
		If intCurrentRecord >= intTotalRecords Then Exit Do

		'SQL Query Array Look Up table
		'0 = tblTopic.Topic_ID
		'1 = tblTopic.Poll_ID
		'2 = tblTopic.Moved_ID
		'3 = tblTopic.Subject
		'4 = tblTopic.Icon
		'5 = tblTopic.Start_Thread_ID
		'6 = tblTopic.Last_Thread_ID
		'7 = tblTopic.No_of_replies
		'8 = tblTopic.No_of_views
		'9 = tblTopic.Locked
		'10 = tblTopic.Priority
		'11 = tblTopic.Hide
		'12 = tblThread.Message_date
		'13 = tblThread.Message,
		'14 = tblThread.Author_ID,
		'15 = tblAuthor.Username,
		'16 = LastThread.Message_date,
		'17 = LastThread.Author_ID,
		'18 = LastAuthor.Username
		'19 = tblTopic.Event_date
		'20 = tblTopic.Event_date_end

		'Read in Topic details from the database
		lngTopicID = CLng(sarryTopics(0,intCurrentRecord))
		lngPollID = CLng(sarryTopics(1,intCurrentRecord))
		intMovedForumID = CInt(sarryTopics(2,intCurrentRecord))
		strSubject = sarryTopics(3,intCurrentRecord)
		strTopicIcon = sarryTopics(4,intCurrentRecord)
		lngNumberOfReplies = CLng(sarryTopics(7,intCurrentRecord))
		lngNumberOfViews = CLng(sarryTopics(8,intCurrentRecord))
		blnTopicLocked = CBool(sarryTopics(9,intCurrentRecord))
		intPriority = CInt(sarryTopics(10,intCurrentRecord))
		blnHideTopic = CBool(sarryTopics(11,intCurrentRecord))
		dtmEventDate = sarryTopics(19,intCurrentRecord)
		dtmEventDateEnd = sarryTopics(20,intCurrentRecord)

		'Read in the first post details
		dtmFirstEntryDate = CDate(sarryTopics(12,intCurrentRecord))
		strFirstPostMsg = Mid(sarryTopics(13,intCurrentRecord), 1, 275)
		lngTopicStartUserID = CLng(sarryTopics(14,intCurrentRecord))
		strTopicStartUsername = sarryTopics(15,intCurrentRecord)

		'Read in the last post details
		lngLastEntryMessageID = CLng(sarryTopics(6,intCurrentRecord))
		dtmLastEntryDate = CDate(sarryTopics(16,intCurrentRecord))
		lngLastEntryUserID = CLng(sarryTopics(17,intCurrentRecord))
		strLastEntryUsername = sarryTopics(18,intCurrentRecord)
		

		'Clean up input to prevent XXS hack
		strSubject = formatInput(strSubject)

		'Remove HTML from message for subject link title
		strFirstPostMsg = removeHTML(strFirstPostMsg, 150, true)

		'Clean up input to prevent XXS hack
		strFirstPostMsg = formatInput(strFirstPostMsg)
		
		
		
		
		'Unread Posts *********
		intUnReadPostCount = 0
					
		'If there is a newer post than the last time the unread posts array was initilised run it again
		If dtmLastEntryDate > CDate(Session("dtmUnReadPostCheck")) Then Call UnreadPosts()
						
		'Count the number of unread posts in this forum
		If isArray(sarryUnReadPosts) AND dtmLastEntryDate > dtmLastVisitDate Then
			For intUnReadForumPostsLoop = 0 to UBound(sarryUnReadPosts,2)
				'Increament unread post count
				If CLng(sarryUnReadPosts(1,intUnReadForumPostsLoop)) = lngTopicID AND sarryUnReadPosts(3,intUnReadForumPostsLoop) = "1" Then intUnReadPostCount = intUnReadPostCount + 1
			Next	
		End If
		

		'Get a record number for the number of non priority posts
		If intPriority <= 1 Then intNonPriorityTopicNum = intNonPriorityTopicNum + 1

		'If this is the first topic that is not important then display the forum topics bar
		If intNonPriorityTopicNum = 1 Then Response.Write vbCrLf & "    <tr class=""tableSubLedger""><td colspan=""6"">" & strTxtForum & " " & strTxtTopics & "</td></tr>"

		'If this is the first topic and it is an important one then display a bar saying so
		If intCurrentRecord = 0 AND intPriority => 2 Then Response.Write vbCrLf & "<tr class=""tableSubLedger""><td colspan=""6"">" & strTxtAnnouncements & "</td></tr>"


		


		'Calculate the row colour
		If intCurrentRecord MOD 2=0 Then strTableRowColour = "evenTableRow" Else strTableRowColour = "oddTableRow"

		'If this is a hidden post then change the row colour to highlight it
		If blnHideTopic Then strTableRowColour = "hiddenTableRow"


		'Write the HTML of the Topic descriptions as hyperlinks to the Topic details and message
		Response.Write(vbCrLf & "  <tr class=""" & strTableRowColour & """>")

		
		'Display the status topic icons
		Response.Write(vbCrLf & "   <td align=""center"">")
		%><!-- #include file="includes/topic_status_icons_inc.asp" --><%
     		Response.Write("</td>")




     		Response.Write(vbCrLf & "   <td><div style=""float:left"">")
     	
		 'If the user is a forum admin or a moderator then give let them delete the topic
		 If blnAdmin  OR blnModerator Then 
		 	
		 	Response.Write("<span id=""modTools" & lngTopicID & """ onclick=""showDropDown('modTools" & lngTopicID & "', 'modToolsMenu" & lngTopicID & "', 120, 0);"" class=""dropDownPointer""><img src=""" & strImagePath & "moderator_tools." & strForumImageType & """ alt=""" & strTxtModeratorTools & """ title=""" & strTxtModeratorTools & """ /></span> " & _
			"<div id=""modToolsMenu" & lngTopicID & """ class=""dropDownStatusBar"">" & _
			"<a href=""javascript:winOpener('pop_up_topic_admin.asp?TID=" & lngTopicID & strQsSID2 & "','admin',1,1,600,285)""><div>" & strTxtTopicAdmin & "</div></a>")
			
			'Lock or un-lock forum if admin
			If blnTopicLocked Then
				Response.Write("<a href=""lock_topic.asp?mode=UnLock&amp;TID=" & lngTopicID & "&FID=" & intForumID & strQsSID2 & """><div>" & strTxtUnLockTopic & "</div></a>")
			Else
				Response.Write("<a href=""lock_topic.asp?mode=Lock&amp;TID=" & lngTopicID & "&FID=" & intForumID & strQsSID2 & """><div>" & strTxtLockTopic & "</div></a>")
			End If

			'Hide or show topic
			If blnHideTopic = false Then
				Response.Write("<a href=""lock_topic.asp?mode=Hide&amp;TID=" & lngTopicID & "&amp;FID=" & intForumID & strQsSID2 & """><div>" & strTxtHideTopic & "</div></a>")
			Else
				Response.Write("<a href=""lock_topic.asp?mode=Show&amp;TID=" & lngTopicID & "&amp;FID=" & intForumID & strQsSID2 & """><div>" & strTxtShowTopic & "</div></a>")
			End If
			
			Response.Write("<a href=""delete_topic.asp?TID=" & lngTopicID & "&amp;PN=" & intRecordPositionPageNum & strQsSID2 & """ onclick=""return confirm('" & strTxtDeleteTopicAlert & "')""><div>" & strTxtDeleteTopic & "</div></a>")
			Response.Write("</div>")
		 	
		End If
		
		
		
		'If topic icons enabled and we have a topic icon display it	
     		If blnTopicIcon AND strTopicIcon <> "" Then Response.Write("<img src=""" & strTopicIcon & """ alt=""" & strTxtMessageIcon & """ title=""" & strTxtMessageIcon & """ /> ")



		'Display the subject of the topic
		Response.Write("<a href=""forum_posts.asp?TID=" & lngTopicID)
		If intPriority = 3 Then Response.Write("&amp;FID=" & intForumID & "&amp;PR=3")
		Response.Write("" & strQsSID2 & """ title=""" & strFirstPostMsg & """>")
		If blnBoldNewTopics AND intUnReadPostCount > 0 Then 'Unread topic subjects in bold
			Response.Write("<strong>" & strSubject & "</strong>")
		Else
			Response.Write(strSubject)
		End If
		Response.Write("</a>")
		
		'Display who started the topic and when
		Response.Write("<br /><span class=""smText"">" & strTxtBy & " <a href=""member_profile.asp?PF=" & lngTopicStartUserID & strQsSID2 & """  class=""smLink"" rel=""nofollow"">" & strTopicStartUsername & "</a>, " & DateFormat(dtmFirstEntryDate) & " " & strTxtAt & " " & TimeFormat(dtmFirstEntryDate) & "</span>" & _
		"</div>")



		 'Calculate the number of pages for the topic and display links if there are more than 1 page
		 intNumberOfTopicPages = ((lngNumberOfReplies + 1)\intThreadsPerPage)

		 'If there is a remainder from calculating the num of pages add 1 to the number of pages
		 If ((lngNumberOfReplies + 1) Mod intThreadsPerPage) > 0 Then intNumberOfTopicPages = intNumberOfTopicPages + 1

		 'If there is more than 1 page for the topic display links to the other pages
		 If intNumberOfTopicPages > 1 Then

		 	Response.Write("<div style=""float:right;""><img src=""" & strImagePath & "multiple_pages." & strForumImageType & """ alt=""" & strTxtMultiplePages & """ title=""" & strTxtMultiplePages & """ />")

		 	'Loop round to display the links to the other pages
		 	For intTopicPagesLoopCounter = 1 To intNumberOfTopicPages

		 		'If there is more than 3 pages display ... last page and exit the loop
		 		If intTopicPagesLoopCounter > 3 Then

		 			'If this is position 4 then display just the 4th page
		 			If intNumberOfTopicPages = 4 Then
		 				Response.Write("<a href=""forum_posts.asp?TID=" & lngTopicID & "&amp;PN=4")
						'If a priority topic need to make sure we don't change forum
			 			If intPriority = 3 Then Response.Write("&amp;FID=" & intForumID & "&amp;PR=3")
			 			Response.Write("" & strQsSID2 & """ class=""smPageLink"" title=""" & strTxtPage & " 4"">4</a>")

					'Else display the last 2 pages
					Else

						Response.Write("&nbsp;")

						Response.Write("<a href=""forum_posts.asp?TID=" & lngTopicID & "&amp;PN=" & intNumberOfTopicPages - 1)
						'If a priority topic need to make sure we don't change forum
			 			If intPriority = 3 Then Response.Write("&amp;FID=" & intForumID & "&amp;PR=3")
			 			Response.Write("" & strQsSID2 & """ class=""smPageLink"" title=""" & strTxtPage & " " & intNumberOfTopicPages - 1 & """>" & intNumberOfTopicPages - 1 & "</a>")

			 			Response.Write("<a href=""forum_posts.asp?TID=" & lngTopicID & "&amp;PN=" & intNumberOfTopicPages)
						'If a priority topic need to make sure we don't change forum
			 			If intPriority = 3 Then Response.Write("&amp;FID=" & intForumID & "&amp;PR=3")
			 			Response.Write("" & strQsSID2 & """ class=""smPageLink"" title=""" & strTxtPage & " " & intNumberOfTopicPages & """>" & intNumberOfTopicPages & "</a>")

					End If

					'Exit the loop as we are finshed here
		 			Exit For
		 		End If

		 		'Display the links to the other pages
		 		Response.Write("<a href=""forum_posts.asp?TID=" & lngTopicID & "&amp;PN=" & intTopicPagesLoopCounter)

		 		'If a priority topic need to make sure we don't change forum
		 		If intPriority = 3 Then Response.Write("&amp;FID=" & intForumID & "&amp;PR=3")
		 		Response.Write("" & strQsSID2 & """ class=""smPageLink"" title=""" & strTxtPage & " " & intTopicPagesLoopCounter & """>" & intTopicPagesLoopCounter & "</a>")
		 	Next
		 	Response.Write("</div>")
		 End If
		  %></td>
   <td align="center"><% = lngNumberOfReplies %></td>
   <td align="center"><% = lngNumberOfViews %></td>
   <td class="smText" nowrap="nowrap"><% = strTxtBy %> <a href="member_profile.asp?PF=<% = lngLastEntryUserID & strQsSID2 %>"  class="smLink" rel="nofollow"><% = strLastEntryUsername %></a><br/> <% = DateFormat(dtmLastEntryDate) & " " & strTxtAt & " " & TimeFormat(dtmLastEntryDate) %> <%
   
   		'If there are unread posts display a differnet icon and link to the last unread post
   		If intUnReadPostCount > 0 Then
   			 
   			Response.Write("<a href=""get_last_post.asp?TID=" & lngTopicID)
   			If intPriority = 3 Then Response.Write("&amp;FID=" & intForumID & "&amp;PR=3") 
			Response.Write("" & strQsSID2 & """><img src=""" & strImagePath & "view_unread_post." & strForumImageType & """ alt=""" & strTxtViewUnreadPost & " [" & intUnReadPostCount & " " & strTxtNewPosts & "]"" title=""" & strTxtViewUnreadPost & " [" & intUnReadPostCount & " " & strTxtNewPosts & "]"" /></a> ") 
   	
   		'Else there are no unread posts so display a normal topic link
   		Else
			Response.Write("<a href=""get_last_post.asp?TID=" & lngTopicID)
			If intPriority = 3 Then Response.Write("&amp;FID=" & intForumID & "&amp;PR=3") 
			Response.Write("" & strQsSID2 & """><img src=""" & strImagePath & "view_last_post." & strForumImageType & """ alt=""" & strTxtViewLastPost & """ title=""" & strTxtViewLastPost & """ /></a> ") 
   		End If
   
   %></td>
  </tr><%

		'Move to the next record
		intCurrentRecord = intCurrentRecord + 1
	Loop
End If


        %>
</table>
<table class="basicTable" cellspacing="0" cellpadding="4" align="center">
 <tr>
  <td>
   <a href="new_topic_form.asp?FID=<% = intForumID & strQsSID2 %>" title="<% = strTxtCreateNewTopic %>" class="largeButton" rel="nofollow">&nbsp;<% = strTxtNewTopic %> <img src="<% = strImagePath %>new_topic.<% = strForumImageType %>" alt="<% = strTxtCreateNewTopic %>" /></a><%

'If the user can create a poll disply a create poll link
If blnPollCreate Then Response.Write ("<a href=""new_poll_form.asp?FID=" & intForumID & strQsSID2 & """ title=""" & strTxtCreateNewPoll & """ class=""largeButton"" rel=""nofollow"">&nbsp;&nbsp;" & strTxtNewPoll & "&nbsp;&nbsp;<img src=""" & strImagePath & "new_poll." & strForumImageType & """  alt=""" & strTxtCreateNewPoll & """></a>")

%>
  </td>
  <td align="right" nowrap="nowrap"><!-- #include file="includes/page_link_inc.asp" --></td>
 </tr>
 <tr>
  <td valign="top"><br /><!-- #include file="includes/forum_jump_inc.asp" --></td>
  <td class="smText" align="right" nowrap="nowrap"><!-- #include file="includes/show_forum_permissions_inc.asp" --></td>
 </tr>
</table>
<br />
<div align="center">
<%

'Reset Server Objects
Call closeDatabase()

'***** START WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******
If blnLCode = True Then
	If blnTextLinks = True Then
		Response.Write("<span class=""text"" style=""font-size:10px"">Bulletin Board Software by <a href=""http://www.webwizforums.com"" target=""_blank"" style=""font-size:10px"">Web Wiz Forums&reg;</a> version " & strVersion & "</span>")
		If blnACode Then Response.Write("<span class=""text"" style=""font-size:10px""> [Free Express Edition]")
	'Else
  		'Response.Write("<a href=""http://www.webwizforums.com"" target=""_blank""><img src=""webwizforums_image.asp"" border=""0"" title=""Bulletin Board Software by Web Wiz Forums&reg; version " & strVersion & """ alt=""Bulletin Board Software by Web Wiz Forums&reg; version " & strVersion& """ /></a>")
  		If blnACode Then Response.Write("<br /><span class=""text"" style=""font-size:10px"">Powered by Web Wiz Forums Free Express Edition</span>")
	End If
	
	Response.Write("<br /><span class=""text"" style=""font-size:10px"">Copyright &copy;2001-2008 <a href=""http://www.webwizguide.com"" target=""_blank"" style=""font-size:10px"">Web Wiz</a></span>")
End If
'***** END WARNING - REMOVAL OR MODIFICATION OF THIS CODE WILL VIOLATE THE LICENSE AGREEMENT ******

'Display the process time
If blnShowProcessTime Then Response.Write "<span class=""smText""><br /><br />" & strTxtThisPageWasGeneratedIn & " " & FormatNumber(Timer() - dblStartTime, 3) & " " & strTxtSeconds & "</span>"
%>
</div><%

'Display an alert message letting the user know the topic has been deleted
If Request.QueryString("DL") = "1" Then
	Response.Write("<script language=""JavaScript"" type=""text/javascript"">")
	Response.Write("alert('" & strTxtTheTopicIsNowDeleted & "')")
	Response.Write("</script>")
End If

'Display an alert message if the user is watching this forum for email notification
If Request.QueryString("EN") = "FS" Then
	Response.Write("<script language=""JavaScript"" type=""text/javascript"">")
	Response.Write("alert('" & strTxtYouAreNowBeNotifiedOfPostsInThisForum & "')")
	Response.Write("</script>")
End If

'Display an alert message if the user is not watching this forum for email notification
If Request.QueryString("EN") = "FU" Then
	Response.Write("<script language=""JavaScript"" type=""text/javascript"">")
	Response.Write("alert('" & strTxtYouAreNowNOTBeNotifiedOfPostsInThisForum & "')")
	Response.Write("</script>")
End If

%>
<!-- #include file="includes/footer.asp" -->
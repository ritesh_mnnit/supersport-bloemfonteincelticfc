<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Forums
'**  http://www.webwizforums.com
'**                                                              
'**  Copyright �2001-2007 Web Wiz. All Rights Reserved.   
'**  
'**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS UNDER LICENSE FROM 'WEB WIZ'. 
'**  
'**  IF YOU DO NOT AGREE TO THE LICENSE AGREEMENT THEN 'WEB WIZ' IS UNWILLING TO LICENSE 
'**  THE SOFTWARE TO YOU, AND YOU SHOULD DESTROY ALL COPIES YOU HOLD OF 'WEB WIZ' SOFTWARE
'**  AND DERIVATIVE WORKS IMMEDIATELY.
'**  
'**  If you have not received a copy of the license with this work then a copy of the latest
'**  license contract can be found at:-
'**
'**  http://www.webwizguide.com/license
'**
'**  For more information about this software and for licensing information please contact
'**  'Web Wiz' at the address and website below:-
'**
'**  Web Wiz, Unit 10E, Dawkins Road Industrial Estate, Poole, Dorset, BH15 4JD, England
'**  http://www.webwizguide.com
'**
'**  Removal or modification of this copyright notice will violate the license contract.
'**
'****************************************************************************************







'******************************************
'*** 	  Database System Type         ****
'******************************************

'Database Type
strDatabaseType = "SQLServer"	'Microsoft SQL Server 2000, 2005, MSDE, 2005 Express
'strDatabaseType = "mySQL"	'MySQL 4.1 or MySQL 5
'strDatabaseType = "Access"	'Microsoft Access Database (Flat database file, slowest and least secure of the 3)





'Dimension global variables
Dim adoCon 			'Database Connection Variable Object
Dim strCon			'Holds the string to connect to the db
Dim rsCommon			'Holds the configuartion recordset
Dim strSQL			'Holds the SQL query for the database
Dim strDbPathAndName		'Holds the path and name of the database
Dim strSQLServerName		'Holds the name of the SQL Server
Dim strSQLDBUserName		'Holds the user name (for SQL Server Authentication)
Dim strSQLDBPassword		'Holds the password (for SQL Server Authentication)
Dim strSQLDBName		'Holds name of a database on the server
Dim strDatabaseDateFunction	'Holds a different date function for Access or SQL server
Dim strDatabaseType		'Holds the type of database used
Dim strDBFalse			'Holds the false value for SQL queries
Dim strDBTrue			'Holds the true value for SQL queries
Dim strDBNoLock			'Holds if the database is locked while running the query for SQL Server
Dim strRowLock			'Holds if the database row is locked while running the query for SQL Server
Dim strDBTop1			'Holds the SQL limit operator (TOP 1) for SQL Server and Access
Dim strDBLimit1			'Holds the SQL limit operator (LIMIT 1) for mySQL





'******************************************
'*** 	      Microsoft Access         ****
'******************************************

If strDatabaseType = "Access" Then
	
	'Microsoft Access is a flat file database system, it suffers from slow performance, limited 
	'connections, and as a flat file it can be easly downloaded by a hacker if you do not secure 
	'the database file
	
	'Virtual path to database
	strDbPathAndName = Server.MapPath("database/wwForum.mdb")  'This is the path of the database from the applications location
	
	'Physical path to database
	'strDbPathAndName = "" 'Use this if you use the physical server path, eg:- C:\Inetpub\private\wwForum.mdb
	
	
	'PLEASE NOTE: - For extra security it is highly recommended you change the name of the database, wwForum.mdb, 
	'to another name and then replace the wwForum.mdb found above with the name you changed the forum database to.
	
	
	'Initilise the DB Connection String
	strCon = strDbPathAndName 
End If






'**********************************************************
'*** 	   Microsoft SQL Server and MySQL Server       ****
'**********************************************************

If strDatabaseType = "SQLServer" OR strDatabaseType = "mySQL" Then
	
	'*** Microsoft SQL Server ***
	'SQL Server is a high performance database server, this requires that you have installed an
	'instence SQL Server 2000 or 2005 with mixed mode connection and have created a database for
	'the Forum on the SQL Server
	
	'*** MySQL Server ***
	'MySQL is a free database server, it's a very fast and robust, this requires that you have
	'installed an instance of mySQL 4.1 or higher and have created a database for the Forum
	'on the mySQL database system
	
	
	
	'Enter the details of your Microsoft SQL Server or MySQL Server and database below
	'*********************************************************************************
	
	strSQLServerName = "sql.supersport.co.za" 'Holds the name of the SQL Server (This is the name/location or IP address of the SQL Server)
	strSQLDBUserName = "forumuser" 'Holds the user name (for SQL Server Authentication)
	strSQLDBPassword = "97Fur0M%2!rW" 'Holds the password (for SQL Server Authentication)
	strSQLDBName = "SupersportZone" 

	
	
	'Initilise the DB Connection String
	strCon = "Server=" & strSQLServerName & ";User ID=" & strSQLDBUserName & ";Password=" & strSQLDBPassword & ";Database=" & strSQLDBName & ";"
End If














'******************************************
'*** 	 Open Database Connection      ****
'******************************************

'This sub procedure opens a connection to the database and creates a recordset object and sets database defaults
Public Sub openDatabase(strCon)

	'Setup database driver and defaults
	'**********************************
	
	'SQL Server Database Defaults
	If strDatabaseType = "SQLServer" Then
		
		'Please note this application has been optimised for the SQL OLE DB Driver using another driver 
		'or system DSN to connect to the SQL Server database will course errors in the application and
		'drastically reduce the performance!
	
	
		'The SQLOLEDB driver offers the highest performance at this time for connecting to SQL Server databases from within ASP.
		
		'MS SQL Server OLE Driver (If you change this string make sure you also change it in the msSQL_server_setup.asp file when creating the database)
		strCon = "Provider=SQLOLEDB;Connection Timeout=90;" & strCon
		
		'The GetDate() function is used in SQL Server to get dates
		strDatabaseDateFunction = "GetDate()"
		
		'Set true and false for db
		strDBFalse = 0
		strDBTrue = 1
		
		'Set the lock variavbles for the db
		strDBNoLock = " WITH (NOLOCK) "
		strRowLock = " WITH (ROWLOCK) "
		
		'Set the Limit opertaor for SQL Server
		strDBTop1 = " TOP 1"
		
	
	'MySQL Server Database Defaults	
	ElseIf strDatabaseType = "mySQL" Then
		
		'This application requires the myODBC 3.51 driver
	
		'myODBC Driver 3.51
		strCon = "Driver={MySQL ODBC 3.51 Driver};Port=3306;Option=3;" & strCon
		
		'Calculate the date web server time incase the database server is out, use international date
		strDatabaseDateFunction = "'" & internationalDateTime(Now())& "'"
		
		'Set true and false for db (true value is -1)
		strDBFalse = 0
		strDBTrue = -1
		
		
		'Set the limit operator
		strDBLimit1 = " LIMIT 1"
		
	
	'MS Access Database Defaults	
	ElseIf strDatabaseType = "Access" Then
		
		'Database driver (Microsoft JET OLE DB driver version 4)
		strCon = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & strCon
		
		'The now() function is used in Access for dates
		strDatabaseDateFunction = "Now()"
		
		'Set true and false for db
		strDBFalse = "false"
		strDBTrue = "true"
			
		'Set the limit operator for Access
		strDBTop1 = " TOP 1"
		
	End If
	
	
	
	'Open Datbase Connection
	'***********************

	'Create a db connection odject
	Set adoCon = CreateObject("ADODB.Connection")
	
	'Set error trapping
	On Error Resume Next
	
	'Set the connection string to the database
	adoCon.connectionstring = strCon
	
	'Set an active connection to the Connection object
	adoCon.Open
	
	'If an error has occurred write an error to the page
	If Err.Number <> 0 Then	Call errorMsg("An error has occurred while connecting to the database.", "db_connection", "common.asp")
					
	'Disable error trapping
	On Error goto 0
	
	
	'Intialise the main ADO recordset object
	Set rsCommon = CreateObject("ADODB.Recordset")

End Sub




'******************************************
'*** 	  Close Database Connection    ****
'******************************************

'This sub procedure will close the main recordset and close the database connection
Public Sub closeDatabase()

	'Close recordset
	If isObject(rsCommon) Then
		Set rsCommon = Nothing
	End If
	
	'Close Database Connection
	If isObject(adoCon) Then
		adoCon.Close
		Set adoCon = Nothing
	End If
End Sub
%>
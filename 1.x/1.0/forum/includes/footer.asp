<!-- footer -->
<style>
/* CoolMenus 4 - default styles - do not edit */
.clCMAbs{position:absolute; visibility:hidden; left:0; top:0}
/* CoolMenus 4 - default styles - end */
  
/*Style for the background-bar*/
.clBar{position:absolute; width:10; height:10; background-color:#11527E; layer-background-color:white; visibility:hidden}

/*Styles for level 0*/
.clLevel0,.clLevel0over{position:absolute; padding:5px; font-family: Arial, Helvetica, sans-serif; font-size:10px;}
.clLevel0{background-color:#ffffff; layer-background-color:#ffffff; color:#677C9B; }
.clLevel0over{background-color:#275579; color:#ffffff; cursor:pointer; cursor:hand; }
.clLevel0border{position:absolute; visibility:hidden;}

/*Styles for level 1*/
.clLevel1, .clLevel1over{position:absolute; padding:5px; font-family:arial,helvetica; font-size:10px; font-weight:bold}
.clLevel1{background-color:white; layer-background-color:white; 
color:#677C9B;}
.clLevel1over{background-color:#ffffff; layer-background-color:#ffffff; color:#275579; cursor:pointer; cursor:hand; }
.clLevel1border{position:absolute; visibility:hidden; background-color:#E2E2E2; layer-background-color:#E2E2E2}

/*Styles for level 2*/
.clLevel2, .clLevel2over{position:absolute; padding:5px; font-family:arial,helvetica; font-size:10px; font-weight:bold}
.clLevel2{background-color:Navy; layer-background-color:Navy; color:#677C9B;}
.clLevel2over{background-color:#ffffff; layer-background-color:#ffffff; color:#275579; cursor:pointer; cursor:hand; }
.clLevel2border{position:absolute; visibility:hidden; background-color:#E2E2E2; layer-background-color:#E2E2E2}



</style>  
      <script language="JavaScript1.2" src="coolmenus4.js">
      /*****************************************************************************
      Copyright (c) 2001 Thomas Brattli (webmaster@dhtmlcentral.com)
      
      DHTML coolMenus - Get it at coolmenus.dhtmlcentral.com
      Version 4.0_beta
      This script can be used freely as long as all copyright messages are
      intact.
      
      Extra info - Coolmenus reference/help - Extra links to help files **** 
      CSS help: http://coolmenus.dhtmlcentral.com/projects/coolmenus/reference.asp?m=37
      General: http://coolmenus.dhtmlcentral.com/reference.asp?m=35
      Menu properties: http://coolmenus.dhtmlcentral.com/properties.asp?m=47
      Level properties: http://coolmenus.dhtmlcentral.com/properties.asp?m=48
      Background bar properties: http://coolmenus.dhtmlcentral.com/properties.asp?m=49
      Item properties: http://coolmenus.dhtmlcentral.com/properties.asp?m=50
      
******************************************************************************/
      </script>
<script>

/*** 
This is the menu creation code - place it right after you body tag
Feel free to add this to a stand-alone js file and link it to your page.
**/

//Menu object creation
oCMenu=new makeCM("oCMenu") //Making the menu object. Argument: menuname

oCMenu.frames = 0

//Menu properties   
oCMenu.pxBetween=4

oCMenu.fromLeft=0

oCMenu.fromTop=168

oCMenu.rows=1
oCMenu.menuPlacement="center"
                                                             
oCMenu.offlineRoot=""
oCMenu.onlineRoot=""
oCMenu.resizeCheck=1 
oCMenu.wait=100 
oCMenu.fillImg="cm_fill.gif"
oCMenu.zIndex=100

//Background bar properties
oCMenu.useBar=0
oCMenu.barWidth="945"
oCMenu.barHeight="menu" 
oCMenu.barClass="clBar"
oCMenu.barX=0 
oCMenu.barY=0
oCMenu.barBorderX=0
oCMenu.barBorderY=0
oCMenu.barBorderClass=""

//Level properties - ALL properties have to be spesified in level 0

oCMenu.level[0]=new cm_makeLevel() //Add this for each new level
oCMenu.level[0].width=1
oCMenu.level[0].height=1
oCMenu.level[0].regClass="clLevel0"
oCMenu.level[0].overClass="clLevel0over"
oCMenu.level[0].borderX=0
oCMenu.level[0].borderY=0
oCMenu.level[0].borderClass="clLevel0border"
oCMenu.level[0].offsetX=0
oCMenu.level[0].offsetY=0
oCMenu.level[0].rows=0
oCMenu.level[0].arrow=0
oCMenu.level[0].arrowWidth=0
oCMenu.level[0].arrowHeight=0
oCMenu.level[0].align="bottom"
            

oCMenu.level[1]=new cm_makeLevel() //Add this for each new level
oCMenu.level[1].width=130
oCMenu.level[1].height=20
oCMenu.level[1].regClass="clLevel0"
oCMenu.level[1].overClass="clLevel0over"
oCMenu.level[1].borderX=1
oCMenu.level[1].borderY=1
oCMenu.level[1].borderClass="clLevel0border"
oCMenu.level[1].offsetX=0
oCMenu.level[1].offsetY=0
oCMenu.level[1].rows=0
oCMenu.level[1].arrow=0
oCMenu.level[1].arrowWidth=0
oCMenu.level[1].arrowHeight=0
oCMenu.level[1].align="middle"
            

oCMenu.level[2]=new cm_makeLevel() //Add this for each new level
oCMenu.level[2].width=110
oCMenu.level[2].height=25
oCMenu.level[2].regClass="clLevel0"
oCMenu.level[2].overClass="clLevel0over"
oCMenu.level[2].borderX=1
oCMenu.level[2].borderY=1
oCMenu.level[2].borderClass="clLevel0border"
oCMenu.level[2].offsetX=0
oCMenu.level[2].offsetY=0
oCMenu.level[2].rows=0
oCMenu.level[2].arrow=0
oCMenu.level[2].arrowWidth=0
oCMenu.level[2].arrowHeight=0
oCMenu.level[2].align="bottom"
            

oCMenu.level[3]=new cm_makeLevel() //Add this for each new level
oCMenu.level[3].width=110
oCMenu.level[3].height=25
oCMenu.level[3].regClass="clLevel0"
oCMenu.level[3].overClass="clLevel0over"
oCMenu.level[3].borderX=1
oCMenu.level[3].borderY=1
oCMenu.level[3].borderClass="clLevel0border"
oCMenu.level[3].offsetX=0
oCMenu.level[3].offsetY=0
oCMenu.level[3].rows=0
oCMenu.level[3].arrow=0
oCMenu.level[3].arrowWidth=0
oCMenu.level[3].arrowHeight=0
oCMenu.level[3].align="bottom"
            

/******************************************
Menu item creation:
myCoolMenu.makeMenu(name, parent_name, text, link, target, width, height, regImage, overImage, regClass, overClass , align, rows, nolink, onclick, onmouseover, onmouseout) 
*************************************/

oCMenu.makeMenu('top0','','','http://www.bloemfonteincelticfc.co.za/default.aspx', '', 60, 24, '../pics/nav/home/nav_off.gif','../pics/nav/home/nav_on.gif')
 oCMenu.makeMenu('top1','','','http://www.bloemfonteincelticfc.co.za/archive.aspx', '', 60, 24, '../pics/nav/news/nav_off.gif','../pics/nav/news/nav_on.gif')
 oCMenu.makeMenu('top2','','','pics/nav/', '', 60, 24, '../pics/nav/psl/nav_off.gif','../pics/nav/psl/nav_on.gif')
 oCMenu.makeMenu('sub2_0','top2','Player Profiles','http://www.bloemfonteincelticfc.co.za/profiles.aspx','') 
  oCMenu.makeMenu('sub2_1','top2','Fixtures','http://www.bloemfonteincelticfc.co.za/fixtures.aspx','') 
 oCMenu.makeMenu('sub2_2','top2','Results','http://www.bloemfonteincelticfc.co.za/results.aspx','') 
 oCMenu.makeMenu('sub2_3','top2','Log','http://www.bloemfonteincelticfc.co.za/logs.aspx','') 
 //oCMenu.makeMenu('top3','','','content.aspx', '', 130, 24, 'pics/nav/supporters_club/nav_off.gif','pics/nav/supporters_club/nav_on.gif')
 //oCMenu.makeMenu('top4','','','pics/nav/', '', 96, 24, 'pics/nav/chat_forum/nav_off.gif','pics/nav/chat_forum/nav_on.gif')
 oCMenu.makeMenu('top3','','','', '', 73, 24, '../pics/nav_on_1.gif','../pics/nav_on_1.gif')
  oCMenu.makeMenu('sub3_0','top3','Coaches`s Column','http://www.bloemfonteincelticfc.co.za/columns.aspx?pId=225','') 
 oCMenu.makeMenu('sub3_1','top3','Management Column','http://www.bloemfonteincelticfc.co.za/columns.aspx?pId=295','') 
 oCMenu.makeMenu('sub3_2','top3','Players Column','http://www.bloemfonteincelticfc.co.za/columns.aspx?pId=291','') 
 oCMenu.makeMenu('top4','','','http://www.reebokstore.co.za/index.php?page=shop.browse&category_id=24&option=com_virtuemart&Itemid=39&vmcchk=1&Itemid=39', '', 96, 24, '../pics/nav/online_shop/nav_off.gif','../pics/nav/online_shop/nav_on.gif')
 //oCMenu.makeMenu('top5','','','http://www.reebokstore.co.za/index.php?page=shop.browse&category_id=24&option=com_virtuemart&Itemid=39&vmcchk=1&Itemid=39', '', 96, 24, 'pics/nav/online_shop/nav_off.gif','pics/nav/online_shop/nav_on.gif')
 oCMenu.makeMenu('top5','','','content.aspx?Id=18958', '', 73, 24, '../pics/nav/club_info/nav_off.gif','../pics/nav/club_info/nav_on.gif')
 oCMenu.makeMenu('sub5_0','top5','Sponsors','http://www.bloemfonteincelticfc.co.za/content.aspx?Id=18955','') 
oCMenu.makeMenu('sub5_1','top5','Staff','http://www.bloemfonteincelticfc.co.za/content.aspx?Id=18956','') 
oCMenu.makeMenu('sub5_2','top5','Social Responsibility','http://www.bloemfonteincelticfc.co.za/content.aspx?Id=18957','') 
oCMenu.makeMenu('top6','','','http://www.bloemfonteincelticfc.co.za/content.aspx?Id=19040', '', 73, 24, '../pics/nav/academy/nav_off.gif','../pics/nav/academy/nav_on.gif')
oCMenu.makeMenu('top7','','','http://www.bloemfonteincelticfc.co.za/content.aspx?Id=19139', '', 86, 24, '../pics/nav/community/nav_off.gif','../pics/nav/community/nav_on.gif')
oCMenu.makeMenu('top8','','','http://www.bloemfonteincelticfc.co.za/content.aspx?Id=19057', '', 73, 24, '../pics/nav/tickets/nav_off.gif','../pics/nav/tickets/nav_on.gif')
oCMenu.makeMenu('top9','','','http://www.bloemfonteincelticfc.co.za/content.aspx?Id=19349', '', 96 , 24, '../pics/kids_corner.gif','../pics/kids_corner.gif')
//Leave this line - it constructs the menu
oCMenu.construct()		

    </script>
</body>
</html>
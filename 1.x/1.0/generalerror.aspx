﻿<%@ Page Language="VB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Error</title>
    <link rel="SHORTCUT ICON" href="favicon.ico">
    <style type="text/css">
        body { margin: 0; padding: 0; }
        body, td, input, select, button, textarea { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; }
        a { font-size: 11px; color: #c21717; text-decoration: none; }
        a:hover { font-size: 11px; color: #c21717; text-decoration: underline; }
    </style>
</head>
<body style="margin: 10px;">
<table width="800" cellspacing="1" cellpadding="0" border="0" align="center" bgcolor="#011031">
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="background-color: #FFFFFF; padding: 15px;">
         <span style="font-size: 14pt; font-weight: bold; color: #BCBCBC">Oops, Page is broken</span>
         <br /><br />
         Due to a technical error on our side this page is currently unavailable we apologize for the inconvenience. We will endeavor to fix the problem as soon as possible however if you wish to contact us please <a href="contact.aspx">click here</a> and fill in the form.
         <br /><br />
         Click <a href="default.aspx">here</a> to go the home page.
        </td>
    </tr>
</table>
</body>
</html>

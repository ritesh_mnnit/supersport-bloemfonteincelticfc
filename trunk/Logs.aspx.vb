
Partial Class Logs
    Inherits System.Web.UI.Page
    Private SiteNumber As Integer = System.Configuration.ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Sub Page_Load(ByVal sedner As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Logs()
    End Sub
    Private Sub Logs()
        ltlLogs.Text &= "<table width='570px' border='1' cellspacing='0' cellpadding='0' bordercolor='#ffffff'>"


        SqlQuery = New SqlCommand("Execute Soccer.dbo.pa_returnLogs 794,0", DatabaseConn)

        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        Dim Counter As Integer = 1
        Dim GroupName As String = ""
        Dim League As String = ""
        While RsRec.Read
            If Counter = 1 Then
                GroupName = RsRec("GroupName")
                League = RsRec("League")
            Else
                Exit While
            End If
            Counter = Counter + 1
        End While
        RsRec.Close()

        If GroupName = "" Then
            ltlLogs.Text &= "<tr style='font-weight:bold;'>"
            ltlLogs.Text &= "<td class='fix' align='center' height='28' >Pos</td>"
            ltlLogs.Text &= "<td class='fix' align='left' height='28'>Team</td>"
            ltlLogs.Text &= "<td   class='fix' align='center' width='28' height='28'>P</td>"
            ltlLogs.Text &= "<td class='fix' align='center' width='28' height='28'>W</td>"
            ltlLogs.Text &= "<td class='fix' align='center' width='28' height='28'>D</td>"
            ltlLogs.Text &= "<td class='fix' align='center' width='28' height='28'>L</td>"
            ltlLogs.Text &= "<td class='fix' align='center' width='28' height='28'>GF</td>"
            ltlLogs.Text &= "<td class='fix' align='center' width='28' height='28'>GA</td>"
            ltlLogs.Text &= "<td class='fix' align='center' width='28' height='28'>GD</td>"
            ltlLogs.Text &= "<td class='fix' align='center' width='28' height='28'>Pts</td>"
            ltlLogs.Text &= "</tr>"
        End If

        Dim CurrentGroup As String = ""
        Dim GroupCount As Integer = 1
        Counter = 1

        SqlQuery = New SqlCommand("Execute Soccer.dbo.pa_returnLogs 794,0", DatabaseConn)

        RsRec = SqlQuery.ExecuteReader
        While RsRec.Read
            If RsRec("GroupName") <> "" And RsRec("GroupName") <> CurrentGroup Then
                Counter = 1
                If GroupCount > 1 Then
                    ltlLogs.Text &= "</table><br><br>"
                    ltlLogs.Text &= "<table width='590px' border='1' cellspacing='0' cellpadding='0' bordercolor='#ffffff'>"
                End If
                ltlLogs.Text &= "<tr class='TabbedPanelsTab1'>"
                ltlLogs.Text &= "<td colspan='10' width='28' height='28'>" & RsRec("GroupName") & "</td>"
                ltlLogs.Text &= "</tr>"
                ltlLogs.Text &= "<tr style='font-weight:bold;'>"
                ltlLogs.Text &= "<td width='28' height='28' align='center' class='fix'>Pos</td>"
                ltlLogs.Text &= "<td class='fix'>Team</td>"
                ltlLogs.Text &= "<td align='center' width='28' height='28' class='fix'>P</td>"
                ltlLogs.Text &= "<td align='center' width='28' height='28' class='fix'>W</td>"
                ltlLogs.Text &= "<td align='center' width='28' height='28' class='fix'>D</td>"
                ltlLogs.Text &= "<td align='center' width='28' height='28' class='fix'>L</td>"
                ltlLogs.Text &= "<td align='center' width='28' height='28' class='fix'>GF</td>"
                ltlLogs.Text &= "<td align='center' width='28' height='28' class='fix'>GA</td>"
                ltlLogs.Text &= "<td align='center' width='28' height='28' class='fix'>GD</td>"
                ltlLogs.Text &= "<td align='center' width='28' height='28' class='fix'>Pts</td>"
                ltlLogs.Text &= "</tr>"
                CurrentGroup = RsRec("GroupName")
                GroupCount = GroupCount + 1
            End If
            If RsRec("Team") = "Bloem Celtic" Then
                ltlLogs.Text &= "<tr style='font-weight:bold;'>"
                ltlLogs.Text &= "<td class='fix' style='height:20px;text-align:center;'>" & Counter & "</td>"
                ltlLogs.Text &= "<td class='fix' style='height:20px;'>" & RsRec("Team") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("Played") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("Won") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("Drew") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("Lost") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("GoalsFor") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("GoalsAgainst") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("GoalsFor") - RsRec("GoalsAgainst") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("LogPoints") & "</td>"
                ltlLogs.Text &= "</tr>"
            Else
                ltlLogs.Text &= "<tr>"
                ltlLogs.Text &= "<td width='20' align='center' class='fix'>" & Counter & "</td>"
                ltlLogs.Text &= "<td class='fix'>" & RsRec("Team") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("Played") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("Won") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("Drew") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("Lost") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("GoalsFor") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("GoalsAgainst") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("GoalsFor") - RsRec("GoalsAgainst") & "</td>"
                ltlLogs.Text &= "<td align='center' class='fix' style='height:20px;'>" & RsRec("LogPoints") & "</td>"
                ltlLogs.Text &= "</tr>"
            End If
            Counter = Counter + 1
        End While
        RsRec.Close()
        ltlLogs.Text &= "</table>"
        'ltlLogs.Text &= "<tr >"
        ltlLogs.Text &= "<div id='more'><a href='javascript:history.go(-1);' style='font-weight:bold'> Back</a></div>"
        'ltlLogs.Text &= "</tr>"


    End Sub
    Private Sub Page_UnLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class

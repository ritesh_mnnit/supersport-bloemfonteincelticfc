<%@ Page Language="VB" AutoEventWireup="false" CodeFile="photogalleryimage.aspx.vb" Inherits="photogalleryimage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" id="head">
<title></title>
<style type="text/css" id="main" runat="server">  @import url('styles/print.css');</style>
</head>
<body>
<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr>
<td align='center' valign='middle' style='Padding-left: 20px; Padding-right: 20px;Padding-Top: 10px; Padding-bottom: 10px;'><table cellspacing='0' cellpadding='0' border='0' align='center'>
<tr>
<td bgcolor='#c0c0c0' colspan='3' style='Border: 1px solid #EBF6EA; Padding-left: 5px; Padding-top: 2px; Padding-bottom: 2px;'><font color='#FFFFFF'><b>Photo Galleries:</b></font></td>
</tr>
<tr>
<td bgcolor='#F2F2F2' style='Padding-left: 20px; Padding-right: 20px; Padding-top: 20px; Padding-bottom: 20px; border: 1px solid #EBF6EA;'><table cellspacing='4' cellpadding='0' border='0' align='center'>
<tr>
<td><asp:Literal id="ltlImage" runat="Server" EnableViewState="False" /></td>
</tr>
<tr>
<td height='10'></td>
</tr>
<tr>
<td style='Font-size: 9pt; Font-Family: Arial;'><asp:Literal id="ltlBlurb" runat="Server" EnableViewState="False" /></td>
</tr>
</table></td></tr>
</table><a href='Javascript: self.close();' style="text-decoration: none;"><font color='#000000'><b>close</b></font></a></td>
</tr>
</table>
</body>
</html>


Partial Class archive
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Archive()
    End Sub
    Sub Archive()
        SqlQuery = New SqlCommand("SELECT TOP 30 a.Id,a.ArticleDate As Created ,a.Headline as Headline, a.body As body,a.Blurb as Blurb, a.SmallImage as Image FROM articlebreakdown a INNER JOIN SuperSportZone.dbo.zonearticleassociations b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.zoneassociations c ON c.Id = b.Association INNER JOIN SuperSportZone.dbo.ZoneArticles d ON d.Id = a.Id WHERE (c.Name = 'Bloem Celtic') AND (a.site = 62) AND (a.Active = 1) ORDER BY a.ArticleDate DESC", DatabaseConn)
        Dim tmpCategory As Integer = SqlQuery.ExecuteScalar
        dl_stories.DataSource = SqlQuery.ExecuteReader
        dl_stories.DataBind()
        dl_stories.DataSource.Close()
    End Sub
    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class

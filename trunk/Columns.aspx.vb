
Partial Class Columns
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer
    Dim pId As String
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Column(pId)
    End Sub
    Sub Column(ByVal pId As String)
        If Not Request.QueryString("pId") Is Nothing Then
            ID = Request.QueryString("pId")
        End If
        'If Id <> "" Then
        'SqlQuery = New SqlCommand("Select Top 1 a.*, b.Id As Author, b.Name As PersonalityName, b.LargeImage From SuperSportZone.dbo.ZonePersonalityArticles a INNER JOIN SuperSportZone.dbo.ZonePersonalities b ON b.Id = a.Personality Where (a.Id = " & Id & ")", DatabaseConn)
        'Else
        SqlQuery = New SqlCommand("Select  Top 1 a.*, b.Id As Author, b.Name As PersonalityName, b.LargeImage From SuperSportZone.dbo.ZonePersonalityArticles a INNER JOIN SuperSportZone.dbo.ZonePersonalities b ON b.Id = a.Personality Where (a.personality = " & Request.QueryString("pId") & ")", DatabaseConn)


        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        Dim AuthorId As Integer = 0
        While RsRec.Read
            Dim tmpDate As DateTime = RsRec("Created")
            AuthorId = RsRec("Author")
            ltlcolumnheader.Text = RsRec("Headline")
            ltlcolumndate.Text = "by <b>" & RsRec("PersonalityName") & "</b>"
            If RsRec("LargeImage") = "" Then
                ltlcolumnimage.Text = ""
            Else
                ltlcolumnimage.Text = "<img src='http://images.supersport.co.za/" & RsRec("LargeImage") & "' alt='' border='0' align='Top'/>"
            End If
            ltlcolumnbody.Text = RsRec("Body")
        End While
        ' End If
        RsRec.Close()


        ltlheadline2.Text = "<span style='font-weight:bold;'>MORE COLUMNS</span>"
        'If ID <> "" Then
        'SqlQuery = New SqlCommand("Select a.*, b.Id As Author, b.Name As PersonalityName, b.LargeImage From SuperSportZone.dbo.ZonePersonalityArticles a INNER JOIN SuperSportZone.dbo.ZonePersonalities b ON b.Id = a.Personality Where (a.Id = " & ID & ")", DatabaseConn)
        'Else
        SqlQuery = New SqlCommand("Select a.*, b.Id As Author, b.Name As PersonalityName, b.LargeImage From SuperSportZone.dbo.ZonePersonalityArticles a INNER JOIN SuperSportZone.dbo.ZonePersonalities b ON b.Id = a.Personality Where (a.personality = " & Request.QueryString("pId") & ")", DatabaseConn)
        'End If
        'SqlQuery = New SqlCommand("Select a.*, b.Id As Author, b.Name As PersonalityName, b.Image From SuperSportZone.dbo.ZonePersonalityArticles a INNER JOIN SuperSportZone.dbo.ZonePersonalities b ON b.Id = a.Personality Where (b.Id = 295)", DatabaseConn)
        Dim RsRec_1 As SqlDataReader = SqlQuery.ExecuteReader
        Dim Count As Integer
        Count = 0
        While RsRec_1.Read


            ltlheadline1.Text = "<a class='more' href='columns.aspx?id=" & RsRec_1("Id") & "'>" & RsRec_1("Headline") & "</a><br>"
            Count = Count + 1

        End While
        RsRec_1.Close()
    End Sub
    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class

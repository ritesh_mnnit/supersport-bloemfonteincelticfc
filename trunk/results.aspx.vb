
Partial Class results
    Inherits System.Web.UI.Page
    Private SiteNumber As Integer = System.Configuration.ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private DatabaseConn1 As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)

    Private SqlQuery As SqlCommand
    Private Sub Page_Load(ByVal sedner As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        DatabaseConn1.Open()
        results()
    End Sub

    Private Sub results()

        ltlresults.Text &= "<table width='570px' cellspacing='1' cellpadding='2' border='0'>"

        SqlQuery = New SqlCommand("Select * From Soccer.dbo.bloemceltic_Result Order By MatchDateTime Desc", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        If RsRec.HasRows Then
            Dim Count As Integer = 1
            Dim Overall As Integer = 1
            While RsRec.Read
                Dim MatchDateTime As DateTime = RsRec("MatchDateTime")
                Dim LeagueName As String = RsRec("League")
                LeagueName = LeagueName.Replace("2007 ", "")
                ltlResults.Text &= "<tr>"
                ltlresults.Text &= "<td class=""fix"" >" & MatchDateTime.ToString("dd MMM") & "</td>"
                ltlresults.Text &= "<td class=""fix"">" & RsRec("HomeTeam") & "</td>"
                If RsRec("HomeTeamPenalties") > 0 Or RsRec("AwayTeamPenalties") > 0 Then
                    ltlresults.Text &= "<td class=""fix"">" & RsRec("HomeTeamScore") & "(" & RsRec("HomeTeamPenalties") & ") - " & RsRec("AwayTeamScore") & "(" & RsRec("AwayTeamPenalties") & ")</td>"
                Else
                    ltlresults.Text &= "<td class=""fix"">" & RsRec("HomeTeamScore") & " - " & RsRec("AwayTeamScore") & "</td>"
                End If
                ltlresults.Text &= "<td class=""fix"">" & RsRec("AwayTeam") & "</td>"
                ltlresults.Text &= "<td class=""fix"">" & RsRec("Venue") & "</td>"
                ltlresults.Text &= "<td class=""fix"" style='font-weight:bold;'>" & LeagueName & "</td>"
                ltlResults.Text &= "</tr>"

                Dim HomeTeamGoals As New ArrayList
                Dim AwayTeamGoals As New ArrayList

                If RsRec("League") = "PSL" Then
                    SqlQuery = New SqlCommand("Select b.MatchId, b.Minutes, b.Player1FirstName+ ' ' + b.Player1LastName As Name, b.EventId From Soccer.dbo.pa_Events b Where (b.MatchId = " & RsRec("Id") & ") And (b.HomeTeam = 1) and (b.EventId = 2 Or b.EventId = 3 Or b.EventId = 5) Order By b.Minutes", DatabaseConn1)
                    Dim RsRec1 As SqlDataReader = SqlQuery.ExecuteReader
                    While RsRec1.Read
                        If RsRec1("EventId") = 5 Then
                            HomeTeamGoals.Add(RsRec1("Name") & " (pen " & RsRec1("Minutes") & ")")
                        ElseIf RsRec1("EventId") = 3 Then
                            HomeTeamGoals.Add(RsRec1("Name") & " (og " & RsRec1("Minutes") & ")")
                        Else
                            HomeTeamGoals.Add(RsRec1("Name") & " (" & RsRec1("Minutes") & ")")
                        End If
                    End While
                    RsRec1.Close()

                    SqlQuery = New SqlCommand("Select b.MatchId, b.Minutes, b.Player1FirstName+ ' ' + b.Player1LastName As Name, b.EventId From Soccer.dbo.pa_Events b Where (b.MatchId = " & RsRec("Id") & ") And (b.HomeTeam = 0) and (b.EventId = 2 Or b.EventId = 3 Or b.EventId = 5) Order By b.Minutes", DatabaseConn1)
                    RsRec1 = SqlQuery.ExecuteReader
                    While RsRec1.Read
                        If RsRec1("EventId") = 5 Then
                            AwayTeamGoals.Add(RsRec1("Name") & " (pen " & RsRec1("Minutes") & ")")
                        ElseIf RsRec1("EventId") = 3 Then
                            AwayTeamGoals.Add(RsRec1("Name") & " (og " & RsRec1("Minutes") & ")")
                        Else
                            AwayTeamGoals.Add(RsRec1("Name") & " (" & RsRec1("Minutes") & ")")
                        End If
                    End While
                    RsRec1.Close()
                Else
                    SqlQuery = New SqlCommand("Select a.Player1Id, b.Fullname + ' ' + b.Surname As Name, a.EventId, a.MatchTime From Soccer.dbo.Commentary a INNER JOIN Soccer.dbo.Persons b ON b.Id = a.Player1Id INNER JOIN Soccer.dbo.Matches c ON c.Id = a.MatchId Where ((a.TeamId = c.TeamAId) AND (a.MatchId = " & RsRec("Id") & ") AND ((a.Eventid = 2) OR (a.EventId = 4))) OR ((a.TeamId = c.TeamBId) AND (a.MatchId = " & RsRec("Id") & ") AND (a.EventId = 3)) ORDER BY MatchTime", DatabaseConn1)
                    Dim RsRec1 As SqlDataReader = SqlQuery.ExecuteReader
                    While RsRec1.Read
                        If RsRec1("EventId") = 4 Then
                            HomeTeamGoals.Add(RsRec1("Name") & " (pen " & RsRec1("MatchTime") & ")")
                        ElseIf RsRec1("EventId") = 3 Then
                            HomeTeamGoals.Add(RsRec1("Name") & " (og " & RsRec1("MatchTime") & ")")
                        Else
                            HomeTeamGoals.Add(RsRec1("Name") & " (" & RsRec1("MatchTime") & ")")
                        End If
                    End While
                    RsRec1.Close()

                    SqlQuery = New SqlCommand("Select a.Player1Id, b.Fullname + ' ' + b.Surname As Name, a.EventId, a.MatchTime From Soccer.dbo.Commentary a INNER JOIN Soccer.dbo.Persons b ON b.Id = a.Player1Id INNER JOIN Soccer.dbo.Matches c ON c.Id = a.MatchId Where ((a.TeamId = c.TeamBId) AND (a.MatchId = " & RsRec("Id") & ") AND ((a.Eventid = 2) OR (a.EventId = 4))) OR ((a.TeamId = c.TeamAId) AND (a.MatchId = " & RsRec("Id") & ") AND (a.EventId = 3)) ORDER BY MatchTime", DatabaseConn1)
                    RsRec1 = SqlQuery.ExecuteReader
                    While RsRec1.Read
                        If RsRec1("EventId") = 4 Then
                            AwayTeamGoals.Add(RsRec1("Name") & " (pen " & RsRec1("MatchTime") & ")")
                        ElseIf RsRec1("EventId") = 3 Then
                            AwayTeamGoals.Add(RsRec1("Name") & " (og " & RsRec1("MatchTime") & ")")
                        Else
                            AwayTeamGoals.Add(RsRec1("Name") & " (" & RsRec1("MatchTime") & ")")
                        End If
                    End While
                    RsRec1.Close()
                End If

                If HomeTeamGoals.Count > 0 Or AwayTeamGoals.Count > 0 Then
                    Dim IEnum As IEnumerator
                    Dim Goals As Integer
                    ltlResults.Text &= "<tr>"
                    ltlresults.Text &= "<td class=""fix""></td>"
                    ltlresults.Text &= "<td class=""fix"">"
                    IEnum = HomeTeamGoals.GetEnumerator
                    Goals = 0
                    While IEnum.MoveNext
                        If Goals > 0 Then
                            ltlResults.Text &= "<br />"
                        End If
                        ltlResults.Text &= IEnum.Current
                        Goals = Goals + 1
                    End While
                    ltlResults.Text &= "</td>"
                    ltlresults.Text &= "<td class=""fix""></td>"
                    ltlresults.Text &= "<td class=""fix"">"
                    IEnum = AwayTeamGoals.GetEnumerator
                    Goals = 0
                    While IEnum.MoveNext
                        If Goals > 0 Then
                            ltlResults.Text &= "<br />"
                        End If
                        ltlResults.Text &= IEnum.Current
                        Goals = Goals + 1
                    End While
                    ltlResults.Text &= "</td>"
                    ltlresults.Text &= "<td class=""fix""></td>"
                    ltlresults.Text &= "<td class=""fix""></td>"
                    ltlResults.Text &= "</tr>"
                End If

                Count = Count + 1
                Overall = Overall + 1
            End While
            RsRec.Close()
        Else
            ltlResults.Text &= "<tr><td>There have been are no results</td></tr>"
        End If
        DatabaseConn1.Close()






        ltlresults.Text &= "<div id='body2'>"
        ltlresults.Text &= "<tr><td><div id=""more""><a href=""javascript:history.go(-1)"" style=""font-weight:bold;"">Back</a></div></td></tr>"
        ltlresults.Text &= "</div>"
        ltlResults.Text &= "</table>"


    End Sub

    Private Sub Page_Unload(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
        DatabaseConn1.Close()
    End Sub
End Class

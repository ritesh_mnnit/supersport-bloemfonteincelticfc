
Partial Class profiles
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = 4665
    Dim Counter As Integer
    Dim Count As Integer
    Public TopImage As String = ""
    Public BottomImage As String = ""


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim Id As String = ""
        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If

        If Not Request.QueryString("cl") Is Nothing Then
            TopImage = ""
            BottomImage = ""
        End If

        DatabaseConn.Open()

        If Id <> "" Then
            Information(Id)
            pnl1.Visible = False
            pnl2.Visible = True
        Else
            FrontPage()
            pnl1.Visible = True
            pnl2.Visible = False
        End If

    End Sub

    Sub Page_UnLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub

    Private Sub FrontPage()

        Dim RsRec As SqlDataReader
        Dim tmpCount As Integer
        Dim I As Integer

        SqlQuery = New SqlCommand("Select Count(*) As Total From Soccer.dbo.Squads a INNER JOIN Soccer.dbo.Persons b ON b.Id = a.PersonId Where (a.TeamId = " & TeamId & ") And (b.PlayingPosition = 2) And (b.Active = 1)", DatabaseConn)
        Dim Total As Integer = SqlQuery.ExecuteScalar
        Dim Percentage As String = "25%"
        If Total = 1 Then
            Percentage = "100%"
        ElseIf Total = 2 Then
            Percentage = "50%"
        ElseIf Total = 3 Then
            Percentage = "33%"
        End If

        SqlQuery = New SqlCommand("Select b.Id, b.RoleId, b.FullName, b.Surname, b.PlayingPosition, b.SmallImage As Image From Soccer.dbo.Squads a INNER JOIN Soccer.dbo.Persons b ON b.Id = a.PersonId Where (a.TeamId = " & TeamId & ") And (b.PlayingPosition = 2) And (b.Active = 1) Order By Surname, FullName", DatabaseConn)
        RsRec = SqlQuery.ExecuteReader
        Dim GoalKeepers As Integer = 0
        While RsRec.Read
            If GoalKeepers = 0 Then
                ltlgoalkeepers.Text = "<tr>"
            Else
                If GoalKeepers Mod 4 = 0 Then
                    ltlgoalkeepers.Text &= "</tr><tr>"
                End If
            End If
            ltlgoalkeepers.Text &= "<td width=""" & Percentage & """ align=""center"" valign=""top"">"
            ltlgoalkeepers.Text &= "<table width=""100%"" cellspacing=""0"" cellpadding=""0"" border=""0"">"
            ltlgoalkeepers.Text &= "<tr>"
            ltlgoalkeepers.Text &= "<td align=""center"" style=""padding-bottom: 2px;"">"
            If RsRec("Image") = "" Then
                ltlgoalkeepers.Text &= "<a href=""profiles.aspx?id=" & RsRec("Id") & """><img src=""pics/bloemlogo.jpg"" alt="""" border="""" /></a>"
            Else
                ltlgoalkeepers.Text &= "<a href=""profiles.aspx?id=" & RsRec("Id") & """><img src=""http://images.supersport.co.za/" & RsRec("Image") & """ alt="""" border="""" /></a>"
            End If
            ltlgoalkeepers.Text &= "</td>"
            ltlgoalkeepers.Text &= "</tr>"
            ltlgoalkeepers.Text &= "<tr>"
            ltlgoalkeepers.Text &= "<td align=""center"">"
            ltlgoalkeepers.Text &= "<a style=""color: #000000;"" href=""profiles.aspx?id=" & RsRec("Id") & """>" & RsRec("FullName") & " " & RsRec("Surname") & "</a>"
            ltlgoalkeepers.Text &= "</td>"
            ltlgoalkeepers.Text &= "</tr>"
            ltlgoalkeepers.Text &= "</table>"
            ltlgoalkeepers.Text &= "</td>"
            GoalKeepers = GoalKeepers + 1
        End While
        If GoalKeepers > 0 And Total > 3 Then
            If GoalKeepers Mod 4 > 0 Then
                tmpCount = 4 - (GoalKeepers Mod 4)
                For I = 1 To tmpCount
                    ltlgoalkeepers.Text &= "<td width=""" & Percentage & """ align=""center"" valign=""top""></td>"
                Next
            End If
            ltlgoalkeepers.Text &= "</tr>"
        End If
        RsRec.Close()

        Percentage = "25%"

        SqlQuery = New SqlCommand("Select b.Id, b.RoleId, b.FullName, b.Surname, b.PlayingPosition, b.SmallImage As Image From Soccer.dbo.Squads a INNER JOIN Soccer.dbo.Persons b ON b.Id = a.PersonId Where (a.TeamId = " & TeamId & ") And (b.PlayingPosition = 3) And (b.Active = 1) Order By Surname, FullName", DatabaseConn)
        RsRec = SqlQuery.ExecuteReader
        Dim Defenders As Integer = 0
        While RsRec.Read
            If Defenders = 0 Then
                ltldefenders.Text = "<tr>"
            Else
                If Defenders Mod 4 = 0 Then
                    ltldefenders.Text &= "</tr><tr>"
                End If
            End If
            ltldefenders.Text &= "<td width=""" & Percentage & """ align=""center"" valign=""top"">"
            ltldefenders.Text &= "<table width=""100%"" cellspacing=""0"" cellpadding=""0"" border=""0"">"
            ltldefenders.Text &= "<tr>"
            ltldefenders.Text &= "<td align=""center"" style=""padding-bottom: 2px;"">"
            If RsRec("Image") = "" Then
                ltldefenders.Text &= "<a href=""profiles.aspx?id=" & RsRec("Id") & """><img src=""pics/bloemlogo.jpg"" alt="""" border="""" /></a>"
            Else
                ltldefenders.Text &= "<a href=""profiles.aspx?id=" & RsRec("Id") & """><img src=""http://images.supersport.co.za/" & RsRec("Image") & """ alt="""" border="""" /></a>"
            End If
            ltldefenders.Text &= "</td>"
            ltldefenders.Text &= "</tr>"
            ltldefenders.Text &= "<tr>"
            ltldefenders.Text &= "<td align=""center"">"
            ltldefenders.Text &= "<a style=""color: #000000;"" href=""profiles.aspx?id=" & RsRec("Id") & """>" & RsRec("FullName") & " " & RsRec("Surname") & "</a>"
            ltldefenders.Text &= "</td>"
            ltldefenders.Text &= "</tr>"
            ltldefenders.Text &= "</table>"
            ltldefenders.Text &= "</td>"
            Defenders = Defenders + 1
        End While
        If Defenders > 0 Then
            If Defenders Mod 4 > 0 Then
                tmpCount = 4 - (Defenders Mod 4)
                For I = 1 To tmpCount
                    ltldefenders.Text &= "<td width=""" & Percentage & """ align=""center"" valign=""top""></td>"
                Next
            End If
            ltldefenders.Text &= "</tr>"
        End If
        RsRec.Close()

        SqlQuery = New SqlCommand("Select b.Id, b.RoleId, b.FullName, b.Surname, b.PlayingPosition, b.SmallImage As Image From Soccer.dbo.Squads a INNER JOIN Soccer.dbo.Persons b ON b.Id = a.PersonId Where (a.TeamId = " & TeamId & ") And (b.PlayingPosition = 4) And (b.Active = 1) Order By Surname, FullName", DatabaseConn)
        RsRec = SqlQuery.ExecuteReader
        Dim Midfielders As Integer = 0
        While RsRec.Read
            If Midfielders = 0 Then
                ltlmidfielders.Text = "<tr>"
            Else
                If Midfielders Mod 4 = 0 Then
                    ltlmidfielders.Text &= "</tr><tr>"
                End If
            End If
            ltlmidfielders.Text &= "<td width=""" & Percentage & """ align=""center"" valign=""top"">"
            ltlmidfielders.Text &= "<table width=""100%"" cellspacing=""0"" cellpadding=""0"" border=""0"">"
            ltlmidfielders.Text &= "<tr>"
            ltlmidfielders.Text &= "<td align=""center"" style=""padding-bottom: 2px;"">"
            If RsRec("Image") = "" Then
                ltlmidfielders.Text &= "<a href=""profiles.aspx?id=" & RsRec("Id") & """><img src=""pics/bloemlogo.jpg"" alt="""" border="""" /></a>"
            Else
                ltlmidfielders.Text &= "<a href=""profiles.aspx?id=" & RsRec("Id") & """><img src=""http://images.supersport.co.za/" & RsRec("Image") & """ alt="""" border="""" /></a>"
            End If
            ltlmidfielders.Text &= "</td>"
            ltlmidfielders.Text &= "</tr>"
            ltlmidfielders.Text &= "<tr>"
            ltlmidfielders.Text &= "<td align=""center"">"
            ltlmidfielders.Text &= "<a style=""color: #000000;"" href=""profiles.aspx?id=" & RsRec("Id") & """>" & RsRec("FullName") & " " & RsRec("Surname") & "</a>"
            ltlmidfielders.Text &= "</td>"
            ltlmidfielders.Text &= "</tr>"
            ltlmidfielders.Text &= "</table>"
            ltlmidfielders.Text &= "</td>"
            Midfielders = Midfielders + 1
        End While
        If Midfielders > 0 Then
            If Midfielders Mod 4 > 0 Then
                tmpCount = 4 - (Midfielders Mod 4)
                For I = 1 To tmpCount
                    ltlmidfielders.Text &= "<td width=""" & Percentage & """ align=""center"" valign=""top""></td>"
                Next
            End If
            ltlmidfielders.Text &= "</tr>"
        End If
        RsRec.Close()

        SqlQuery = New SqlCommand("Select b.Id, b.RoleId, b.FullName, b.Surname, b.PlayingPosition, b.SmallImage As Image From Soccer.dbo.Squads a INNER JOIN Soccer.dbo.Persons b ON b.Id = a.PersonId Where (a.TeamId = " & TeamId & ") And (b.PlayingPosition = 5) And (b.Active  = 1) Order By Surname, FullName", DatabaseConn)
        RsRec = SqlQuery.ExecuteReader
        Dim Forwards As Integer = 0
        While RsRec.Read
            If Forwards = 0 Then
                ltlforwards.Text = "<tr>"
            Else
                If Forwards Mod 4 = 0 Then
                    ltlforwards.Text &= "</tr><tr>"
                End If
            End If
            ltlforwards.Text &= "<td width=""" & Percentage & """ align=""center"" valign=""top"">"
            ltlforwards.Text &= "<table width=""100%"" cellspacing=""0"" cellpadding=""0"" border=""0"">"
            ltlforwards.Text &= "<tr>"
            ltlforwards.Text &= "<td align=""center"" style=""padding-bottom: 2px;"">"
            If RsRec("Image") = "" Then
                ltlforwards.Text &= "<a href=""profiles.aspx?id=" & RsRec("Id") & """><img src=""pics/bloemlogo.jpg"" alt="""" border="""" /></a>"
            Else
                ltlforwards.Text &= "<a href=""profiles.aspx?id=" & RsRec("Id") & """><img src=""http://images.supersport.co.za/" & RsRec("Image") & """ alt="""" border="""" /></a>"
            End If
            ltlforwards.Text &= "</td>"
            ltlforwards.Text &= "</tr>"
            ltlforwards.Text &= "<tr>"
            ltlforwards.Text &= "<td align=""center"">"
            ltlforwards.Text &= "<a style=""color: #000000;"" href=""profiles.aspx?id=" & RsRec("Id") & """>" & RsRec("FullName") & " " & RsRec("Surname") & "</a>"
            ltlforwards.Text &= "</td>"
            ltlforwards.Text &= "</tr>"
            ltlforwards.Text &= "</table>"
            ltlforwards.Text &= "</td>"
            Forwards = Forwards + 1
        End While
        If Forwards > 0 Then
            If Forwards Mod 4 > 0 Then
                tmpCount = 4 - (Forwards Mod 4)
                For I = 1 To tmpCount
                    ltlforwards.Text &= "<td width=""" & Percentage & """ align=""center"" valign=""top""></td>"
                Next
            End If
            ltlforwards.Text &= "</tr>"
        End If
        RsRec.Close()

    End Sub

    Private Sub Information(ByVal Id As String)

        SqlQuery = New SqlCommand("Select Jersey From Soccer.dbo.Squads Where (TeamId = " & TeamId & ") And (PersonId = " & Id & ")", DatabaseConn)
        Dim Number As String = SqlQuery.ExecuteScalar
        Dim PlayingPosition As Integer = 0

        SqlQuery = New SqlCommand("Select a.*, b.Name As Country From Soccer.dbo.Persons a INNER JOIN Soccer.dbo.Countries b ON b.Id = a.BirthCountryId Where (a.Id = " & Id & ")", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            Dim BirthDate As DateTime = RsRec("BirthDate")
            If Number <> "" Then
                ltlname.Text = "<div class=""art_title"" style=""padding-bottom: 5px;"">Profile : " & Number & ". " & RsRec("FullName") & " " & RsRec("Surname") & "</div>"
            Else
                ltlname.Text = "<div class=""art_title"" style=""padding-bottom: 5px;"">Profile : " & RsRec("FullName") & " " & RsRec("Surname") & "</div>"
            End If
            If RsRec("LargeImage") <> "" Then
                ltlprofile.Text &= "<img src=""http://images.supersport.co.za/" & RsRec("LargeImage") & """ border="""" alt="""" align=""right"" style=""border: 1px solid #01743d; "" />"
            End If
            If RsRec("Nickname") <> "" Then
                ltlprofile.Text &= "<b>Nickname:</b> " & RsRec("Nickname") & "<br />"
            End If
            If BirthDate.Year > 1900 Then
                ltlprofile.Text &= "<b>Date of birth:</b> " & BirthDate.ToString("dd MMMM yyyy") & "<br />"
            End If
            If RsRec("Height") <> "" Then
                ltlprofile.Text &= "<b>Height:</b> " & RsRec("Height") & "m" & "<br />"
            End If
            If RsRec("Weight") <> "" Then
                ltlprofile.Text &= "<b>Weight:</b> " & RsRec("Weight") & "kg" & "<br />"
            End If

            Try
                PlayingPosition = CType(RsRec("PlayingPosition"), Integer)
            Catch ex As Exception
                PlayingPosition = 0
            End Try

            If PlayingPosition > 1 Then
                Dim Position As String = ""
                Select Case PlayingPosition
                    Case 2
                        Position = "Goalkeeper"
                    Case 3
                        Position = "Defender"
                    Case 4
                        Position = "Midfielder"
                    Case 5
                        Position = "Forward"
                End Select

                ltlprofile.Text &= "<b>Position:</b> " & Position & "<br />"
            End If

            ltlprofile.Text &= "<b>Nationality:</b> " & RsRec("Country") & "<br />"
            If RsRec("Clubs") <> "" Then
                ltlprofile.Text &= "<b>Previous Clubs:</b> " & RsRec("Clubs") & "<br />"
            End If
            ltlprofile.Text &= "<table cellspacing='0' cellpadding='0' border='0'><tr><td style='padding-top: 15px; color: #000000; font-weight: bold; padding-bottom: 2px;'>About me:</td></tr></table>"
            If RsRec("FavOpponent") <> "" Then
                ltlprofile.Text &= "<b>Favourite opponent:</b> " & RsRec("FavOpponent") & "<br />"
            End If
            If RsRec("FavFood") <> "" Then
                ltlprofile.Text &= "<b>Favourite food:</b> " & RsRec("FavFood") & "<br />"
            End If
            If RsRec("FavFilm") <> "" Then
                ltlprofile.Text &= "<b>Favourite film:</b> " & RsRec("FavFilm") & "<br />"
            End If
            If RsRec("FavMusic") <> "" Then
                ltlprofile.Text &= "<b>Favourite music:</b> " & RsRec("FavMusic") & "<br />"
            End If
            If RsRec("Car") <> "" Then
                ltlprofile.Text &= "<b>Favourite car:</b> " & RsRec("Car") & "<br />"
            End If
            If RsRec("Hobbies") <> "" Then
                ltlprofile.Text &= "<b>Hobbies:</b> " & RsRec("Hobbies") & "<br />"
            End If
            If RsRec("Motto") <> "" Then
                ltlprofile.Text &= "<b>Motto:</b> " & RsRec("Motto") & "<br />"
            End If
            If RsRec("Message") <> "" Then
                ltlprofile.Text &= "<table cellspacing='0' cellpadding='0' border='0'><tr><td style='padding-top: 15px; color: #C6CDE7; font-weight: bold; padding-bottom: 2px;'>Highlights/Lowlights:</td></tr></table>"
                ltlprofile.Text &= RsRec("Message")
            End If
        End While
        RsRec.Close()

    End Sub

End Class

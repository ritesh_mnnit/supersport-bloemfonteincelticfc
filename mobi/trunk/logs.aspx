﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="logs.aspx.cs" Inherits="SuperSport_BloemCelticMobi.logs" %>
<%@ Register src="~/MatchCentreUI/Mobi/Logs/GenericLogs.ascx" tagname="GenericLogs" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="LogHeader" class="header">
        Log
        <asp:Image ID="imgLogHeaderIcon" runat="server" AlternateText="Log" ImageUrl="~/images/header_arrow_s.png" CssClass="headericonright_e" />
    </div>
    <div id="LogContent" class="home-Container">
        <uc:GenericLogs ID="ucLogs" runat="server" CssClass="logs" CentroidTeamName="Bloem Celtic" />
        <br class="clear" />
    </div>
    <div id="FixturesHeader" class="header">
        <asp:HyperLink ID="lnkFixturesPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.FixturesRoute, null) %>'>
            Full Fixtures
            <asp:Image ID="imgFixturesHeaderIcon" runat="server" AlternateText="Fixtures" ImageUrl="~/images/header_arrow_e.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
    <div id="ResultsHeader" class="header">
        <asp:HyperLink ID="lnkResultsPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.ResultsRoute, null) %>'>
            Full Results
            <asp:Image ID="imgResultsHeaderIcon" runat="server" AlternateText="Results" ImageUrl="~/images/header_arrow_e.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
</asp:Content>

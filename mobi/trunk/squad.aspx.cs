﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.DAL.Football;
using SuperSport.MatchCentre.UI.Utils;

namespace SuperSport_BloemCelticMobi
{
    public partial class squad : System.Web.UI.Page
    {
        protected FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            RenderSquadList();
        }

        private void RenderSquadList()
        {
            ucSquadBrowser.PlayerCentre = _FootballCentre;
            ucSquadBrowser.TeamID = SiteConfig.DefaultTeamID;
            ucSquadBrowser.DataBind();
        }
    }
}
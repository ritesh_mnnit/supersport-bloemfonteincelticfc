﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.MatchCentre.DAL.Football;
using SuperSport_BloemCelticMobi.General;

namespace SuperSport_BloemCelticMobi
{
    public partial class results : PageBase
    {
        protected FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPageTitle("Results");
            RenderLeagueResults();
        }

        private void RenderLeagueResults()
        {
            ResultsListLeague.ResultsCentre = _FootballCentre;
            ResultsListLeague.SiteID = SiteConfig.SiteID;
            ResultsListLeague.DataBind();
        }
    }
}
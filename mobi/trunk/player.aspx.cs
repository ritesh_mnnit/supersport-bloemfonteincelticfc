﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.DAL.Football;
using SuperSport_BloemCelticMobi.General;

namespace SuperSport_BloemCelticMobi
{
    public partial class player : PageBase
    {
        private FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPageTitle("Players");
            RenderPlayer();
        }

        private void RenderPlayer()
        {
            ucPlayerView.PlayerCentre = _FootballCentre;
            ucPlayerView.DataBind();
        }
    }
}
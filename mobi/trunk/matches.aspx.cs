﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.DAL.Football;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport_BloemCelticMobi.General;

namespace SuperSport_BloemCelticMobi
{
    public partial class matches : PageBase
    {
        private FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPageTitle("Matches");
            RenderLeagueFixtures();
            RenderLeagueResults();
            RenderLogs();
        }

        private void RenderLogs()
        {
            ucLogs.LogsCentre = _FootballCentre;
            ucLogs.Category = SiteConfig.DefaultCategory;
            ucLogs.DataBind();
        }

        private void RenderLeagueFixtures()
        {
            FixturesListLeague.FixturesCentre = _FootballCentre;
            FixturesListLeague.SiteID = SiteConfig.SiteID;
            FixturesListLeague.DataBind();
        }

        private void RenderLeagueResults()
        {
            ResultsListLeague.ResultsCentre = _FootballCentre;
            ResultsListLeague.SiteID = SiteConfig.SiteID;
            ResultsListLeague.DataBind();
        }
    }
}
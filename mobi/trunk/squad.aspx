﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="squad.aspx.cs" Inherits="SuperSport_BloemCelticMobi.squad" %>
<%@ Register src="~/MatchCentreUI/Mobi/Players/GenericSquadBrowser.ascx" tagname="SquadBrowser" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <uc:SquadBrowser ID="ucSquadBrowser" runat="server" CssClass="squad" PositionSections="Goalkeeper,Defender,Midfielder,Forward" />
</asp:Content>

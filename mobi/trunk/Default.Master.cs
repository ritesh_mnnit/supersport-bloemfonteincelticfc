﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.CMS.Models;
using SuperSport_BloemCelticMobi.Properties;
using SuperSport_BloemCelticMobi.General;

namespace SuperSport_BloemCelticMobi
{
    public partial class Default : System.Web.UI.MasterPage, IPageInfo
    {
        private string m_PageTitlePrefix = "";
        private string m_LeagueName = "";
        public string PageTitlePrefix
        {
            get {
                if(m_PageTitlePrefix == "")
                {
                    m_PageTitlePrefix = Settings.Default.PageTitlePrefix.Trim();
                }
                return m_PageTitlePrefix;
            }
        }

        public string LeagueName
        {
            get
            {
                if (m_LeagueName.Trim() == "")
                {
                    m_LeagueName = Settings.Default.LeagueName.Trim();
                }
                return m_LeagueName;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != Request.UrlReferrer && !Request.FilePath.Contains("default.aspx"))
            {
                lnkBack.Visible = true;
                lnkBack.NavigateUrl = Request.UrlReferrer.ToString();
            }
            RenderNavigation();
        }

        private void RenderNavigation()
        {
            ucNavigationMenuTop.CustomNavigationList = GetTopNavigation();
            ucNavigationMenuTop.DataBind();

            ucNavigationMenuBot.SiteID = SiteConfig.NavigationSiteID;
            ucNavigationMenuBot.DataBind();
        }

        private List<NavNode> GetTopNavigation()
        {
            SuperSport.CMS.Models.Navigation navigation = new SuperSport.CMS.Models.Navigation();
            navigation = SuperSport.CMS.Models.Navigation.GetNavigation(SiteConfig.NavigationSiteID);
            navigation.navMenuList = navigation.navMenuList.FindAll(navItem => { return (navItem.Label.ToUpper() != "CONTACT US"); });

            return navigation.navMenuList;
        }
    }
}
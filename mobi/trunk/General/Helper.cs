﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperSport_BloemCelticMobi.Properties;

namespace SuperSport_BloemCelticMobi.General
{
    public static class Helper
    {
        public static string GetPageTitle()
        {
            return Settings.Default.PageTitlePrefix.Trim();
        }

        public static string GetLeagueName()
        {
            return Settings.Default.LeagueName.Trim();
        }
    }
}
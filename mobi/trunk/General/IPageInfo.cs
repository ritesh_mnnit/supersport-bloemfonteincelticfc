﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperSport_BloemCelticMobi.General
{
    public interface IPageInfo
    {
        string PageTitlePrefix { get; }
        string LeagueName { get; }
    }
}

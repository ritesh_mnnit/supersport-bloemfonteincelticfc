﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperSport_BloemCelticMobi.General
{
    public class PageBase : System.Web.UI.Page
    {
        public IPageInfo PageInfo
        {
            get 
            {
                if (this.Master != null)
                {
                    try
                    {
                        return this.Master as IPageInfo;
                    }
                    catch
                    {
                        return null;
                    }
                }
                return null;
            }
        }

        public void SetPageTitle(string pageSuffix)
        {
            this.Title = string.Format("{0} - {1}", PageInfo.PageTitlePrefix, pageSuffix.Trim());
        }
    }
}
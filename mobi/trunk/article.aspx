﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="article.aspx.cs" Inherits="SuperSport_BloemCelticMobi.article" %>
<%@ Register src="~/MatchCentreUI/Mobi/News/GenericArticleView.ascx" tagname="ArticleView" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/News/GenericNewsBrowser.ascx" tagname="NewsBrowser" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/News/GenericArticleMetaTags.ascx" tagname="ArticleMetaTags" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <uc:ArticleMetaTags 
        ID="ucArticleMetaTags" 
        runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <uc:ArticleView ID="ucArticleView" runat="server" CssClass="article" />
    <div id="LatestNewsHeader" class="header">
        <asp:HyperLink ID="lnkNewsPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.NewsBrowseRoute, null) %>'>
            More News
        </asp:HyperLink>
    </div>
    <div id="LatestNewsArticleViewContent" class="home-Container">
        <uc:NewsBrowser ID="ucNewsBrowser" runat="server" CssClass="news" Count="2" />
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="photo.aspx.cs" Inherits="SuperSport_BloemCelticMobi.photo" %>
<%@ Register src="~/MatchCentreUI/Mobi/Galleries/GenericPhotoView.ascx" tagname="PhotoView" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="GalleriesHeader" class="header">
        <asp:Literal ID="LiteralGalleryName" runat="server" Text="Gallery"></asp:Literal>
    </div>
    <div id="GalleriesContent" class="home-Container">
        <uc:PhotoView ID="ucPhotoView" runat="server" CssClass="photoview" />
        <br class="clear" />
    </div>
</asp:Content>

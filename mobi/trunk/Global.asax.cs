﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using System.Configuration;
using SuperSport.MatchCentre.UI.Utils.Mobi;

namespace SuperSport_BloemCelticMobi
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RoutingHelper.BuildFixturesRouting("fixtures/", "~/fixtures.aspx");
            RoutingHelper.BuildResultsRouting("results/", "~/results.aspx");
            RoutingHelper.BuildLogsRouting("logs/", "~/logs.aspx");
            RoutingHelper.BuildNewsBrowseRouting("news/", "~/news.aspx");
            RoutingHelper.BuildNewsViewRouting("news/", "~/article.aspx");
            RoutingHelper.BuildMatchesRouting("matches/", "~/matches.aspx");
            RoutingHelper.BuildGalleriesBrowseRouting("galleries/", "~/galleries.aspx");
            RoutingHelper.BuildGalleryBrowseRouting("gallery/", "~/gallery.aspx");
            RoutingHelper.BuildPhotoViewRouting("photo/", "~/photo.aspx");
            RoutingHelper.BuildSquadBrowseRouting("players/", "~/squad.aspx");
            RoutingHelper.BuildPlayerViewRouting("player/", "~/player.aspx");
            RoutingHelper.BuildContentViewRouting("content/", "~/content.aspx");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Database errors
            System.Exception myError = Server.GetLastError();

            System.Data.SqlClient.SqlConnection DatabaseConn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["ltdWriter"].ConnectionString);
            string ipAddress = "";
            string Referrer = "";
            string errorText = myError.ToString();
            bool logError = true;

            if (Request.Url.ToString().IndexOf(".beta") >= 0)
            {
                logError = false;
            }
            if (errorText.IndexOf("The file") >= 0 && errorText.IndexOf("does not exist") >= 0)
            {
                logError = false;
            }

            if (HttpContext.Current.Request.ServerVariables["Remote_Addr"] != null)
            {
                ipAddress = HttpContext.Current.Request.ServerVariables["Remote_Addr"].ToString();
            }

            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                Referrer = HttpContext.Current.Request.UrlReferrer.ToString().Replace("'", "''");
            }

            try
            {
                if (logError == true)
                {
                    System.Data.SqlClient.SqlCommand SqlQuery = new System.Data.SqlClient.SqlCommand("Insert Into SuperSportZone.dbo.zonewebsiteerrors (siteId, url, referrer, error, ipAddress) Values (161, '" + Request.Url.ToString().Replace("'", "''") + "', '" + Referrer + "', '" + errorText.Replace("'", "''") + "', '" + ipAddress.Replace("'", "''") + "')", DatabaseConn);
                    DatabaseConn.Open();
                    SqlQuery.ExecuteNonQuery();
                    DatabaseConn.Close();
                }
            }
            catch
            {

            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
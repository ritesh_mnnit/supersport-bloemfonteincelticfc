﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.CMS.Models;
using SuperSport_BloemCelticMobi.General;
using SuperSport.MatchCentre.UI.Mobi.SocialNetwork;

namespace SuperSport_BloemCelticMobi
{
    public partial class article : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != Page.RouteData.Values["headline"])
            {
                Page.Title = string.Format("{0} - {1}", PageInfo.PageTitlePrefix, Page.RouteData.Values["headline"].ToString().Replace('_', ' '));
            }
            ucArticleView.DataBind();

            ucArticleMetaTags.OpenGraphMetaType = OpenGraphMetaType.Article;
            ucArticleMetaTags.DataBind();

            RenderTopNews();
        }

        private void RenderTopNews()
        {
            ucNewsBrowser.SiteID = SiteConfig.SiteID;
            ucNewsBrowser.DataBind();
        }
    }
}
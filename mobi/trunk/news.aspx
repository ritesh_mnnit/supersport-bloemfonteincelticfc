﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="news.aspx.cs" Inherits="SuperSport_BloemCelticMobi.news" %>
<%@ Register src="~/MatchCentreUI/Mobi/News/GenericNewsBrowser.ascx" tagname="GenericNewsBrowser" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Polls/GenericFeaturedPoll.ascx" tagname="FeaturedPoll" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Galleries/GenericGalleriesBrowser.ascx" tagname="Galleries" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Logs/GenericLogs.ascx" tagname="GenericLogs" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="LatestNewsHeader" class="header">Latest News</div>
    <div id="LatestNewsContent" class="home-Container">
        <uc:GenericNewsBrowser ID="ucNewsBrowser" runat="server" CssClass="news" Count="4" />
    </div>
    <div id="PollsHeader" class="header" runat="server">Poll</div>
    <div id="PollsContent" class="home-Container" runat="server">
        <form runat="server">
            <uc:FeaturedPoll ID="ucFeaturedPoll" runat="server" CssClass="poll" />
        </form>
    </div>
    <div id="GalleriesHeader" class="header">
        <asp:HyperLink ID="lnkGalleriesPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.GalleriesBrowseRoute, null) %>'>
            Latest Pics
            <asp:Image ID="imgGalleriesHeaderIcon" runat="server" AlternateText="Latest Pics" ImageUrl="~/images/header_arrow_s.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
    <div id="GalleriesContent" class="home-Container">
        <uc:Galleries ID="ucGalleries" runat="server" CssClass="galleries-control" Count="1" />
        <br class="clear" />
        <asp:HyperLink ID="lnkGalleriesPage2" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.GalleriesBrowseRoute, null) %>'
        CssClass="home-MoreLink">
            ...galleries
        </asp:HyperLink>
        <br class="clear" />
    </div>
    <div id="LogHeader" class="header">
        <asp:HyperLink ID="lnkLogsPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.LogsRoute, null) %>'>
            Log
            <asp:Image ID="imgLogHeaderIcon" runat="server" AlternateText="Log" ImageUrl="~/images/header_arrow_s.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
    <div id="LogContent" class="home-Container">
        <uc:GenericLogs ID="ucLogs" runat="server" CssClass="logs" CentroidTeamName="Bloem Celtic" Count="5" />
        <br class="clear" />
    </div>
</asp:Content>

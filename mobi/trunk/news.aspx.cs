﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.MatchCentre.DAL.Football;
using SuperSport_BloemCelticMobi.General;

namespace SuperSport_BloemCelticMobi
{
    public partial class news : PageBase
    {
        protected FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPageTitle("News");
            RenderTopNews();
            RenderFeaturedPoll();
            RenderGalleries();
            RenderLogs();
        }

        private void RenderTopNews()
        {
            ucNewsBrowser.SiteID = SiteConfig.SiteID;
            ucNewsBrowser.DataBind();
        }

        private void RenderFeaturedPoll()
        {
            try
            {
                ucFeaturedPoll.SiteID = SiteConfig.SiteID;
                ucFeaturedPoll.PollCategory = SiteConfig.DefaultPollCategory;
                ucFeaturedPoll.DataBind();
            }
            catch
            {
                //some exception occurred here, so don't display the polls section
                PollsHeader.Visible=false;
                PollsContent.Visible = false;
            }
        }

        private void RenderGalleries()
        {
            ucGalleries.SiteID = SiteConfig.SiteID;
            ucGalleries.DataBind();
        }

        private void RenderLogs()
        {
            ucLogs.LogsCentre = _FootballCentre;
            ucLogs.Category = SiteConfig.DefaultCategory;
            ucLogs.DataBind();
        }
    }
}
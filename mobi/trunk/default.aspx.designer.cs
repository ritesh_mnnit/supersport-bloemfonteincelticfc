﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace SuperSport_BloemCelticMobi {
    
    
    public partial class _default {
        
        /// <summary>
        /// ucNewsBrowser control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SuperSport.MatchCentre.UI.Mobi.News.GenericNewsBrowser ucNewsBrowser;
        
        /// <summary>
        /// lnkNewsPage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink lnkNewsPage;
        
        /// <summary>
        /// ucFixturesList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SuperSport.MatchCentre.UI.Mobi.Fixtures.GenericFixtureList ucFixturesList;
        
        /// <summary>
        /// lnkFixturesPage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink lnkFixturesPage;
        
        /// <summary>
        /// ucResultsList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SuperSport.MatchCentre.UI.Mobi.Results.GenericResultsList ucResultsList;
        
        /// <summary>
        /// lnkResultsPage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink lnkResultsPage;
        
        /// <summary>
        /// lnkLogsPage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink lnkLogsPage;
        
        /// <summary>
        /// imgLogHeaderIcon control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgLogHeaderIcon;
        
        /// <summary>
        /// ucLogs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SuperSport.MatchCentre.UI.Mobi.Logs.GenericLogs ucLogs;
    }
}

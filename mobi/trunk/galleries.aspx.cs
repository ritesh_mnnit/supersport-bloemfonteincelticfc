﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport_BloemCelticMobi.General;

namespace SuperSport_BloemCelticMobi
{
    public partial class galleries : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPageTitle("Latest Pics");
            ucGalleries.SiteID = SiteConfig.SiteID;
            ucGalleries.DataBind();
        }
    }
}
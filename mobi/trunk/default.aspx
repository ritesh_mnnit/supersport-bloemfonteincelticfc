﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SuperSport_BloemCelticMobi._default" ContentType="text/html" %>
<%@ Register src="~/MatchCentreUI/Mobi/News/GenericNewsBrowser.ascx" tagname="GenericNewsBrowser" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Fixtures/IconFixturesList.ascx" tagname="FixturesList" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Results/IconResultsList.ascx" tagname="ResultsList" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Logs/GenericLogs.ascx" tagname="GenericLogs" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Polls/GenericFeaturedPoll.ascx" tagname="FeaturedPoll" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
<div id="LatestNewsHeader" class="header">Latest News</div>
<div id="LatestNewsContent" class="home-Container">
    <uc:GenericNewsBrowser ID="ucNewsBrowser" runat="server" CssClass="news" Count="2" />
    <asp:HyperLink ID="lnkNewsPage" runat="server" 
    NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.NewsBrowseRoute, null) %>'
    CssClass="home-MoreLink">...more news</asp:HyperLink>
    <br class="clear" />
</div>

<div id="ResultsFixturesHeader" class="header">Results and Next Match</div>

<div id="ResultsFixturesContent">
    <div id="FixturesHeader" class="subheader">Next Match</div>
    <div id="FixturesContent" class="home-Container">
        <uc:FixturesList ID="ucFixturesList" runat="server" CssClass="iconfixtures" Count="1" />
        <asp:HyperLink ID="lnkFixturesPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.FixturesRoute, null) %>'
        CssClass="home-MoreLink">...more fixtures</asp:HyperLink>
        <br class="clear" />
    </div>
    <div id="ResultsHeader" class="subheader">Latest Result</div>
    <div id="ResultsContent" class="home-Container">
        <uc:ResultsList ID="ucResultsList" runat="server" CssClass="iconresults" Count="1" />
        <asp:HyperLink ID="lnkResultsPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.ResultsRoute, null) %>'
        CssClass="home-MoreLink">...more results</asp:HyperLink>
        <br class="clear" />
    </div>
</div>
<div id="LogHeader" class="header">
    <asp:HyperLink ID="lnkLogsPage" runat="server" 
    NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.LogsRoute, null) %>'>
        Log
        <asp:Image ID="imgLogHeaderIcon" runat="server" AlternateText="Log" ImageUrl="~/images/header_arrow_e.png" CssClass="headericonright_e" />
    </asp:HyperLink>
</div>
    <div id="LogContent" class="home-Container">
        <uc:GenericLogs ID="ucLogs" runat="server" CssClass="logs" CentroidTeamName="Bloem Celtic" Count="5" />
        <br class="clear" />
    </div>

</asp:Content>

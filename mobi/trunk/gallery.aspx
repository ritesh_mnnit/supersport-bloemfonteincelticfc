﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="gallery.aspx.cs" Inherits="SuperSport_BloemCelticMobi.gallery" %>
<%@ Register src="~/MatchCentreUI/Mobi/Galleries/GenericGalleryBrowser.ascx" tagname="Gallery" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="GalleriesHeader" class="header">
        <asp:Literal ID="LiteralGalleryName" runat="server" Text="Gallery"></asp:Literal>
    </div>
    <div id="GalleriesContent" class="home-Container">
        <uc:Gallery ID="ucGallery" runat="server" CssClass="gallery-control" Count="3" />
        <br class="clear" />
    </div>
</asp:Content>

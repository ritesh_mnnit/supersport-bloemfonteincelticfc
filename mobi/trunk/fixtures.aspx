﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="fixtures.aspx.cs" Inherits="SuperSport_BloemCelticMobi.fixtures" %>
<%@ Register src="~/MatchCentreUI/Mobi/Fixtures/CompactFixturesList.ascx" tagname="FixturesList" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="FixturesHeader" class="header">Fixtures</div>
    <div id="FixturesContent" class="home-Container">
        <uc:FixturesList ID="FixturesListLeague" runat="server" CssClass="fixtures" Count="50" />
    </div>

</asp:Content>

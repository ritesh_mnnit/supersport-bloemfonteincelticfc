﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericFixturesList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Fixtures.GenericFixtureList" EnableViewState="false" %>
<div class="<%=CssClass%>">
<asp:Repeater ID="rptFixtures" runat="server" 
    onitemdatabound="rptFixtures_ItemDataBound">
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <div class="<%=CssClass%>Item">
        <span class="<%=CssClass%>DateContainer">
            <span class="<%=CssClass%>LabelDate">Date: </span>
            <span class="<%=CssClass%>ItemDate"><%# String.Format("{0:dd MMMM yyyy}",Eval("MatchDateTime"))%></span>
        </span>
        <span class="<%=CssClass%>TimeContainer">
            <span class="<%=CssClass%>LabelDate">Time: </span>
            <span class="<%=CssClass%>ItemDate"><%# String.Format("{0:HH:mm}",Eval("MatchDateTime"))%></span>
        </span>
        <br class="<%=CssClass%>ItemBreakDate" />
        <span class="<%=CssClass%>ItemHomeTeam"><%# Eval("HomeTeam")%></span>
        <span class="<%=CssClass%>ItemVs">vs</span>
        <span class="<%=CssClass%>ItemAwayTeam"><%# Eval("AwayTeam")%></span>
        <br class="<%=CssClass%>ItemBreakTeams" />
        <span class="<%=CssClass%>ItemVenue"><%# Eval("Location")%></span>
        <br class="<%=CssClass%>ItemBreakVenue" />
        <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="ItemLiveCommentaryLink" Visible="false">
        <%-- lnkLiveCommentary.CssClass will be prepended with GenericFixtureList.CssClass --%>
            <asp:Literal ID="LiteralLiveCommentary" runat="server">Live Commentary</asp:Literal>
        </asp:HyperLink>
        <br class="<%=CssClass%>ItemBreak" />
    </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <div class="<%=CssClass%>AltItem">
        <span class="<%=CssClass%>AltDateContainer">
            <span class="<%=CssClass%>AltLabelDate">Date: </span>
            <span class="<%=CssClass%>AltItemDate"><%# String.Format("{0:dd MMMM yyyy}",Eval("MatchDateTime"))%></span>
        </span>
        <span class="<%=CssClass%>AltTimeContainer">
            <span class="<%=CssClass%>AltLabelDate">Time: </span>
            <span class="<%=CssClass%>AltItemDate"><%# String.Format("{0:HH:mm}",Eval("MatchDateTime"))%></span>
        </span>
        <br class="<%=CssClass%>AltItemBreakDate" />
        <span class="<%=CssClass%>AltItemHomeTeam"><%# Eval("HomeTeam")%></span>
        <span class="<%=CssClass%>AltItemVs">vs</span>
        <span class="<%=CssClass%>AltItemAwayTeam"><%# Eval("AwayTeam")%></span>
        <br class="<%=CssClass%>AltItemBreakTeams" />
        <span class="<%=CssClass%>AltItemVenue"><%# Eval("Location")%></span>
        <br class="<%=CssClass%>AltItemBreakVenue" />
        <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="AltItemLiveCommentaryLink" Visible="false">
        <%-- lnkLiveCommentary.CssClass will be prepended with GenericFixtureList.CssClass --%>
            <asp:Literal ID="LiteralLiveCommentary" runat="server">Live Commentary</asp:Literal>
        </asp:HyperLink>
        <br class="<%=CssClass%>AltItemBreak" />
    </div>
    </AlternatingItemTemplate>
    <SeparatorTemplate>
    </SeparatorTemplate>
    <FooterTemplate>
    <span class="<%=CssClass%>EmptyMessage">
        <asp:Literal ID="LiteralEmptyMessage" runat="server" Visible="false">
            There are currently no fixtures available.
        </asp:Literal>
    </span>
    </FooterTemplate>
</asp:Repeater>
</div>

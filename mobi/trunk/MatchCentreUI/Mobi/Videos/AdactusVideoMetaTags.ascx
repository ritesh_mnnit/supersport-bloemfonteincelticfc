﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdactusVideoMetaTags.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Videos.AdactusVideoMetaTags" EnableViewState="false" %>
<meta id="ogtitle" runat="server" property="og:title" />
<meta id="ogtype" runat="server" property="og:type" />
<meta id="ogurl" runat="server" property="og:url" />
<meta id="ogimage" runat="server" property="og:image" />
<meta id="fbadmins" runat="server" property="fb:admins" />
<meta id="ogsite_name" runat="server" property="og:site_name" />
<meta id="ogdescription" runat="server" property="og:description" />
<meta id="fbapp_id" runat="server" property="fb:app_id" />

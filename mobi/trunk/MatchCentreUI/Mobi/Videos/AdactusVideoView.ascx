﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdactusVideoView.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Videos.AdactusVideoView" %>
<div class="<%=CssClass %>">
    <asp:Panel ID="pnlVideoView" runat="server">
        <asp:ListView ID="rptGallery" runat="server" 
            ItemPlaceholderID="PlaceHolderItem" 
            onitemdatabound="rptGallery_ItemDataBound">
            <LayoutTemplate>
        <div class="<%#CssClass %>ItemContainer">
            <asp:PlaceHolder ID="PlaceHolderItem" runat="server"></asp:PlaceHolder>
        </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="<%#CssClass %>Item">
                    <div class="<%#CssClass %>ItemHeadlineContainer">
                        <a class="<%#CssClass %>ItemHeadlineLink" href="<%# GetHighQualityVideoViewUrl(Container.DataItem) %>">
                            <%# Eval("ShortName")%>
                        </a>
                    </div>
                    <div class="<%#CssClass %>ItemImageContainer">
                        <a class="<%#CssClass %>ItemImageLink" href="<%# GetHighQualityVideoViewUrl(Container.DataItem) %>">
                            <asp:Image ID="imgGalleryPic" runat="server" CssClass="ItemImage" />
                        </a>
                    </div>
                    <div class="<%#CssClass %>ItemTextContainer">
                        <a class="<%#CssClass %>ItemDurationLink" href="<%# GetHighQualityVideoViewUrl(Container.DataItem) %>">
                            <%# (Convert.ToInt32(Eval("LengthInSeconds")) / 60)%>:<%# (Convert.ToInt32(Eval("LengthInSeconds")) % 60).ToString().PadLeft(2,'0') %>
                        </a>
                        <br class="<%#CssClass %>ItemDurationBreak" />
                        <div class="<%#CssClass %>ItemLinksContainer">
                            <asp:ListView ID="rptVideoStreams" runat="server">
                                <LayoutTemplate>
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <a href="<%# Eval("Url")%>" class="<%#CssClass %>ItemStreamLink"><%# Eval("Quality")%></a>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                        <span class="<%#CssClass %>ItemDescriptionLink" href="<%# GetHighQualityVideoViewUrl(Container.DataItem) %>">
                                <%# Eval("Description")%>
                        </span>
                        <br class="<%#CssClass %>ItemDescriptionBreak" />
                        <a class="<%#CssClass %>ItemDateTimeLink" href="<%# GetHighQualityVideoViewUrl(Container.DataItem) %>">
                            <%# string.Format("{0:" + DisplayDateFormat + "}", Eval("DateCreated"))%>
                        </a>
                        <br class="<%#CssClass %>ItemDateTimeBreak" />
                        <br class="<%#CssClass %>ItemBreak" />
                    </div>
                    <div class="<%=CssClass %>SocialLinks">
                        <asp:HyperLink ID="lnkTwitter" runat="server" Target="_blank" Visible="false">
                            <img src="<%= (!string.IsNullOrEmpty(TwitterImage) ? TwitterImage : "http://www.supersport.com/images/sm_twitter.png" ) %>" alt="Twitter" title="Twitter" />
                        </asp:HyperLink>
                        <asp:HyperLink ID="lnkFacebook" runat="server" Target="_blank" Visible="false">
                            <img src="<%= (!string.IsNullOrEmpty(FacebookImage) ? FacebookImage : "http://www.supersport.com/images/sm_fb.png" ) %>" alt="Facebook" title="Facebook" />
                        </asp:HyperLink>
	                </div>
                    <div class="<%=CssClass %>SocialComments" />
                        <div id="fb-root"></div>
                        <script>
                            (function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                fjs.parentNode.insertBefore(js, fjs);
                            } (document, 'script', 'facebook-jssdk'));
                        </script>
                        <fb:comments href="<%= HttpContext.Current.Request.Url.AbsoluteUri %>"  num_posts="10" mobile="true" width="0"></fb:comments>    
                    </div>
                </div>
            </ItemTemplate>
            <EmptyDataTemplate>
                <div class="<%#CssClass %>EmptyMessage">
                    The requested video is not available for your device.
                </div>
            </EmptyDataTemplate>
        </asp:ListView>
    </asp:Panel>
    <asp:Panel ID="pnlVideoViewNotAvailable" runat="server" Visible="false">
        <div class="<%= CssClass %>EmptyMessage">
            The requested video is not available for your region or device.
        </div>
    </asp:Panel>
    <asp:Literal ID="LiteralCountryCode" runat="server"></asp:Literal>
    <asp:Literal ID="LiteralIP" runat="server"></asp:Literal>
</div>


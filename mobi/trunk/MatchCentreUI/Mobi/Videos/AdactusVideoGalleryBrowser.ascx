﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdactusVideoGalleryBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Videos.AdactusVideoGalleryBrowser" EnableViewState="false" %>

<%@ Register src="../Utils/GenericPager.ascx" tagname="GenericPager" tagprefix="uc1" %>

<div class="<%=CssClass %>">
    <uc1:GenericPager ID="ucGenericPagerTop" runat="server" CssClass="PagerTop" />
    <asp:ListView ID="rptGallery" runat="server" 
        ItemPlaceholderID="PlaceHolderItem" 
        onitemdatabound="rptGallery_ItemDataBound">
        <LayoutTemplate>
    <div class="<%#CssClass %>ItemContainer">
        <asp:PlaceHolder ID="PlaceHolderItem" runat="server"></asp:PlaceHolder>
    </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="<%#CssClass %>Item">
                <div class="<%#CssClass %>ItemImageContainer">
                    <a class="<%#CssClass %>ItemImageLink" href="<%# GetVideoViewUrl(Convert.ToInt32(Eval("Id")), Convert.ToString(Eval("UrlName"))) %>">
                        <asp:Image ID="imgGalleryPic" runat="server" CssClass="ItemImage" />
                    </a>
                </div>
                <div class="<%#CssClass %>ItemTextContainer">
                    <a class="<%#CssClass %>ItemDurationLink" href="<%# GetVideoViewUrl(Convert.ToInt32(Eval("Id")), Convert.ToString(Eval("UrlName"))) %>">
                        <%# (Convert.ToInt32(Eval("LengthInSeconds")) / 60)%>:<%# (Convert.ToInt32(Eval("LengthInSeconds")) % 60).ToString().PadLeft(2,'0') %>
                    </a>
                    <br class="<%#CssClass %>ItemDurationBreak" />
                    <a class="<%#CssClass %>ItemHeadlineLink" href="<%# GetVideoViewUrl(Convert.ToInt32(Eval("Id")), Convert.ToString(Eval("UrlName"))) %>">
                        <%# Eval("ShortName")%>
                    </a>
                    <br class="<%#CssClass %>ItemHeadlineBreak" />
                    <a class="<%#CssClass %>ItemDateTimeLink" href="<%# GetVideoViewUrl(Convert.ToInt32(Eval("Id")), Convert.ToString(Eval("UrlName"))) %>">
                        <%# string.Format("{0:" + DisplayDateFormat + "}", Eval("DateCreated"))%>
                    </a>
                    <br class="<%#CssClass %>ItemDateTimeBreak" />
                    <span class="<%#CssClass %>ItemDescriptionLink" href="<%# GetVideoViewUrl(Convert.ToInt32(Eval("Id")), Convert.ToString(Eval("UrlName"))) %>">
                            <%# Eval("Description")%>
                    </span>
                    <br class="<%#CssClass %>ItemDescriptionBreak" />
                    <a class="<%#CssClass %>ItemViewLink" href="<%# GetVideoViewUrl(Convert.ToInt32(Eval("Id")), Convert.ToString(Eval("UrlName"))) %>">
                            <%# VideoViewLinkText%>
                    </a>
                    <br class="<%#CssClass %>ItemViewBreak" />
                </div>
            </div>
        </ItemTemplate>
        <EmptyDataTemplate>
            <div class="<%#CssClass %>EmptyMessage">
                The requested videos are not available for your device.
            </div>
        </EmptyDataTemplate>
    </asp:ListView>
    <uc1:GenericPager ID="ucGenericPagerBot" runat="server" CssClass="PagerBot" />
</div>


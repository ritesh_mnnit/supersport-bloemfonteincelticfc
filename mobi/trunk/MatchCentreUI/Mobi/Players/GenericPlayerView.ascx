﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericPlayerView.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Players.GenericPlayerView" EnableViewState="false" %>
<div class="<%=CssClass %>">
    <div class="<%=CssClass %>ProfileHeader">
        <asp:Literal ID="LiteralProfileHeader" runat="server"></asp:Literal>
    </div>
    <div class="<%=CssClass %>ProfileContainer">
        <span class="<%=CssClass %>ProfileCombinedNameLabel">
            <asp:Literal ID="LiteralCombinedName" runat="server"></asp:Literal>
        </span>
        <br class="<%=CssClass %>ProfileCombinedNameBreak" />
        <asp:Image ID="imgProfilePic" runat="server" CssClass="ProfileImage" />
        <%-- imgProfilePic.CssClass will be appended with GenericPlayerView.CssClass property --%>
        <span class="<%=CssClass %>ProfileLabel">
            DOB:
        </span>
        <span class="<%=CssClass %>ProfileValue">
            <asp:Literal ID="LiteralDateOfBirth" runat="server"></asp:Literal>
        </span>
        <br class="<%=CssClass %>ProfileAttributeBreak" />
        <span class="<%=CssClass %>ProfileLabel">
            Position:
        </span>
        <span class="<%=CssClass %>ProfileValue">
            <asp:Literal ID="LiteralPosition" runat="server"></asp:Literal>
        </span>
        <br class="<%=CssClass %>ProfileAttributeBreak" />
        <span class="<%=CssClass %>ProfileLabel">
            Previous Clubs:
        </span>
        <span class="<%=CssClass %>ProfileValue">
            <asp:Literal ID="LiteralPreviousClubs" runat="server"></asp:Literal>
        </span>
        <br class="<%=CssClass %>ProfileAttributeBreak" />
        <span class="<%=CssClass %>ProfileLabel">
            Stats:
        </span>
        <span class="<%=CssClass %>ProfileValue">
            <asp:Literal ID="LiteralStats" runat="server"></asp:Literal>
        </span>
        <br class="<%=CssClass %>ProfileAttributeBreak" />
        <span class="<%=CssClass %>ProfileInfoLabel">
            Profile Info:
        </span>
        <br class="<%=CssClass %>ProfileAttributeBreak" />
        <span class="<%=CssClass %>ProfileValue">
            <asp:Literal ID="LiteralProfileInfo" runat="server"></asp:Literal>
        </span>
        <br class="<%=CssClass %>ProfileAttributeBreak" />
        <br class="<%=CssClass %>BodyBreak" />
    </div>
    <div class="<%=CssClass %>SquadLinkContainer">
        <asp:HyperLink ID="lnkSquad" runat="server" CssClass="SquadLink">
        <%-- lnkSquad.CssClass will be appended with GenericPlayerView.CssClass property --%>
            ...See Other <asp:Literal ID="LiteralSquadCategory" runat="server"></asp:Literal>
        </asp:HyperLink>
    </div>
</div>

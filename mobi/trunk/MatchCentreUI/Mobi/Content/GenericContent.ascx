﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericContent.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Content.GenericContent" EnableViewState="false" %>
<div class="<%=CssClass %>">
    <asp:Panel ID="pnlHeader" runat="server" CssClass="ContentHeader">
        <asp:Literal ID="LiteralHeader" runat="server"></asp:Literal>
    </asp:Panel>
    <div class="<%=CssClass %>ContentBody">
        <asp:Literal ID="LiteralContent" runat="server"></asp:Literal>
    </div>
</div>
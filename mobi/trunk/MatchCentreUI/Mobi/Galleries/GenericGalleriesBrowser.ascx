﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericGalleriesBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Galleries.GenericGalleriesBrowser" EnableViewState="false" %>
<div class="<%=CssClass %>">
<asp:ListView ID="rptGalleries" runat="server" 
    ItemPlaceholderID="PlaceHolderItem" 
    onitemdatabound="rptGalleries_ItemDataBound">
    <LayoutTemplate>
    <asp:PlaceHolder ID="PlaceHolderItem" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>
    <ItemTemplate>
    <div class="<%#CssClass %>Item">
        <asp:HyperLink ID="lnkGalleryPic" runat="server" CssClass="ItemImageLink">
        <%-- lnkGalleryPic.CssClass will be appended with GenericGalleriesBrowser.CssClass property --%>
            <asp:Image ID="imgGalleryPic" runat="server" CssClass="ItemImage" />
            <%-- imgGalleryPic.CssClass will be appended with GenericGalleriesBrowser.CssClass property --%>
        </asp:HyperLink>
        <asp:HyperLink ID="lnkHeadline" runat="server" CssClass="ItemHeadlineLink">
        <%-- lnkHeadline.CssClass will be appended with GenericGalleriesBrowser.CssClass property --%>
            <span class="<%#CssClass %>ItemHeadline"><%# Eval("Headline")%></span>
        </asp:HyperLink>
        <br class="<%#CssClass %>ItemLinkBreak" />
    </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <div class="<%#CssClass %>AltItem">
        <asp:HyperLink ID="lnkGalleryPic" runat="server" CssClass="AltItemImageLink">
        <%-- lnkGalleryPic.CssClass will be appended with GenericGalleriesBrowser.CssClass property --%>
            <asp:Image ID="imgGalleryPic" runat="server" CssClass="AltItemImage" />
            <%-- imgGalleryPic.CssClass will be appended with GenericGalleriesBrowser.CssClass property --%>
        </asp:HyperLink>
        <asp:HyperLink ID="lnkHeadline" runat="server" CssClass="AltItemHeadlineLink">
        <%-- lnkHeadline.CssClass will be appended with GenericGalleriesBrowser.CssClass property --%>
            <span class="<%#CssClass %>AltItemHeadline"><%# Eval("Headline")%></span>
        </asp:HyperLink>
        <br class="<%#CssClass %>AltItemLinkBreak" />
    </div>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    </EmptyDataTemplate>
</asp:ListView>
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericGalleryBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Galleries.GenericGalleryBrowser" EnableViewState="false" %>
<div class="<%=CssClass %>">
    <asp:ListView ID="rptGallery" runat="server" 
        ItemPlaceholderID="PlaceHolderItem" 
        onitemdatabound="rptGallery_ItemDataBound" onprerender="rptGallery_PreRender">
        <LayoutTemplate>
    <div class="<%#CssClass %>ItemContainer">
        <asp:PlaceHolder ID="PlaceHolderItem" runat="server"></asp:PlaceHolder>
    </div>
        </LayoutTemplate>
        <ItemTemplate>
        <div class="<%#CssClass %>Item">
            <asp:HyperLink ID="lnkPic" runat="server" CssClass="ItemImageLink">
            <%-- lnkPic.CssClass will be appended with GenericGalleryBrowser.CssClass property --%>
                <asp:Image ID="imgPic" runat="server" CssClass="ItemImage" />
                <%-- imgPic.CssClass will be appended with GenericGalleryBrowser.CssClass property --%>
            </asp:HyperLink>
            <br class="<%#CssClass %>ItemLinkBreak" />
        </div>
        </ItemTemplate>
        <AlternatingItemTemplate>
        <div class="<%#CssClass %>AltItem">
            <asp:HyperLink ID="lnkPic" runat="server" CssClass="AltItemImageLink">
            <%-- lnkPic.CssClass will be appended with GenericGalleryBrowser.CssClass property --%>
                <asp:Image ID="imgPic" runat="server" CssClass="AltItemImage" />
                <%-- imgPic.CssClass will be appended with GenericGalleryBrowser.CssClass property --%>
            </asp:HyperLink>
            <br class="<%#CssClass %>AltItemLinkBreak" />
        </div>
        </AlternatingItemTemplate>
        <EmptyDataTemplate>
        </EmptyDataTemplate>
    </asp:ListView>
    <div class="<%#CssClass %>Pager">
        <asp:DataPager ID="pgrGalleryBrowser" runat="server" PagedControlID="rptGallery" PageSize="2">
            <Fields>
                <asp:TemplatePagerField>
                    <PagerTemplate>
                        <asp:HyperLink ID="lnkPrev" runat="server" CssClass="PagerPrevLink">&lt; Previous</asp:HyperLink>
                        <%-- lnkPrev.CssClass will be prepended with GenericGalleryBrowser.CssClass property --%>
                        <asp:HyperLink ID="lnkNext" runat="server" CssClass="PagerNextLink">Next &gt;</asp:HyperLink>
                        <%-- lnkPic.CssClass will be prepended with GenericGalleryBrowser.CssClass property --%>
                    </PagerTemplate>
                </asp:TemplatePagerField>
            </Fields>
        </asp:DataPager>
    </div>
</div>

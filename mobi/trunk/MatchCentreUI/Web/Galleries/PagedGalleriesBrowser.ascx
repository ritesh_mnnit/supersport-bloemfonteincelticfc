﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagedGalleriesBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Galleries.PagedGalleriesBrowser" EnableViewState="false" %>

<%@ Register src="../Utils/GenericPager.ascx" tagname="GenericPager" tagprefix="uc1" %>

<div id="<%= ClientID %>" class="mc-ui-gallery-widget ui-widget ui-widget-content <%=CssClass %>">
<uc1:GenericPager ID="ucGenericPagerTop" runat="server" CssClass="pager-top" />
<asp:ListView ID="rptGalleries" runat="server" 
    onitemdatabound="rptGalleries_ItemDataBound">
    <LayoutTemplate>
        <div id="groupPlaceholder" runat="server"></div>
    </LayoutTemplate>
    <GroupTemplate>
        <div id="galleryGroup" runat="server">
            <div id="itemPlaceholder" runat="server"></div>
        </div>
    </GroupTemplate>
    <GroupSeparatorTemplate>
    </GroupSeparatorTemplate>
    <ItemTemplate>
    <div class="mc-ui-gallery-item">
        <div class="mc-ui-gallery-item-image-container">
            <asp:HyperLink ID="lnkGalleryPic" runat="server" CssClass="mc-ui-gallery-item-image-link">
                <asp:Image ID="imgGalleryPic" runat="server" CssClass="mc-ui-gallery-item-image" />
            </asp:HyperLink>
        </div>
        <div class="mc-ui-gallery-item-text-container">
            <span class="mc-ui-gallery-item-headline">
                <asp:HyperLink ID="lnkHeadline" runat="server" CssClass="mc-ui-gallery-item-headline-link">
                    <span class="mc-ui-gallery-item-headline-ellipsis">
                        <%# Eval("Headline")%>
                    </span>
                    <span class="mc-ui-gallery-item-headline-non-ellipsis">
                        <%# Eval("Headline")%>
                    </span>
                </asp:HyperLink>
            </span>
            <br class="mc-ui-gallery-item-headline-break" />
            <span class="mc-ui-gallery-item-datetime">
                <span class="mc-ui-gallery-item-date">
                    <asp:HyperLink ID="lnkDate" runat="server" CssClass="mc-ui-gallery-item-date-link">
                        <%# string.Format("{0:yyyy/MM/dd}", Eval("DateCreated"))%>
                    </asp:HyperLink>
                </span>
                <span class="mc-ui-gallery-item-time">
                    <asp:HyperLink ID="lnkTime" runat="server" CssClass="mc-ui-gallery-item-time-link">
                        <%# string.Format("{0:HH:mm}", Eval("DateCreated"))%>
                    </asp:HyperLink>
                </span>
            </span>
            <br class="mc-ui-gallery-item-datetime-break" />
            <span class="mc-ui-gallery-item-description">
                <asp:HyperLink ID="lnkDescription" runat="server" CssClass="mc-ui-gallery-item-description-link">
                    <span class="mc-ui-gallery-item-description-ellipsis">
                        <%# Eval("Description")%>
                    </span>
                    <span class="mc-ui-gallery-item-description-non-ellipsis">
                        <%# Eval("Description")%>
                    </span>
                </asp:HyperLink>
            </span>
            <br class="mc-ui-gallery-item-description-break" />
        </div>
        <br class="mc-ui-gallery-item-break" />
    </div>
    </ItemTemplate>
    <ItemSeparatorTemplate>
    </ItemSeparatorTemplate>
    <EmptyDataTemplate>
            <span class="mc-ui-empty-message">
                There are currently no galleries available.
            </span>
    </EmptyDataTemplate>
</asp:ListView>
<uc1:GenericPager ID="ucGenericPagerBot" runat="server" CssClass="pager-bot" />
</div>

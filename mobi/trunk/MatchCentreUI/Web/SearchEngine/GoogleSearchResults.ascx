﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoogleSearchResults.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.SearchEngine.GoogleSearchResults" EnableViewState="false" %>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
    //<![CDATA[

    google.load('search', '1.0');

    function OnLoad() {
        var searchControl = new google.search.SearchControl();
        searchControl.setResultSetSize(GSearch.LARGE_RESULTSET);
        searchControl.setLinkTarget(google.search.Search.LINK_TARGET_SELF);


        var siteSearch = new google.search.WebSearch();
        siteSearch.setUserDefinedLabel("<%= (!string.IsNullOrEmpty(UserDefinedLabel) ? UserDefinedLabel : SuperSport.MatchCentre.UI.Utils.SiteConfig.SiteUrlAuthority) %>");
        siteSearch.setUserDefinedClassSuffix("siteSearch");
        siteSearch.setSiteRestriction("<%= SuperSport.MatchCentre.UI.Utils.SiteConfig.SiteUrlAuthority %>");
        searchControl.addSearcher(siteSearch);

        var drawOptions = new google.search.DrawOptions();
        drawOptions.setDrawMode(google.search.SearchControl.DRAW_MODE_TABBED);
        searchControl.draw(document.getElementById("<%= ClientID %>"), drawOptions);

        searchControl.execute("<%= Request.QueryString["q"] %>");
    }

    google.setOnLoadCallback(OnLoad, true);

    //]]>
</script>
<div id="<%= ClientID %>" class="mc-ui-search-results-widget ui-widget ui-widget-content <%=CssClass %>">
Loading...
</div>
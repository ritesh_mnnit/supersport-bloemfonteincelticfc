﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericVenuesList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Venues.GenericVenuesList" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-venues-widget ui-widget ui-widget-content <%=CssClass%>">
    <asp:ListView ID="rptVenues" runat="server" 
        onitemdatabound="rptVenues_ItemDataBound">
        <LayoutTemplate>
            <ul class="mc-ui-venues-list">
                <li id="itemPlaceholder" runat="server"></li>
            </ul>
        </LayoutTemplate>
        <ItemTemplate>
            <li id="itemPlaceholder" runat="server" class="mc-ui-venues-list-item">
                <asp:Label ID="spanVenueImageContainer" runat="server" CssClass="mc-ui-venue-image-container">
                    <a class="mc-ui-venue-image-link" href="<%# GetVenueViewUrl(Convert.ToInt32(Eval("ID"))) %>">
                        <asp:Image ID="imgVenueImage" runat="server" CssClass="mc-ui-venue-image" />
                    </a>
                </asp:Label>
                <asp:Label ID="spanVenueName" runat="server" Text="" CssClass="mc-ui-venue-name">
                    <a class="mc-ui-venue-name-link" href="<%# GetVenueViewUrl(Convert.ToInt32(Eval("ID"))) %>">
                        <asp:Literal ID="LiteralVenueName" runat="server"></asp:Literal>
                    </a>
                </asp:Label>
            </li>
        </ItemTemplate>
    </asp:ListView>
</div>


﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagedColumnsBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Columns.PagedColumnsBrowser" EnableViewState="false" %>

<%@ Register src="../Utils/GenericPager.ascx" tagname="GenericPager" tagprefix="uc1" %>

<div id="<%= ClientID %>" class="mc-ui-news-widget ui-widget ui-widget-content <%= CssClass %>">
<uc1:GenericPager ID="ucGenericPagerTop" runat="server" CssClass="pager-top" />
<asp:ListView ID="rptNews" runat="server" OnItemDataBound="rptNews_ItemDataBound">
    <LayoutTemplate>
        <div class="mc-ui-news-list-container">
            <ul class="mc-ui-news-list">
                <li id="itemPlaceholder" runat="server"></li>
            </ul>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <li id="itemPlaceholder" runat="server" class="mc-ui-news-list-item">
            <div class="mc-ui-news-item-container">
                <div class="mc-ui-news-item-image-container">
                    <a class="mc-ui-news-item-full-image-link" href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("AuthorUrlName").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                        <asp:Image ID="imgArticleFullPic" runat="server" CssClass="mc-ui-news-item-full-image" />
                    </a>
                </div>
                <div class="mc-ui-news-item-text-container">
                    <a class="mc-ui-news-item-image-link" href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("AuthorUrlName").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                        <asp:Image ID="imgArticleThumbnailPic" runat="server" CssClass="mc-ui-news-item-image" />
                    </a>
                    <br class="mc-ui-news-item-image-break" />
                    <span class="mc-ui-news-item-headline">
                        <a class="mc-ui-news-item-headline-link" href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("AuthorUrlName").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                            <span class="mc-ui-news-item-headline-ellipsis">
                                <%# Eval("Headline")%>
                            </span>
                            <span class="mc-ui-news-item-headline-non-ellipsis">
                                <%# Eval("Headline")%>
                            </span>
                        </a>
                    </span>
                    <br class="mc-ui-news-item-headline-break" />
                    <span class="mc-ui-news-item-datetime">
                        <a class="mc-ui-news-item-date-link" href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("UrlFriendlyDate").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                            <%# string.Format("{0:" + DisplayDateFormat + "}",(DisplayDate == SuperSport.MatchCentre.UI.Web.Columns.DisplayDate.DateModified ? Eval("UpdatedDate") : Eval("DateCreated")))%>
                        </a>
                    </span>
                    <br class="mc-ui-news-item-datetime-break" />
                    <span class="mc-ui-news-item-blurb">
                        <a class="mc-ui-news-item-blurb-link" href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("AuthorUrlName").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                            <span class="mc-ui-news-item-blurb-ellipsis">
                                <%# Eval("Blurb")%>
                            </span>
                            <span class="mc-ui-news-item-blurb-non-ellipsis">
                                <%# Eval("Blurb")%>
                            </span>
                        </a>
                    </span>
                    <br class="mc-ui-news-item-blurb-break" />
                    <span class="mc-ui-news-item-category">
                        <a class="mc-ui-news-item-category-link" href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("AuthorUrlName").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                            <%# Eval("Category")%>
                        </a>
                    </span>
                    <br class="mc-ui-news-item-category-break" />
                    <span class="mc-ui-news-item-goto">
                        <a class="mc-ui-news-item-goto-link" href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("AuthorUrlName").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                        <%# ViewArticleLinkText %>
                        </a>
                    </span>
                    <br class="mc-ui-news-item-goto-break" />
                </div>
            </div>
        </li>
    </ItemTemplate>
    <ItemSeparatorTemplate>
    </ItemSeparatorTemplate>
    <EmptyDataTemplate>
    </EmptyDataTemplate>
</asp:ListView>
<uc1:GenericPager ID="ucGenericPagerBot" runat="server" CssClass="pager-bot" />
</div>
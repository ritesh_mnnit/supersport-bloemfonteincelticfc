﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericPager.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Utils.GenericPager" EnableViewState="false" %>
<div class="mc-ui-pager <%= CssClass %>">
    <span class="page-count-info">Page <span class="page-index"><asp:Literal ID="LiteralPageIndex" runat="server"></asp:Literal></span> of  <span class="page-count"><asp:Literal ID="LiteralPageCount" runat="server"></asp:Literal></span></span>
    <asp:HyperLink ID="lnkPagerFirst" runat="server"><span>&lt;&lt;</span></asp:HyperLink>
    <asp:HyperLink ID="lnkPagerPrev" runat="server"><span>&lt;</span></asp:HyperLink>
    <asp:Repeater ID="rptPager" runat="server">
        <ItemTemplate>
            <a href="<%# Eval("Value") %>" class="<%# CurrentPageCssClass(Container.DataItem) %>"><span><%# Eval("Key") %></span></a>
        </ItemTemplate>
    </asp:Repeater>
    <asp:HyperLink ID="lnkPagerNext" runat="server"><span>&gt;</span></asp:HyperLink>
    <asp:HyperLink ID="lnkPagerLast" runat="server"><span>&gt;&gt;</span></asp:HyperLink>
</div>
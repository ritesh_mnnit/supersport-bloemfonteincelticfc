﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericVideoView.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Videos.GenericVideoView" EnableViewState="false" %>
<div class="mc-ui-video-player-widget">
    <asp:Panel ID="pnlVideoView" runat="server">
        <script type="text/javascript" src="http://core.dstv.com/js/swfobject.js"></script>
        <div class="video_player_block">
            <script type="text/javascript">
                var flashvars = { viewwidth: 536,
	                viewheight: 400,
	                clientid: "za-101768",
	                vcid: "b03",
	                brand: "supersport",
	                base_url: "<%= this.SSVideoPlayer.JsonUrl %>",
	                id: <%= this.VideoID %>
                };
                var params = {
	                menu: "false",
	                allowFullscreen: "true",
	                allowScriptAccess: "always",
	                bgcolor: "#000000",
	                wmode: "transparent"
                };
                var attributes = {
	                id: "flashcontent"
                };

                (function(){
                    swfobject.embedSWF("<%=this.SSVideoPlayer.FlashUrl%>", "altContent", "536", "400", "9.0.246", "http://core.dstv.com/flash/expressInstall.swf", flashvars, params, attributes);
                })();

                function callFromPlayer(param) {
	                //alert(param.call)
	                //alert(param.params)
	                //we will insert this once the call is added to the player
	                if (param.call == 'stop_video') {
	                }
	                if (param.call == 'play_video') {
	                }
	                if (param.call == 'pop_up') {
	                    window.open('http://www.dstv.com', 'pop', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=622,height=350');
	                    newWindow.focus();
	                    void (0);
	                }
	                if (param.call == 'dim_lights') {
	                    document.getElementById('light').style.display = 'block';
	                }
	                if (param.call == 'raise_lights') {
	                    document.getElementById('light').style.display = 'none';
	                }
                }

                function popup() {

                }

                function sendToPlayer(action, params) {
	                getPlayer("flashcontent").callPlayerFunction(action, params);
                }

                function getPlayer(movieName) {
	                if (navigator.appName.indexOf("Microsoft") != -1) {
	                    return window[movieName];
	                } else {
	                    return document[movieName];
	                }
	                return null;
                }
            </script>

            <div class="video_player">
                <div id="altContent">
                    <h1>DSTV content</h1>
                    <p>
                        <a href="http://get.adobe.com/flashplayer/">
                            <img src="http://www.adobe.com/images/shared/download_buttons/get_adobe_flash_player.png" alt="Get Adobe Flash player" />
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlVideoViewNotAvailable" runat="server" Visible="false">
        <div class="mc-ui-empty-message">
            The requested video is not available for your region or device.
        </div>
    </asp:Panel>
    <asp:Literal ID="LiteralCountryCode" runat="server"></asp:Literal>
    <asp:Literal ID="LiteralIP" runat="server"></asp:Literal>
</div>
﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="galleries.aspx.cs" Inherits="SuperSport_BloemCelticMobi.galleries" %>
<%@ Register src="~/MatchCentreUI/Mobi/Galleries/GenericGalleriesBrowser.ascx" tagname="Galleries" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="GalleriesHeader" class="header">
        Latest Pics
        <asp:Image ID="imgGalleriesHeaderIcon" runat="server" AlternateText="Latest Pics" ImageUrl="~/images/header_arrow_s.png" CssClass="headericonright_e" />
    </div>
    <div id="GalleriesContent" class="home-Container">
        <uc:Galleries ID="ucGalleries" runat="server" CssClass="galleries-control" Count="4" />
        <br class="clear" />
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CMS.Models;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport_BloemCelticMobi.General;

namespace SuperSport_BloemCelticMobi
{
    public partial class gallery : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPageTitle("Gallery");
            // Set Gallery Title
            List<PhotoGallery> galleries = PhotoGallery.GetPhotoGalleries(SiteConfig.SiteID, "");
            string galleryName = (
                from PhotoGallery gallery in galleries
                where gallery.Id == Convert.ToInt32(Page.RouteData.Values["id"])
                select gallery.Headline
                ).FirstOrDefault();

            if (!string.IsNullOrEmpty(galleryName))
            {
                LiteralGalleryName.Text = galleryName;
                Page.Title = string.Format("{0} - {1}", PageInfo.PageTitlePrefix, galleryName);
            }

            ucGallery.DataBind();
        }
    }
}
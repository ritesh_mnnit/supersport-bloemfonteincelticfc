﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.Models;
using SuperSport.MatchCentre.DAL.Football;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport_BloemCelticMobi.General;

namespace SuperSport_BloemCelticMobi
{
    public partial class logs : PageBase
    {
        protected FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPageTitle("Log");
            RenderLogs();
        }

        private void RenderLogs()
        {
            ucLogs.LogsCentre = _FootballCentre;
            ucLogs.Category = SiteConfig.DefaultCategory;
            ucLogs.DataBind();
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="matches.aspx.cs" Inherits="SuperSport_BloemCelticMobi.matches" %>
<%@ Register src="~/MatchCentreUI/Mobi/Fixtures/CompactFixturesList.ascx" tagname="FixturesList" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Results/CompactResultsList.ascx" tagname="ResultsList" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Logs/GenericLogs.ascx" tagname="GenericLogs" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
<div id="FixturesHeader" class="header">Latest Fixtures</div>
<div id="FixturesContent" class="home-Container">
    <uc:FixturesList ID="FixturesListLeague" runat="server" CssClass="fixtures" Count="3" />
    <asp:HyperLink ID="lnkFixturesPage" runat="server" 
    NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.FixturesRoute, null) %>'
    CssClass="home-MoreLink">...more fixtures</asp:HyperLink>
    <br class="clear" />
</div>
<div id="ResultsHeader" class="header">Latest Results</div>
<div id="ResultsContent" class="home-Container">
    <uc:ResultsList ID="ResultsListLeague" runat="server" CssClass="results" Count="3" />
    <asp:HyperLink ID="lnkResultsPage" runat="server" 
    NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.ResultsRoute, null) %>'
    CssClass="home-MoreLink">...more results</asp:HyperLink>
    <br class="clear" />
</div>
<div id="LogHeader" class="header">
    <asp:HyperLink ID="lnkLogsPage" runat="server" 
    NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.LogsRoute, null) %>'>
        Log
        <asp:Image ID="imgLogHeaderIcon" runat="server" AlternateText="Log" ImageUrl="~/images/header_arrow_e.png" CssClass="headericonright_e" />
    </asp:HyperLink>
</div>
    <div id="LogContent" class="home-Container">
        <uc:GenericLogs ID="ucLogs" runat="server" CssClass="logs" CentroidTeamName="Bloem Celtic" Count="5" />
        <br class="clear" />
    </div>
</asp:Content>

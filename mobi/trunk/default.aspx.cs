﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.Models;
using SuperSport.MatchCentre.DAL.Football;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.CMS.Models;
using SuperSport_BloemCelticMobi.General;

namespace SuperSport_BloemCelticMobi
{
    public partial class _default : PageBase
    {
        protected FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            RenderTopNews();
            SetPageTitle("Home");

            //league fixtures
            RenderLeagueFixtures();
            RenderLeagueResults();

            RenderLogs();
        }

        private void RenderTopNews()
        {
            ucNewsBrowser.SiteID = SiteConfig.SiteID;
            ucNewsBrowser.DataBind();
        }

        private void RenderLeagueFixtures()
        {
            ucFixturesList.FixturesCentre = _FootballCentre;
            ucFixturesList.SiteID = SiteConfig.SiteID;
            ucFixturesList.DataBind();
        }

        private void RenderLeagueResults()
        {
            ucResultsList.ResultsCentre = _FootballCentre;
            ucResultsList.SiteID = SiteConfig.SiteID;
            ucResultsList.DataBind();
        }

        private void RenderLogs()
        {
            ucLogs.LogsCentre = _FootballCentre;
            ucLogs.Category = SiteConfig.DefaultCategory;
            ucLogs.DataBind();
        }
    }
}
﻿Version (1.0)

ReadMe
------
This is a Read Me file to install the Bloem Celtic Mobile Web Application.
Contents of this file are:
1.Prerequisites
2.Installation Details
3.Deployment notes
4.Release Notes

1. Prerequisites
----------------
1.1 IIS 7
1.2 Memcache server
1.3 Access to SuperSport MS SQL database schema


2. Installation Details
-----------------------
2.1 Setup folder on IIS as a .Net 4 Application. (App pool can use integrated mode)
2.2 Open web.config and configure the following settings according to the environment
	2.2.1 connectionStrings (za-sql.supersport.com for local lan testing and za-sql.dstvo.local for production)
	2.2.2 memcached -> servers (127.0.0.1 if memcache running local)
	2.2.3 when deploying to production make sure customErrors mode="On"
	2.2.4 when deploying to production make sure compilation debug="false"
2.3 Delete/Replace all existing files/folders on deployment server that is contained in the release

3. Deployment Instructions
--------------------------


4. Release Notes
-----------------
Version 1.0 - 2012/03/08
- Live deployment of Bloem Celtic Mobile site

﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="results.aspx.cs" Inherits="SuperSport_BloemCelticMobi.results" %>
<%@ Register src="~/MatchCentreUI/Mobi/Results/CompactResultsList.ascx" tagname="ResultsList" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="ResultsHeader" class="header">Results</div>
    <div id="ResultsContent" class="home-Container">
        <uc:ResultsList ID="ResultsListLeague" runat="server" CssClass="results" Count="50" />
    </div>
</asp:Content>
